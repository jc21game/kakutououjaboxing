#include "Enemy.h"
#include "Engine/ResouceManager/Model.h"
#include "Enemy_Hand_decision.h"
#include "Enemy_Hand_decision_jab.h"
#include "Enemy_Hand_decision_hook.h"
#include "Enemy_Hand_decision_upper.h"
#include "Gauge_bar_2p.h"
#include "PlayScene.h"
//#include "Engine/gameObject/Camera.h"

//コンストラクタ
Enemy::Enemy(IGameObject * parent)
	:IGameObject(parent, "Enemy"), _hModel(-1),guardpoint(false),Rotate_(3.0),
	down_(false), tdown_(false), downjudge(false)
{
}

//デストラクタ
Enemy::~Enemy()
{
}

//初期化
void Enemy::Initialize()
{
	//モデルデータのロード
	_hModel = Model::Load("data/bule.mortion.fbx");
	assert(_hModel >= 0);

	/*Camera* pCamera = CreateGameObject<Camera>(this);

	pCamera->SetPosition(D3DXVECTOR3(0, 5, 0.5));
	pCamera->SetTarget(D3DXVECTOR3(0, 5, 1));
	
	*/

	guardpoint == false;

	//位置を変えている
	_position = D3DXVECTOR3(0, 4, 5);
	//_scale = D3DXVECTOR3(0.7f, 0.7f, 0.7f);
	_rotate.y = 180;
	Model::SetAnimFrame(_hModel, 20, 40, 0.5f);

	SphereCollider* head = new SphereCollider(D3DXVECTOR3(0.3, 9.5, 0), 0.6f);
	AddCollider(head);//頭判定
	BoxCollider* body = new BoxCollider(D3DXVECTOR3(0, 6.5, 0), D3DXVECTOR3(1.5, 3.5, 1));
	AddCollider(body);//体判定
}

//更新
void Enemy::Update()
{
	//左スティックでオブジェクトを動かす
	//スティックの倒した方向に進む
	stick = Input::GetPadStickL(1);

	D3DXMATRIX mRot;
	D3DXMatrixRotationY(&mRot, D3DXToRadian(_rotate.y));

	D3DXVec3TransformCoord(&move, &D3DXVECTOR3(0, 0, 0.2f), &mRot);

	//ダウン中以外は行動可能
	if (downjudge == false)
	{
		//ガード中は動けない
		if (guardpoint == false) {
			//そのままだと早すぎるので割って遅くする


			//2DにするんだったらXの表記を消してもらって横移動だけで…
			//_position.x -= (stick.y / 2.5f);

			_position.z += (stick.x / 2.5f);

			//キーボードのDを押されたときキャラを右に動かす
			if (Input::IsKey(DIK_D))
			{
				_position.z -= 0.5;
			}
			//キーボードのAを押されたときキャラを左に動かす
			if (Input::IsKey(DIK_A))
			{
				_position.z += 0.5;
			}
			//キーボードのWを押されたときキャラの向いてる方向に進ませる
			if (Input::IsKey(DIK_W))
			{
				_position = _position + move;
			}
			//キーボードのSを押されたときキャラの向いてる方向に下がらせる
			if (Input::IsKey(DIK_S))
			{
				_position = _position - move;
			}
			//キーボードのYを押されたときキャラが右回転する
			if (Input::IsKey(DIK_Y))
			{
				_rotate.y += Rotate_;
			}
			//キーボードのUを押されたときキャラが左回転する
			if (Input::IsKey(DIK_U))
			{
				_rotate.y -= Rotate_;
			}

			// 指定範囲を超えないようにする
			if (_position.x < -20.0f)
			{
				_position.x = -20.0f;
			}
			if (_position.z < -20.0f)
			{
				_position.z = -20.0f;
			}
			if (20.0f < _position.x)
			{
				_position.x = 20.0f;
			}
			if (20.0f < _position.z)
			{
				_position.z = 20.0f;
			}
			//ここまで//////////////////

			//攻撃関係
			//Fキーでパンチ！！！！！！！！
			if (Input::IsKey(DIK_F) || Input::IsPadButtonDown(XINPUT_GAMEPAD_X, 1))
			{
				if ((Model::GetAnimFrame(_hModel) > 55 || Model::GetAnimFrame(_hModel) < 45)
					&&(Model::GetAnimFrame(_hModel) < 60  || Model::GetAnimFrame(_hModel) > 73)//ストレート中以外
					&&(Model::GetAnimFrame(_hModel) < 135 || Model::GetAnimFrame(_hModel) > 153)//フック中以外
					&&(Model::GetAnimFrame(_hModel) < 230 || Model::GetAnimFrame(_hModel) > 250))//アッパー中以外
				{
					Model::SetAnimFrame(_hModel, 45, 55, 0.5f);

					//ハンド呼び出し
					Enemy_Hand_decision_jab* Ppunch_jab =
						CreateGameObject<Enemy_Hand_decision_jab>(FindObject("PlayScene"));

					//拳から攻撃判定を表示するための関数
					//ライトハンド参照
					D3DXVECTOR3 shotPos = Model::GetBonePosition(_hModel, "Character1_LeftHand");
					//ライトフィンガーベース参照
					D3DXVECTOR3 PunchRoot = Model::GetBonePosition(_hModel, "Character1_LeftFingerBase");
					//パンチの出し方？
					Ppunch_jab->Punch(shotPos, shotPos - PunchRoot);
				}
			}

			if (Model::GetAnimFrame(_hModel) == 55)
			{
				Model::SetAnimFrame(_hModel, 20, 40, 0.5f);//待機モーション
			}

			//Gで右ストレートでぶっとばす
			if (Input::IsKeyDown(DIK_G) || Input::IsPadButtonDown(XINPUT_GAMEPAD_Y, 1))
			{
				/*if (Model::GetAnimFrame(_hModel) > 73 || Model::GetAnimFrame(_hModel) < 60)
				{*/

				if (Model::GetAnimFrame(_hModel) < 55 && Model::GetAnimFrame(_hModel) > 50)//ジャブのフレーム
				{
					Model::SetAnimFrame(_hModel, 60, 73, 0.5f);
					IGameObject* playScene = FindObject("PlayScene");
					Enemy_Hand_decision* Ppunch = CreateGameObject<Enemy_Hand_decision>(FindObject("PlayScene"));

					//拳から攻撃判定を表示するための関数
					//ライトハンド参照
					D3DXVECTOR3 shotPos = Model::GetBonePosition(_hModel, "Character1_RightHand");
					//ライトフィンガーベース参照
					D3DXVECTOR3 PunchRoot = Model::GetBonePosition(_hModel, "Character1_RightFingerBase");
					//パンチの出し方？
					Ppunch->Punch(shotPos, shotPos - PunchRoot);

				}
			}
			if (Model::GetAnimFrame(_hModel) == 73)
			{
				Model::SetAnimFrame(_hModel, 20, 40, 0.5f);//待機モーション
			}



			//フック(仮)
			if (Input::IsKeyDown(DIK_J) || Input::IsPadButtonDown(XINPUT_GAMEPAD_B, 1))
			{
				/*if (Model::GetAnimFrame(_hModel) > 135 || Model::GetAnimFrame(_hModel) < 153)
				{*/

				if (Model::GetAnimFrame(_hModel) < 73 && Model::GetAnimFrame(_hModel) > 65)//ジャブのフレーム
				{
					Model::SetAnimFrame(_hModel, 135, 153, 1.0f);

					//ハンド呼び出し
					Enemy_Hand_decision_hook* Ppunch_hook =
						CreateGameObject<Enemy_Hand_decision_hook>(FindObject("PlayScene"));

					//拳から攻撃判定を表示するための関数
					//ライトハンド参照
					D3DXVECTOR3 shotPos = Model::GetBonePosition(_hModel, "Character1_LeftHand");
					//ライトフィンガーベース参照
					D3DXVECTOR3 PunchRoot = Model::GetBonePosition(_hModel, "Character1_LeftFingerBase");
					//パンチの出し方？
					Ppunch_hook->Punch(shotPos, shotPos - PunchRoot);
				}
			}
			if (Model::GetAnimFrame(_hModel) == 153)
			{
				Model::SetAnimFrame(_hModel, 20, 40, 0.5f);//待機モーション
			}

			//アッパー
			if (Input::IsKeyDown(DIK_K) || Input::IsPadButtonDown(XINPUT_GAMEPAD_A, 1))
			{
				/*if (Model::GetAnimFrame(_hModel) > 230 || Model::GetAnimFrame(_hModel) < 250)
				{*/

				//
				if (Model::GetAnimFrame(_hModel) < 55 && Model::GetAnimFrame(_hModel) > 50)//ジャブのフレーム
				{
					Model::SetAnimFrame(_hModel, 230, 250, 1.0f);

					//ハンド呼び出し
					Enemy_Hand_decision_upper* Ppunch_upper =
						CreateGameObject<Enemy_Hand_decision_upper>(FindObject("PlayScene"));

					//拳から攻撃判定を表示するための関数
					//ライトハンド参照
					D3DXVECTOR3 shotPos = Model::GetBonePosition(_hModel, "Character1_LeftHand");
					//ライトフィンガーベース参照
					D3DXVECTOR3 PunchRoot = Model::GetBonePosition(_hModel, "Character1_LeftFingerBase");
					//パンチの出し方？
					Ppunch_upper->Punch(shotPos, shotPos - PunchRoot);
				}

			}
			if (Model::GetAnimFrame(_hModel) == 250)
			{
				Model::SetAnimFrame(_hModel, 20, 40, 0.5f);//待機モーション
			}
		}
		//ガードを行う//////////////////////////////////////////////////////
		if (Input::IsKey(DIK_H) ||
			(Input::IsPadButton(XINPUT_GAMEPAD_LEFT_SHOULDER, 1) &&
				Input::IsPadButton(XINPUT_GAMEPAD_RIGHT_SHOULDER, 1)))
		{
			if (Model::GetAnimFrame(_hModel) > 105 || Model::GetAnimFrame(_hModel) < 95)
			{
				Model::SetAnimFrame(_hModel, 95, 105, 0.5f);
				guardpoint = true;
				((PlayScene*)GetParent())->GetBar_2p()->SetGuard(true);
			}
		}
		if (Model::GetAnimFrame(_hModel) == 100)
		{
			Model::SetAnimFrame(_hModel, 100, 105, 0.5f);//待機モーション
		}
		if (Input::IsKeyUp(DIK_H) ||
			Input::IsPadButtonUp(XINPUT_GAMEPAD_LEFT_SHOULDER, 1) ||
			Input::IsPadButtonUp(XINPUT_GAMEPAD_RIGHT_SHOULDER, 1))
		{
			Model::SetAnimFrame(_hModel, 105, 110, 0.5f);
			guardpoint = false;
			((PlayScene*)GetParent())->GetBar_2p()->SetGuard(false);
		}
		if (Model::GetAnimFrame(_hModel) == 110)
		{
			Model::SetAnimFrame(_hModel, 20, 40, 0.5f);//待機モーション
		}

		//ガード判定おわり///////////////////////////////////////////////////
	}
	/////////////////////////ダウン関係↓////////////////////////////////////
	//ダウン(仮)
	if (Input::IsKeyUp(DIK_L) || down_ == true)
	{
		Model::SetAnimFrame(_hModel, 155, 190, 0.5f);
		//無限ループの判定
		down_ = false;
		//ダウンの判定
		downjudge = true;
		//ガード中に倒れた時用の措置
		guardpoint = false;
		//ダウン中に時間を止める関数
		((PlayScene*)GetParent())->downtime(true);
	}

	//倒れ継続
	if (Model::GetAnimFrame(_hModel) == 190)
	{
		Model::SetAnimFrame(_hModel, 190, 191, 0.5f);//待機モーション
	}

	//倒れているときにGかボタンのYで復帰
	if (Model::GetAnimFrame(_hModel) <= 190 && downjudge == true &&( Input::IsKeyUp(DIK_G) || Input::IsPadButtonDown(XINPUT_GAMEPAD_Y, 1)))
	{
		Model::SetAnimFrame(_hModel, 191, 200, 0.5f);//起き上がりモーション
		//時間を進める
		((PlayScene*)GetParent())->downtime(false);
	}
	//復帰
	if (Model::GetAnimFrame(_hModel) == 200)
	{
		Model::SetAnimFrame(_hModel, 20, 40, 0.5f);//待機モーション
		downjudge = false;
	}

	//ノックアウト(仮)
	if (Input::IsKeyUp(DIK_O) || tdown_ == true)
	{
		Model::SetAnimFrame(_hModel, 201, 220, 0.5f);
		tdown_ = false;
		downjudge = true;
		//ダウン中に時間を止める関数
		((PlayScene*)GetParent())->downtime(true);
	}
	if (Model::GetAnimFrame(_hModel) == 220)
	{
		Model::SetAnimFrame(_hModel, 221, 300, 0.5f);//待機モーション
		//3つたまったらRESULTへ変更
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		//changeScebeの引数nextをRESULTに
		pSceneManager->ChangeScene(SCENE_ID_RESULT_CHARA);
	}
	//////////////////////ダウン関係おわり///////////////////////////////
}

//描画
void Enemy::Draw()
{
	Model::SetMatrix(_hModel, _worldMatrix);
	Model::Draw(_hModel);
}

//開放
void Enemy::Release()
{

}

void Enemy::OnCollision(IGameObject * pTarget)
{
	//ダウン中は判定を通さない
	if (downjudge == false)
	{
		//弾に当たった
		if (pTarget->GetObjectName() == "Chara_Hand_decision")
		{
			//ダメージを受けたい
			//((Gauge_bar_2p*)pTarget)->SetHit(true);
			//ペアレントで自分の親を生成。playsceneからGetBarをキャストして呼び出し、
			//Sethitを呼びだした
			((PlayScene*)GetParent())->GetBar_2p()->SetHit(true);

			//相手（弾）を消す
			pTarget->KillMe();
		}

		//ジャブに当たった
		if (pTarget->GetObjectName() == "Chara_Hand_decision_jab")
		{
			//ダメージを受けたい
			((PlayScene*)GetParent())->GetBar_2p()->JobSetHit(true);
			//相手（弾）を消す
			pTarget->KillMe();
		}

		//フックに当たった
		if (pTarget->GetObjectName() == "Chara_Hand_decision_hook")
		{
			//ダメージを受けたい
			((PlayScene*)GetParent())->GetBar_2p()->HookSetHit(true);
			//相手（弾）を消す
			pTarget->KillMe();
		}

		//アッパーに当たった
		if (pTarget->GetObjectName() == "Chara_Hand_decision_upper")
		{
			//ダメージを受けたい
			((PlayScene*)GetParent())->GetBar_2p()->UpperSetHit(true);
			//相手（弾）を消す
			pTarget->KillMe();
		}

		//ガード中に攻撃を食らうと少し後ろにノックバックする判定
		if (guardpoint == true
			&& (pTarget->GetObjectName() == "Chara_Hand_decision"
			|| pTarget->GetObjectName() == "Chara_Hand_decision_jab"
			|| pTarget->GetObjectName() == "Chara_Hand_decision_hook"
			|| pTarget->GetObjectName() == "Chara_Hand_decision_upper"))
		{
			_position = _position - move;
		}

	}
	//お互いが当たった時裏側に回れない用にする
	if (pTarget->GetObjectName() == "Chara")
	{
		_position.z -= (stick.x / 2.5f);
	}
}