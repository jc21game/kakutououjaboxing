#pragma once
#include "Engine/GameObject/GameObject.h"

//◆◆◆を管理するクラス
class FrontWall : public IGameObject
{

public:
	//コンストラクタ
	FrontWall(IGameObject* parent);

	//デストラクタ
	~FrontWall();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};