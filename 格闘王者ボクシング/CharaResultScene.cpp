#include "CharaResultScene.h"
#include "Engine/DirectX/Input.h"
#include "Engine/ResouceManager/Image.h"
//#include "Engine/ResouceManager/Audio.h"
#include "ReB1.h"
#include "ReB1C.h"
#include "ReB2.h"
#include "ReB2C.h"
#include "Winner1P.h"
#include "WinBack.h"
#include "Engine/ResouceManager/Model.h"
//コンストラクタ
CharaResultScene::CharaResultScene(IGameObject * parent)
	: IGameObject(parent, "CharaResultScene"), _hPict(-1), _hSound(-1), butp1(0), butp2(0)
	//重くなるのでダメでした
	//, _hModel(-1)
{
}

//初期化
void CharaResultScene::Initialize()
{
	CreateGameObject<WinBack>(this);
	CreateGameObject<Winner1P>(this);
	
	CreateGameObject<ReB1>(this);
	CreateGameObject<ReB2>(this);

	
	/*
	//キャラ
	//モデルデータのロード
	_hModel = Model::Load("data/red.mortion.fbx");
	assert(_hModel >= 0);
	//position.zは1.0にしないとなぜか表示されませんでした
	_position.z = 1.0f;
	_position.x = 1.5f;
	_position.y = 2.0f;
	*/

}


//更新
void CharaResultScene::Update()
{
	//両者が特定のボタンを押すとタイトルに戻るように
	if (Input::IsKeyDown(DIK_SPACE) || Input::IsPadButtonDown(XINPUT_GAMEPAD_A, 0))
	{
		butp1 = 1;
		//Audio::Play(_hSound);
		CreateGameObject<ReB1C>(this);
	}

	if (Input::IsKeyDown(DIK_RETURN) || Input::IsPadButtonDown(XINPUT_GAMEPAD_A, 1))
	{
		butp2 = 1;
		//Audio::Play(_hSound);
		CreateGameObject<ReB2C>(this);
	}

	if ((butp1 == 1) && (butp2 == 1))
	{

		butp1 = 0;
		butp2 = 0;
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		//changeScebeの引数nextをMENUに
		pSceneManager->ChangeScene(SCENE_ID_TITLE);
	}
}

//描画
void CharaResultScene::Draw()
{
	Image::SetMatrix(_hPict, _worldMatrix);
	Image::Draw(_hPict);

}

//開放
void CharaResultScene::Release()
{
}
