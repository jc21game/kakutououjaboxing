#include "Rule_.h"
#include "Engine/ResouceManager/Image.h"

//コンストラクタ
Rule_::Rule_(IGameObject * parent)
	:IGameObject(parent, "Rule_"), _hPict(-1)
{
}

//デストラクタ
Rule_::~Rule_()
{
}

//初期化
void Rule_::Initialize()
{
	//画像データのロード
	_hPict = Image::Load("data/RULE_.png");
	assert(_hPict >= 0);

	_position.x = 100.0f;
	_position.y = 200.0f;

}

//更新
void Rule_::Update()
{
}

//描画
void Rule_::Draw()
{
	Image::SetMatrix(_hPict, _worldMatrix);
	Image::Draw(_hPict);
}

//開放
void Rule_::Release()
{
}