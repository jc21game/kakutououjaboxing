#pragma once
#include "Engine/GameObject/GameObject.h"

//UIを管理するクラス
class UI : public IGameObject
{
	//UI(矢印)の画像番号
	int ui_hPict;
	//サウンド番号
	int ui_Sound;
public:
	//コンストラクタ
	UI(IGameObject* parent);

	//デストラクタ
	~UI();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};