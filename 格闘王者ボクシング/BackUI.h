#pragma once
#include "Engine/GameObject/GameObject.h"

//◆◆◆を管理するクラス
class BackUI : public IGameObject
{
	//バックボタンの画像番号
	int backui_hPict;


public:
	//コンストラクタ
	BackUI(IGameObject* parent);

	//デストラクタ
	~BackUI();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//何かに当たった
	
};