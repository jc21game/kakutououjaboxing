/////////////////////////////////////
//////これコピッて使ってね///////////
/////////////////////////////////////

#include "bootScene.h"
#include "Engine/ResouceManager/Model.h"

//コンストラクタ
BootScene::BootScene(IGameObject * parent)
	: IGameObject(parent, "BootScene"), _hModel(-1)
{
}

//初期化
void BootScene::Initialize()
{

	//モデルデータのロード
	_hModel = Model::Load("data/ring.fbx");
	assert(_hModel >= 0);

}

//更新
void BootScene::Update()
{
	//スぺスキーを押すと
	if (Input::IsKeyDown(DIK_SPACE))
	{
		//スプラッシュシーンへ切り替わる
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_SPLASH);

	}
}

//描画
void BootScene::Draw()
{
	Model::SetMatrix(_hModel, _worldMatrix);
	Model::Draw(_hModel);
}

//開放
void BootScene::Release()
{
}
