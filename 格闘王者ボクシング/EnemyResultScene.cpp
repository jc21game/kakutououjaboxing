#include "EnemyResultScene.h"
#include "Engine/DirectX/Input.h"
#include "Engine/ResouceManager/Image.h"
//#include "Engine/ResouceManager/Audio.h"
#include "Engine/ResouceManager/Model.h"
#include "ReB1.h"
#include "ReB1C.h"
#include "ReB2.h"
#include "ReB2C.h"
#include "Winner2P.h"
#include "WinBack.h"

//コンストラクタ
EnemyResultScene::EnemyResultScene(IGameObject * parent)
	: IGameObject(parent, "EnemyResultScene"), _hPict(-1), _hSound(-1), butp1(0), butp2(0)
	//重すぎるのでダメでした
	//, _hModel(-1)
{
}

//初期化
void EnemyResultScene::Initialize()
{
	CreateGameObject<WinBack>(this);
	CreateGameObject<Winner2P>(this);
	CreateGameObject<ReB1>(this);
	CreateGameObject<ReB2>(this);


	//モデルデータのロード
	/*_hModel = Model::Load("data/bule.mortion.fbx");
	assert(_hModel >= 0);
	_position.z = 1.0f;*/
}


//更新
void EnemyResultScene::Update()
{
	//両者が特定のボタンを押すとタイトルに戻るように
	if (Input::IsKeyDown(DIK_SPACE) || Input::IsPadButtonDown(XINPUT_GAMEPAD_A, 0))
	{
		butp1 = 1;
		//Audio::Play(_hSound);
		CreateGameObject<ReB1C>(this);
	}

	if (Input::IsKeyDown(DIK_RETURN) || Input::IsPadButtonDown(XINPUT_GAMEPAD_A, 1))
	{
		butp2 = 1;
		//Audio::Play(_hSound);
		CreateGameObject<ReB2C>(this);
	}

	if ((butp1 == 1) && (butp2 == 1))
	{

		butp1 = 0;
		butp2 = 0;
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		//changeScebeの引数nextをMENUに
		pSceneManager->ChangeScene(SCENE_ID_TITLE);
	}
}

//描画
void EnemyResultScene::Draw()
{
	Image::SetMatrix(_hPict, _worldMatrix);
	Image::Draw(_hPict);

}

//開放
void EnemyResultScene::Release()
{
}
