#include "Winner1P.h"
#include "Engine/ResouceManager/Image.h"

//コンストラクタ
Winner1P::Winner1P(IGameObject * parent)
	:IGameObject(parent, "Winner1P"), _hPict(-1)
{
}

//デストラクタ
Winner1P::~Winner1P()
{
}

//初期化
void Winner1P::Initialize()
{
	//画像データのロード
	_hPict = Image::Load("data/1P WIN.png");
	assert(_hPict >= 0);
	// 大きさを変える
	_scale = D3DXVECTOR3(2.0f, 2.0f, 2.0f);
	//位置を変える
	_position = D3DXVECTOR3(200.0f, -700.0f, 0);
}

//更新
void Winner1P::Update()
{
}

//描画
void Winner1P::Draw()
{
	Image::SetMatrix(_hPict, _worldMatrix);
	Image::Draw(_hPict);
}

//開放
void Winner1P::Release()
{
}