#pragma once
#include "Engine/GameObject/GameObject.h"

//◆◆◆を管理するクラス
class Enemy : public IGameObject
{
	bool down_;		//ワンダウンの判定
	bool tdown_;	//スリーダウンの判定
	bool downjudge;	//ダウンの判定
	int _hModel;    //画像番号
	const float Rotate_;
	bool guardpoint; //ガード自動かない用とダメージ軽減判定？

	//移動ベクトル
	D3DXVECTOR3 move;

	//左スティックでオブジェクトを動かす
	//スティックの倒した方向に進む
	D3DXVECTOR3 stick;

public:
	//コンストラクタ
	Enemy(IGameObject* parent);

	//デストラクタ
	~Enemy();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//引数：pTarget 当たった相手
	void OnCollision(IGameObject *pTarget) override;

	int GetModelHandle() { return _hModel; }

	//ワンダウン用
	void Cdown(bool cdown)
	{
		down_ = cdown;
	}

	//スリーダウン用
	void Ctdown(bool ctdown)
	{
		tdown_ = ctdown;
	}
};
