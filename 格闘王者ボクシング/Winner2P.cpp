#include "Winner2P.h"
#include "Engine/ResouceManager/Image.h"

//コンストラクタ
Winner2P::Winner2P(IGameObject * parent)
	:IGameObject(parent, "Winner2P"), _hPict(-1)
{
}

//デストラクタ
Winner2P::~Winner2P()
{
}

//初期化
void Winner2P::Initialize()
{
	//画像データのロード
	_hPict = Image::Load("data/2P WIN.png");
	assert(_hPict >= 0);
	//大きさを変える
	_scale = D3DXVECTOR3(2.0f, 2.0f, 2.0f);
	//位置を変える
	_position = D3DXVECTOR3(200.0f, -700.0f, 0);
}

//更新
void Winner2P::Update()
{
}

//描画
void Winner2P::Draw()
{
	Image::SetMatrix(_hPict, _worldMatrix);
	Image::Draw(_hPict);
}

//開放
void Winner2P::Release()
{
}