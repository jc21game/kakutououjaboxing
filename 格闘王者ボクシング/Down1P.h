#pragma once
#include "Engine/GameObject/GameObject.h"

//◆◆◆を管理するクラス
class Down1P : public IGameObject
{
	int _hPict;    //画像番号
	int DownCount = 0;//スタンの回数表記(仮)
	bool down_;		//falseとtrueで判断
public:
	//コンストラクタ
	Down1P(IGameObject* parent);

	//デストラクタ
	~Down1P();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	void Down();

	void SetDown(bool down)
	{
		down_ = down;
	}
};