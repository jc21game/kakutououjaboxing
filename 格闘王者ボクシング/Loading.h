#pragma once
#include "Engine/GameObject/GameObject.h"


//◆◆◆を管理するクラス
class Loading : public IGameObject
{

	int _hPict; //画像番号
	int fps;
	int cnt = 0;
public:
	//コンストラクタ
	Loading(IGameObject* parent);

	//デストラクタ
	~Loading();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};

