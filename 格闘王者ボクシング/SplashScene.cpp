#include "SplashScene.h"
#include "Logo.h"
#include "SchoolName.h"
#include "Engine/ResouceManager/Image.h"


//コンストラクタ
SplashScene::SplashScene(IGameObject * parent)
	: IGameObject(parent, "SplashScene"), _hPict(-1)
{
}

//初期化
void SplashScene::Initialize()
{

	CreateGameObject<Logo>(this);
	CreateGameObject<SchoolName>(this);
}

//更新
void SplashScene::Update()
{
	//SPACEが押されたら
	if (Input::IsKeyDown(DIK_SPACE) || Input::IsPadButtonDown(XINPUT_GAMEPAD_A))
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		//changeScebeの引数nextをMENUに
		pSceneManager->ChangeScene(SCENE_ID_TITLE);
	}
}

//描画
void SplashScene::Draw()
{

}

//開放
void SplashScene::Release()
{
	
}
