
#include "MenuScene.h"
#include "Engine/ResouceManager/Image.h"
#include"Fight.h"
#include "Option.h"
#include "UI.h"
#include "Engine/ResouceManager/Audio.h"

//コンストラクタ
MenuScene::MenuScene(IGameObject * parent)
	: IGameObject(parent, "MenuScene"), hPict_(-1), 
	testsound_(-1)
{
}


//初期化
void MenuScene::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("data/Menu.png");
	assert(hPict_ >= 0);

	//サウンドデータのロード
	testsound_ = Audio::Load("Data/Menu.wav");
	assert(testsound_ >= 0);

	CreateGameObject<Fight>(this);
	CreateGameObject<Option>(this);
	CreateGameObject<UI>(this);
	
	//Audio::Play(testsound_);
	
}


//更新
void MenuScene::Update()
{
	
}


//描画
void MenuScene::Draw()
{
	Image::SetMatrix(hPict_, _worldMatrix);
	Image::Draw(hPict_);

	
}


//開放
void MenuScene::Release()
{
}
