#include "DrawBack.h"
#include "Engine/ResouceManager/Image.h"

//コンストラクタ
DrawBack::DrawBack(IGameObject * parent)
	:IGameObject(parent, "DrawBack"), _hPict(-1)
{
}

//デストラクタ
DrawBack::~DrawBack()
{
}

//初期化
void DrawBack::Initialize()
{
	//画像データのロード
	_hPict = Image::Load("data/DrawBack.png");
	assert(_hPict >= 0);
	//大きさを変える
	//_scale = D3DXVECTOR3(1.3f, 1.3f, 1.0f);
	//位置を変える
	//_position = D3DXVECTOR3(900.0f, 550.0f, 0);
}

//更新
void DrawBack::Update()
{
}

//描画
void DrawBack::Draw()
{
	Image::SetMatrix(_hPict, _worldMatrix);
	Image::Draw(_hPict);
}

//開放
void DrawBack::Release()
{
}