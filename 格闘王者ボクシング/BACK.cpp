#include "BACK.h"
#include "Engine/ResouceManager/Model.h"
#include "Engine/ResouceManager/Image.h"

//コンストラクタ
BACK::BACK(IGameObject * parent)
	:IGameObject(parent, "BACK"), _hModel(-1), _hPict(-1)
{
}

//デストラクタ
BACK::~BACK()
{
}

//初期化
void BACK::Initialize()
{
	//モデルデータのロード
	_hModel = Model::Load("data/backmodel.fbx");
	assert(_hModel >= 0);
	//画像データのロード
	_hPict = Image::Load("data/backpic.png");
	assert(_hModel >= 0);
	_scale = D3DXVECTOR3(3, 3, 3);
}

//更新
void BACK::Update()
{

	_rotate.y += 0.5f;
}

//描画
void BACK::Draw()
{
	Model::SetMatrix(_hModel, _worldMatrix);
	Model::Draw(_hModel);
}

//開放
void BACK::Release()
{
}