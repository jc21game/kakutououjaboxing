#pragma once
#include "Engine/GameObject/GameObject.h"

//◆◆◆を管理するクラス
class Rule_ : public IGameObject
{
	int _hPict;    //画像番号

public:
	//コンストラクタ
	Rule_(IGameObject* parent);

	//デストラクタ
	~Rule_();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};