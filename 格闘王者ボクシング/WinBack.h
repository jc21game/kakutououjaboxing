#pragma once
#include "Engine/GameObject/GameObject.h"

//●●を管理するクラス
class WinBack : public IGameObject
{
	int _hPict; //画像番号
	int _hModel;    //モデル番号

public:
	//コンストラクタ
	WinBack(IGameObject* parent);

	//デストラクタ
	~WinBack();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};