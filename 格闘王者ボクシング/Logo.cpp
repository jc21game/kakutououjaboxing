#include "Logo.h"
#include "Engine/ResouceManager/Image.h"
//コンストラクタ
Logo::Logo(IGameObject * parent)
	:IGameObject(parent, "Logo"), Logo_hPict(-1)//初期化
{
}

//デストラクタ
Logo::~Logo()
{
}

//初期化
void Logo::Initialize()
{
	//画像データのロード
	Logo_hPict = Image::Load("data/Ramen.png"); //チームロゴ
	assert(Logo_hPict >= 0);
	//大きさを変える(元の画像より小さくした)
	_scale = D3DXVECTOR3(0.8f, 0.8f, 0.8f);

	//ポジション(上にある)
	_position = D3DXVECTOR3(400, -900, 0);
}

//更新
void Logo::Update()
{
	if (_position.y <= 100)
	{
		//int WaitTimer(1000);
		_position.y += 3.0f;
	}

}

//描画
void Logo::Draw()
{
	//画像を描画する
	Image::SetMatrix(Logo_hPict, _worldMatrix);
	Image::Draw(Logo_hPict);
}

//開放
void Logo::Release()
{
}