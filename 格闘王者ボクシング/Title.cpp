#include "Title.h"
#include "Engine/ResouceManager/Image.h"
#include "assert.h"
#include "Press.h"

//Backボタンの表示					
//Backボタンが選択されたらメニューシーンへ切り替え	
//背景の表示



//コンストラクタ
Title::Title(IGameObject * parent)
	:IGameObject(parent, "Title"), _hPict(-1)
{
}

//デストラクタ
Title::~Title()
{
}

//初期化
void Title::Initialize()
{
	//画像データのロード
	_hPict = Image::Load("data/Title.png");
	assert(_hPict >= 0);
	//大きさを変える
	_scale = D3DXVECTOR3(1.5f, 1.5f, 1.5f);
	//位置を変える
	_position = D3DXVECTOR3(-700.0f, -1000.0f, 0);
	
	//最初の色を明るくしたい
	D3DCOLOR color = 0xffffffff;

}

//更新
void Title::Update()
{

	if (_position.x >= 300) 
	{
		//すべて表示されているときにスペースで次に進む
		if ((Input::IsKeyDown(DIK_SPACE) || Input::IsPadButtonDown(XINPUT_GAMEPAD_A)) && count == 1)
		{
			//タイトルシーンへ切り替わる
			SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
			pSceneManager->ChangeScene(SCENE_ID_CHOICE);
		}
	}

	if (_position.x <= 300) {
		//タイトルの動きを飛ばした時
		if (Input::IsKeyDown(DIK_SPACE) || Input::IsPadButtonDown(XINPUT_GAMEPAD_A))
		{
			//規定値に動かす
			_position.x = 300;
			_position.y = 0;
			_scale.x = 0.5f;
			_scale.y = 0.5f;
			_scale.z = 0.5f;
			CreateGameObject<Press>(this);
			//すべて表示できた判定
			count = 1;
		}
	}

	if (_position.x <= 300)
	{
		_position.x += 10;
	}
	if (_position.y <= 0)
	{
		_position.y += 10;
	}

	if (_scale.x >= 0.5) 
	{
		_scale.x -= 0.01f;
		_scale.y -= 0.01f;
		_scale.z -= 0.01f;
	}

	if (_position.x == 300 ) {
		CreateGameObject<Press>(this);
		//すべて表示されたとき用
		count = 1;
	}

}

//描画
void Title::Draw()
{
	Image::SetMatrix(_hPict, _worldMatrix);
	Image::Draw(_hPict);
}

//開放
void Title::Release()
{
}

