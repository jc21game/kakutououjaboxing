#include "Center.h"
#include "CharaView.h"
#include "Engine/ResouceManager/Model.h"

//コンストラクタ
Center::Center(IGameObject * parent)
	:IGameObject(parent, "Center"), _hModel(-1)
{
}

//デストラクタ
Center::~Center()
{
}

//初期化
void Center::Initialize()
{
	CreateGameObject<CharaView>(this);
}

//更新
void Center::Update()
{
}

//描画
void Center::Draw()
{
	Model::SetMatrix(_hModel, _worldMatrix);
	Model::Draw(_hModel);
}

//開放
void Center::Release()
{
}