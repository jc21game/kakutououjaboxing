#include "HitPoint2P.h"
#include "Engine/ResouceManager/Image.h"

//コンストラクタ
HitPoint2P::HitPoint2P(IGameObject * parent)
	:IGameObject(parent, "HitPoint2P"), _hPict(-1)
{
}

//デストラクタ
HitPoint2P::~HitPoint2P()
{
}

//初期化
void HitPoint2P::Initialize()
{
	//画像データのロード
	_hPict = Image::Load("data/gaugeframe_2p.png");
	assert(_hPict >= 0);
	//大きさを変える
	_scale = D3DXVECTOR3(0.9f, 0.9f, 0.9f);
	//位置を変える
	_position = D3DXVECTOR3(770, -170, 0);
}

//更新
void HitPoint2P::Update()
{
}

//描画
void HitPoint2P::Draw()
{
	Image::SetMatrix(_hPict, _worldMatrix);
	Image::Draw(_hPict);
}

//開放
void HitPoint2P::Release()
{
}