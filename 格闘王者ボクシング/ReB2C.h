#pragma once
#include "Engine/GameObject/GameObject.h"

//●●を管理するクラス
class ReB2C : public IGameObject
{
	int _hPict; //画像番号
	int fps;
	int cnt = 0;
public:
	//コンストラクタ
	ReB2C(IGameObject* parent);

	//デストラクタ
	~ReB2C();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};