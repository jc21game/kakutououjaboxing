#include "LeftWall.h"
#include "Engine/gameObject/Camera.h"

//コンストラクタ
LeftWall::LeftWall(IGameObject * parent)
	:IGameObject(parent, "LeftWall")
{
}

//デストラクタ
LeftWall::~LeftWall()
{
}

//初期化
void LeftWall::Initialize()
{
	BoxCollider* collision = new BoxCollider(D3DXVECTOR3(-22, 6, 0), D3DXVECTOR3(3, 11, 40));//1Pから見て左
	//当たり判定読み込み
	AddCollider(collision);
}

//更新
void LeftWall::Update()
{
}

//描画
void LeftWall::Draw()
{
}

//開放
void LeftWall::Release()
{
}