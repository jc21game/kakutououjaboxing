#pragma once
#include "Engine/GameObject/GameObject.h"

//◆◆◆を管理するクラス
class EnemyView : public IGameObject
{

public:
	//コンストラクタ
	EnemyView(IGameObject* parent);

	//デストラクタ
	~EnemyView();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};
