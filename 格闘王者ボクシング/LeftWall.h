#pragma once
#include "Engine/GameObject/GameObject.h"

//◆◆◆を管理するクラス
class LeftWall : public IGameObject
{

public:
	//コンストラクタ
	LeftWall(IGameObject* parent);

	//デストラクタ
	~LeftWall();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};