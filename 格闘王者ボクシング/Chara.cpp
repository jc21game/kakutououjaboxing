#include "Chara.h"
#include "Engine/ResouceManager/Model.h"
#include "CharaView.h"
#include "Chara_Hand_decision.h"
#include "Chara_Hand_decision_jab.h"
#include "Chara_Hand_decision_hook.h"
#include "Chara_Hand_decision_upper.h"
#include "Gauge_bar_1p.h"
#include "PlayScene.h"

//コンストラクタ
Chara::Chara(IGameObject * parent)
	:IGameObject(parent, "Chara"), _hModel(-1), guardpoint(false), Rotate_(3.0),
	down_(false),tdown_(false),downjudge(false)
{
	
}

//デストラクタ
Chara::~Chara()
{
}

//初期化
void Chara::Initialize()
{
	//モデルデータのロード
	_hModel = Model::Load("data/red.mortion.fbx");
	assert(_hModel >= 0);

	guardpoint == false;

	//位置を変えている
	_position = D3DXVECTOR3(0, 4, -10);
	//_scale = D3DXVECTOR3(0.7f, 0.7f, 0.7f);
	Model::SetAnimFrame(_hModel, 20, 40, 0.5f);

	SphereCollider* head = new SphereCollider(D3DXVECTOR3(-0.3, 9.5, 0), 0.6f);
	AddCollider(head);//頭判定
	BoxCollider* body = new BoxCollider(D3DXVECTOR3(0, 6.5, 0), D3DXVECTOR3(1.5, 3.5, 1));
	AddCollider(body);//体判定



}

//更新
void Chara::Update()
{
	/*Enemy* pEnemy = (Enemy*)FindObject("Enemy");    //ステージオブジェクトを探す
	int hEnemyModel = pEnemy->GetModelHandle();    //モデル番号を取得

	RayCastData data;
	data.start = _position;              //レイの発射位置
	data.dir = D3DXVECTOR3(0, -1, 0);    //レイの方向
	Model::RayCast(hEnemyModel, &data); //レイを発射

	//レイが当たったら
	if (data.hit)
	{
		//その分位置を下げる
		_position.y -= data.dist;
	}*/


	D3DXMATRIX mRot;
	D3DXMatrixRotationY(&mRot, D3DXToRadian(_rotate.y));

	D3DXVec3TransformCoord(&move, &D3DXVECTOR3(0, 0, 0.2f), &mRot);


	//左スティックでオブジェクトを動かす
	//スティックの倒した方向に進む
	stick = Input::GetPadStickL(0);

	//ダウン中以外行動可能
	if (downjudge == false)
	{
		//ガード中不可
		if (guardpoint == false) {
			//そのままだと早すぎるので割って遅くする



			//2DにするんだったらXの表記を消してもらって横移動だけで…
			//_position.x -= (stick.y / 2.5f);


			_position.z += (stick.x / 2.5f);




			//キーボードの右を押されたときキャラを右に動かす
			if (Input::IsKey(DIK_RIGHT))
			{
				_position.z += 0.5;
			}
			//キーボードの左を押されたときキャラを左に動かす
			if (Input::IsKey(DIK_LEFT))
			{
				_position.z -= 0.5;
			}
			//キーボードのWを押されたときキャラの向いてる方向に進ませる
			if (Input::IsKey(DIK_UP))
			{
				_position = _position + move;
			}
			//キーボードのSを押されたときキャラの向いてる方向に下がらせる
			if (Input::IsKey(DIK_DOWN))
			{
				_position = _position - move;
			}
			//キーボードのEを押されたときキャラが右回転する
			if (Input::IsKey(DIK_E))
			{
				_rotate.y += Rotate_;
			}
			//キーボードのRを押されたときキャラが左回転する
			if (Input::IsKey(DIK_R))
			{
				_rotate.y -= Rotate_;
			}

			// 指定範囲を超えないようにする
			if (_position.x < -20.0f)
			{
				_position.x = -20.0f;
			}
			if (_position.z < -20.0f)
			{
				_position.z = -20.0f;
			}
			if (20.0f < _position.x)
			{
				_position.x = 20.0f;
			}
			if (20.0f < _position.z)
			{
				_position.z = 20.0f;
			}
			//ここまで//////////////////

			//Zキーでジャブ
			if (Input::IsKeyDown(DIK_Z) || Input::IsPadButtonDown(XINPUT_GAMEPAD_X, 0))
			{
				if ((Model::GetAnimFrame(_hModel) > 55 || Model::GetAnimFrame(_hModel) < 45)
					&& (Model::GetAnimFrame(_hModel) < 60 || Model::GetAnimFrame(_hModel) > 73)//ストレート中以外
					&& (Model::GetAnimFrame(_hModel) < 135 || Model::GetAnimFrame(_hModel) > 153)//フック中以外
					&& (Model::GetAnimFrame(_hModel) < 230 || Model::GetAnimFrame(_hModel) > 250))//アッパー中以外
				{
					Model::SetAnimFrame(_hModel, 45, 55, 0.5f);

					//ハンド呼び出し
					Chara_Hand_decision_jab* Ppunch_jab =
						CreateGameObject<Chara_Hand_decision_jab>(FindObject("PlayScene"));

					//拳から攻撃判定を表示するための関数
					//レフトハンド参照
					D3DXVECTOR3 shotPos = Model::GetBonePosition(_hModel, "Character1_LeftHand");
					//レフトフィンガーベース参照
					D3DXVECTOR3 PunchRoot = Model::GetBonePosition(_hModel, "Character1_LeftFingerBase");
					//パンチの出し方？
					Ppunch_jab->Punch(shotPos, shotPos - PunchRoot);
				}
			}

			if (Model::GetAnimFrame(_hModel) == 55)
			{
				Model::SetAnimFrame(_hModel, 20, 40, 0.5f);//待機モーション
			}

			//Xキーは右ストレートでぶっとばす
			if (Input::IsKeyDown(DIK_X) || Input::IsPadButtonDown(XINPUT_GAMEPAD_Y, 0))
			{
				/*if ((Model::GetAnimFrame(_hModel) > 73 || Model::GetAnimFrame(_hModel) < 60)
					&& (Model::GetAnimFrame(_hModel) > 50 || Model::GetAnimFrame(_hModel) < 45))*///ジャブのフレーム

				//ジャブからしか出ない版
				//後半から
				if (Model::GetAnimFrame(_hModel) < 55 && Model::GetAnimFrame(_hModel) > 50)//ジャブのフレーム
				{
					Model::SetAnimFrame(_hModel, 60, 73, 0.5f);
					IGameObject* playScene = FindObject("PlayScene");
					Chara_Hand_decision* Ppunch = CreateGameObject<Chara_Hand_decision>(FindObject("PlayScene"));

					//拳から攻撃判定を表示するための関数
					//ライトハンド参照
					D3DXVECTOR3 shotPos = Model::GetBonePosition(_hModel, "Character1_RightHand");
					//ライトフィンガーベース参照
					D3DXVECTOR3 PunchRoot = Model::GetBonePosition(_hModel, "Character1_RightFingerBase");
					//パンチの出し方？
					Ppunch->Punch(shotPos, shotPos - PunchRoot);

				}
			}
			if (Model::GetAnimFrame(_hModel) == 73)
			{
				Model::SetAnimFrame(_hModel, 20, 40, 0.5f);//待機モーション
			}

			//フック
			if (Input::IsKeyDown(DIK_V) || Input::IsPadButtonDown(XINPUT_GAMEPAD_B, 0))
			{

				//記述途中なので判定おかしいです
				/*if (Model::GetAnimFrame(_hModel) > 135 || Model::GetAnimFrame(_hModel) < 153
					&&( Model::GetAnimFrame(_hModel) > 68 || Model::GetAnimFrame(_hModel) < 60)) *///ストレートのフレーム中らしい

				//ストレートからしかコンボででなくしたやつ
				//ストレートの後半からしか出ないぞ！
				if ((Model::GetAnimFrame(_hModel) < 73 && Model::GetAnimFrame(_hModel) > 66))
				{
					Model::SetAnimFrame(_hModel, 135, 153, 1.0f);

					//ハンド呼び出し
					Chara_Hand_decision_hook* Ppunch_hook =
						CreateGameObject<Chara_Hand_decision_hook>(FindObject("PlayScene"));

					//拳から攻撃判定を表示するための関数
					//ライトハンド参照
					D3DXVECTOR3 shotPos = Model::GetBonePosition(_hModel, "Character1_RightHand");
					//ライトフィンガーベース参照
					D3DXVECTOR3 PunchRoot = Model::GetBonePosition(_hModel, "Character1_RightFingerBase");
					//パンチの出し方
					Ppunch_hook->Punch(shotPos, shotPos - PunchRoot);
				}
			}
			if (Model::GetAnimFrame(_hModel) == 153)
			{
				Model::SetAnimFrame(_hModel, 20, 40, 0.5f);//待機モーション
			}

			//アッパー
			if (Input::IsKeyDown(DIK_B) || Input::IsPadButtonDown(XINPUT_GAMEPAD_A, 0))
			{
				/*if (Model::GetAnimFrame(_hModel) > 230 || Model::GetAnimFrame(_hModel) < 250)
				{*/

				//ジャブからの派生のみ
				if (Model::GetAnimFrame(_hModel) < 55 && Model::GetAnimFrame(_hModel) > 50)//ジャブのフレーム
				{
					Model::SetAnimFrame(_hModel, 230, 250, 1.0f);


					//ハンド呼び出し
					Chara_Hand_decision_upper* Ppunch_upper =
						CreateGameObject<Chara_Hand_decision_upper>(FindObject("PlayScene"));

					//拳から攻撃判定を表示するための関数
					//ライトハンド参照
					D3DXVECTOR3 shotPos = Model::GetBonePosition(_hModel, "Character1_RightHand");
					//ライトフィンガーベース参照
					D3DXVECTOR3 PunchRoot = Model::GetBonePosition(_hModel, "Character1_RightFingerBase");
					//パンチの出し方？
					Ppunch_upper->Punch(shotPos, shotPos - PunchRoot);
				}
			}
			if (Model::GetAnimFrame(_hModel) == 250)
			{
				Model::SetAnimFrame(_hModel, 20, 40, 0.5f);//待機モーション
			}
		}

		//ガード関係////////////////////////////////////////////////
		//Cキーおしっぱでガード
		if (Input::IsKey(DIK_C) ||
			(Input::IsPadButton(XINPUT_GAMEPAD_LEFT_SHOULDER, 0) &&
				Input::IsPadButton(XINPUT_GAMEPAD_RIGHT_SHOULDER, 0)))
		{
			if (Model::GetAnimFrame(_hModel) > 105 || Model::GetAnimFrame(_hModel) < 95)
			{
				Model::SetAnimFrame(_hModel, 95, 105, 0.5f);
			}
			guardpoint = true;
			((PlayScene*)GetParent())->GetBar_1p()->CSetGuard(true);
		}
		if (Model::GetAnimFrame(_hModel) == 100)
		{
			Model::SetAnimFrame(_hModel, 100, 105, 0.5f);//待機モーション
		}
		//ガードを話した時
		if (Input::IsKeyUp(DIK_C) ||
			Input::IsPadButtonUp(XINPUT_GAMEPAD_LEFT_SHOULDER, 0) ||
			Input::IsPadButtonUp(XINPUT_GAMEPAD_RIGHT_SHOULDER, 0))
		{
			Model::SetAnimFrame(_hModel, 105, 110, 0.5f);
			guardpoint = false;
			((PlayScene*)GetParent())->GetBar_1p()->CSetGuard(false);
		}

		if (Model::GetAnimFrame(_hModel) == 110)
		{
			Model::SetAnimFrame(_hModel, 20, 40, 0.5f);//待機モーション
		}
		//ガード関係おわり/////////////////////////////////////
	}

	////////////////////////ダウン↓//////////////////////////
	if (Input::IsKeyUp(DIK_N) || down_ == true)
	{
		Model::SetAnimFrame(_hModel, 155, 190, 0.5f);
		//ダウンを行ったので無限ループ解除用
		down_ = false;
		//ダウン中は判定をなくす関数
		downjudge = true;
		//ガード中に倒れた時用のガードの措置
		guardpoint = false;
		//ダウン中に時間を止める関数
		((PlayScene*)GetParent())->downtime(true);
	}
	//倒れ継続
	if (Model::GetAnimFrame(_hModel) == 190)
	{
		Model::SetAnimFrame(_hModel, 190, 191, 0.5f);//待機モーション
	}

	//倒れているときにXかボタンのYで復帰
	if (Model::GetAnimFrame(_hModel) <= 190 && downjudge == true && (Input::IsKeyUp(DIK_X) || Input::IsPadButtonDown(XINPUT_GAMEPAD_Y, 0)))
	{
		Model::SetAnimFrame(_hModel, 191, 200, 0.5f);//起き上がりモーション

		//時間を進める
		((PlayScene*)GetParent())->downtime(false);
	}
	//復帰
	if (Model::GetAnimFrame(_hModel) == 200)
	{
		Model::SetAnimFrame(_hModel, 20, 40, 0.5f);//待機モーション
		downjudge = false;
	}
	/////////////////ダウン表記おわり///////////////////////////////


	//ノックアウト(仮)
	if (Input::IsKeyUp(DIK_M) || tdown_ == true)
	{
		Model::SetAnimFrame(_hModel, 201, 220, 0.5f);
		//三回ダウンの判定を一応消しておく
		tdown_ = false;
		downjudge = true;
		//ダウン中に時間を止める関数
		((PlayScene*)GetParent())->downtime(true);
	}
	if (Model::GetAnimFrame(_hModel) == 220)
	{
		Model::SetAnimFrame(_hModel, 221, 300, 0.5f);//待機モーション
		//3つたまったらRESULTへ変更
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		//changeScebeの引数nextをRESULTに
		pSceneManager->ChangeScene(SCENE_ID_RESULT_ENEMY);


	}
}

//描画
void Chara::Draw()
{
	Model::SetMatrix(_hModel, _worldMatrix);
	Model::Draw(_hModel);
}

//開放
void Chara::Release()
{
}

//何かに当たった
void Chara::OnCollision(IGameObject * pTarget)
{
	//ダウン中は判定を通さない
	if (downjudge == false)
	{
		//弾に当たった
		if (pTarget->GetObjectName() == "Enemy_Hand_decision")
		{
			//ダメージを受けたい
			//((Gauge_bar_2p*)pTarget)->SetHit(true);
			//ペアレントで自分の親を生成。playsceneからGetBarをキャストして呼び出し、
			//CSethitを呼びだした
			((PlayScene*)GetParent())->GetBar_1p()->CSetHit(true);
			//相手（弾）を消す
			pTarget->KillMe();
			
		}
		//ジャブに当たった
		if (pTarget->GetObjectName() == "Enemy_Hand_decision_jab")
		{
			//ダメージを受けたい
			((PlayScene*)GetParent())->GetBar_1p()->CJobSetHit(true);
			//相手（弾）を消す
			pTarget->KillMe();
		}
		//フックに当たった
		if (pTarget->GetObjectName() == "Enemy_Hand_decision_hook")
		{
			//ダメージを受けたい
			((PlayScene*)GetParent())->GetBar_1p()->CHookSetHit(true);
			//相手（弾）を消す
			pTarget->KillMe();
		}

		//アッパーに当たった
		if (pTarget->GetObjectName() == "Enemy_Hand_decision_upper")
		{
			//ダメージを受けたい
			((PlayScene*)GetParent())->GetBar_1p()->CUpperSetHit(true);
			//相手（弾）を消す
			pTarget->KillMe();
		}
		
		//ガード中に攻撃を食らうと少し後ろにノックバックする判定
		if (guardpoint == true
			&& (pTarget->GetObjectName() == "Enemy_Hand_decision"
			|| pTarget->GetObjectName() == "Enemy_Hand_decision_jab"
			|| pTarget->GetObjectName() == "Enemy_Hand_decision_hook"
			|| pTarget->GetObjectName() == "Enemy_Hand_decision_upper"))
		{
			_position = _position - move;
		}

	}
	//すり抜けないように
	if (pTarget->GetObjectName() == "Enemy")
	{
		_position.z -= (stick.x / 2.5f);
	}
}
