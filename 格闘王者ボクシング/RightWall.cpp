#include "RightWall.h"
#include "Engine/gameObject/Camera.h"

//コンストラクタ
RightWall::RightWall(IGameObject * parent)
	:IGameObject(parent, "RightWall")
{
}

//デストラクタ
RightWall::~RightWall()
{
}

//初期化
void RightWall::Initialize()
{
	BoxCollider* collision = new BoxCollider(D3DXVECTOR3(22, 6, 0), D3DXVECTOR3(3, 11, 40));//1Pから見て右
	//当たり判定読み込み
	AddCollider(collision);
}

//更新
void RightWall::Update()
{
}

//描画
void RightWall::Draw()
{
}

//開放
void RightWall::Release()
{
}