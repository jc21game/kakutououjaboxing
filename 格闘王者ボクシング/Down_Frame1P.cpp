#include "Down_Frame1P.h"
#include "Engine/ResouceManager/Image.h"

//コンストラクタ
Down_Frame1P::Down_Frame1P(IGameObject * parent)
	:IGameObject(parent, "Down_Frame1P"), _hPict(-1)
{
}

//デストラクタ
Down_Frame1P::~Down_Frame1P()
{
}

//初期化
void Down_Frame1P::Initialize()
{
	//画像データのロード
	_hPict = Image::Load("data/Down_gauge_frame.png");
	assert(_hPict >= 0);
	//大きさを変える
	_scale = D3DXVECTOR3(0.25f, 0.25f, 0.25f);
	//位置を変える
	_position = D3DXVECTOR3(10, 90, 0);
}

//更新
void Down_Frame1P::Update()
{
}

//描画
void Down_Frame1P::Draw()
{
	Image::SetMatrix(_hPict, _worldMatrix);
	Image::Draw(_hPict);
}

//開放
void Down_Frame1P::Release()
{
}