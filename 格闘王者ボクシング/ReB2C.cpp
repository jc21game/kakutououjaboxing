#include "ReB2C.h"
#include "Engine/ResouceManager/Image.h"

//コンストラクタ
ReB2C::ReB2C(IGameObject * parent)
	:IGameObject(parent, "ReB2C"), _hPict(-1)
{
}

//デストラクタ
ReB2C::~ReB2C()
{
}

//初期化
void ReB2C::Initialize()
{
	//画像データのロード
	_hPict = Image::Load("data/Testbutton2.png");
	assert(_hPict >= 0);
	//大きさを変える
	_scale = D3DXVECTOR3(1.0f, 1.0f, 1.0f);
	//位置を変える
	_position = D3DXVECTOR3(100.0f, 550.0f, 0);
}

//更新
void ReB2C::Update()
{
}

//描画
void ReB2C::Draw()
{
	Image::SetMatrix(_hPict, _worldMatrix);
	Image::Draw(_hPict);
}

//開放
void ReB2C::Release()
{
}