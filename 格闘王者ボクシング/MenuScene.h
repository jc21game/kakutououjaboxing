#pragma once

#include "Engine/global.h"

//■■シーンを管理するクラス
class MenuScene : public IGameObject
{

	int hPict_;

	int testsound_;
public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	MenuScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};
