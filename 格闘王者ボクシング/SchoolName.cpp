#include "SchoolName.h"
#include "Engine/ResouceManager/Image.h"
#include <chrono>
#include <thread>


//コンストラクタ
SchoolName::SchoolName(IGameObject * parent)
	:IGameObject(parent, "SchoolName"), SchoolName_hPict(-1)//初期化
{
}

//デストラクタ
SchoolName::~SchoolName()
{
}

//初期化
void SchoolName::Initialize()
{

	//画像データのロード
	SchoolName_hPict = Image::Load("data/SchoolName.png"); //学校名
	assert(SchoolName_hPict >= 0);
	//大きさを変える(元の画像より小さくした)
	_scale = D3DXVECTOR3(0.3f, 0.3f, 0.3f);

	//ポジション
	_position = D3DXVECTOR3(500, -1000, 0);

	
}

//更新
void SchoolName::Update()
{
	
	if(_position.y <= 0)
	{
		_position.y += 3.0f;
		
	}

	if (_position.y >= 0)
	{
		std::this_thread::sleep_for(std::chrono::seconds(2));      // 2 秒
		//タイトルシーンへ切り替わる
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_TITLE);
	}
}

//描画
void SchoolName::Draw()
{
	//画像を描画する
	Image::SetMatrix(SchoolName_hPict, _worldMatrix);
	Image::Draw(SchoolName_hPict);
}

//開放
void SchoolName::Release()
{
}