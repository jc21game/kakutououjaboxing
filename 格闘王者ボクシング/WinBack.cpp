#include "WinBack.h"
#include "Engine/ResouceManager/Image.h"
#include "Engine/ResouceManager/Model.h"

//コンストラクタ
WinBack::WinBack(IGameObject * parent)
	:IGameObject(parent, "WinBack"), _hPict(-1), _hModel(-1)
{
}

//デストラクタ
WinBack::~WinBack()
{
}

//初期化
void WinBack::Initialize()
{
	//画像データのロード
	_hPict = Image::Load("data/WinBack.png");
	assert(_hPict >= 0);
	//_position.z = 1.0f;
	
	//大きさを変える
	//_scale = D3DXVECTOR3(1.3f, 1.3f, 1.0f);
	//位置を変える
	//_position = D3DXVECTOR3(900.0f, 550.0f, 0);
}

//更新
void WinBack::Update()
{
}

//描画
void WinBack::Draw()
{
	Image::SetMatrix(_hPict, _worldMatrix);
	Image::Draw(_hPict);

	
}

//開放
void WinBack::Release()
{
}