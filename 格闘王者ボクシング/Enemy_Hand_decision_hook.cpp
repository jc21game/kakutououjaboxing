#include "Enemy_Hand_decision_hook.h"
#include "Engine/ResouceManager/Model.h"
#include "Chara.h"
#include "Enemy.h"

//コンストラクタ
Enemy_Hand_decision_hook::Enemy_Hand_decision_hook(IGameObject * parent)
	:IGameObject(parent, "Enemy_Hand_decision_hook"), _move(D3DXVECTOR3(0, 0, 0)), _dz(0.5f),
	SPEED(0.5f)
{
}

//デストラクタ
Enemy_Hand_decision_hook::~Enemy_Hand_decision_hook()
{
}

//初期化
void Enemy_Hand_decision_hook::Initialize()
{
	//衝突判定
	SphereCollider* collision = new SphereCollider(D3DXVECTOR3(2.0, 0.0, -1.0), 0.5f);
	AddCollider(collision);
}

//更新
void Enemy_Hand_decision_hook::Update()
{

	//キーボードの右を押されたときキャラを右に動かす
	if (Input::IsKey(DIK_RIGHT))
	{
		_position.x += 0.3;
	}
	//キーボードの左を押されたときキャラを左に動かす
	if (Input::IsKey(DIK_LEFT))
	{

		_position.x -= 0.3;
	}
	//キーボードの上を押されたときキャラを上に動かす
	if (Input::IsKey(DIK_UP))
	{
		_position.z += 0.5;
	}
	//キーボードの下を押されたときキャラを下に動かす
	if (Input::IsKey(DIK_DOWN))
	{

		_position.z -= 0.5;
	}

	//攻撃判定の移動
	_position += _move;
	_move.z -= _dz;
	_dz += 0.01f;

	//こぶし判定くらいのところに飛ばしたら消える
	if (_dz > 0.55)
	{
		KillMe();
	}
}

//描画
void Enemy_Hand_decision_hook::Draw()
{
}

//開放
void Enemy_Hand_decision_hook::Release()
{
}
//発射
void Enemy_Hand_decision_hook::Punch(D3DXVECTOR3 position, D3DXVECTOR3 direction)
{
	//位置
	_position = position;

	//移動は、引数で渡された方向にSPEEDの速さ
	D3DXVec3Normalize(&_move, &direction);
	_move *= SPEED;
}

//何かに当たった
void Enemy_Hand_decision_hook::OnCollision(IGameObject * pTarget)
{

}