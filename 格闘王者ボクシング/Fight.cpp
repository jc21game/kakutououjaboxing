#include "Engine/ResouceManager/Image.h"
#include "Fight.h"
#include "Engine/ResouceManager/Audio.h"

//コンストラクタ
Fight::Fight(IGameObject * parent)
	: IGameObject(parent, "Fight"), fight_hPict(-1), _hSound(-1)
{
}


//初期化
void Fight::Initialize()
{


	//画像データのロード
	fight_hPict = Image::Load("Data/FIGHT.png");
	assert(fight_hPict >= 0);

	//大きさを変える,元の画像より小さくなっている
	_scale = D3DXVECTOR3(0.2f, 0.2f, 0.2f);

	//位置を変えている
	_position = D3DXVECTOR3(630.0f, 550.0f, 0);

	//当たり判定(箱型コライダー)
	BoxCollider* collision = new BoxCollider(D3DXVECTOR3(0, -150, 0), D3DXVECTOR3(3.0f, 1.0f, 1.0f));
	AddCollider(collision);

	//サウンドデータのロード
	_hSound = Audio::Load("Data/Test2.wav");
	assert(_hSound >= 0);

}


//更新
void Fight::Update()
{
}


//描画
void Fight::Draw()
{
	//画像の描画
	Image::SetMatrix(fight_hPict, _worldMatrix);
	Image::Draw(fight_hPict);
}


//開放
void Fight::Release()
{
}

//何かに当たった
void Fight::OnCollision(IGameObject * pTarget)
{
	//UIがFightボタンに当り、かつスペースキーが押された
	if (pTarget->GetObjectName() == "UI" &&Input::IsKey(DIK_SPACE))
	{
		
		    //戦闘準備画面への切り替え
			SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
			pSceneManager->ChangeScene(SCENE_ID_CHOICE);
			//効果音を流す
			//Audio::Play(_hSound);
		
	}

}