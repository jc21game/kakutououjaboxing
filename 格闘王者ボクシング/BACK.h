#pragma once
#include "Engine/GameObject/GameObject.h"

//◆◆◆を管理するクラス
class BACK : public IGameObject
{
	int _hModel;    //画像番号
	int _hPict;

public:
	//コンストラクタ
	BACK(IGameObject* parent);

	//デストラクタ
	~BACK();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};