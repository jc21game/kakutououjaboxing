#pragma once

//インクルード
#include <Windows.h>
#include <d3dx9.h>

//リンカ
#pragma comment(lib,"d3d9.lib")
#pragma comment(lib,"d3dx9.lib")


//画面の描画に関する処理
namespace Direct3D
{
	extern LPDIRECT3DDEVICE9	_pDevice;	//Direct3Dデバイスオブジェクト
	extern float				_aspect;	//スクリーンのアスペクト比	
	extern bool					_isDrawCollision;	//コリジョンを表示するか
	extern bool					_isLighting;		//ライティングするか

	static D3DCOLORVALUE diff[3];          //  ライト色の変数          
	static D3DCOLORVALUE spec[3];          //  ハイライト色の変数          
	static D3DXVECTOR3 dir[3];             //  ライト向きの変数

	//初期化処理
	//引数：hWnd			ウィンドウハンドル
	//引数：screenWidth		スクリーンの幅
	//引数：screenHeight	スクリーンの高さ
	void Initialize(HWND hWnd, int screenWidth, int screenHeight);

	//描画開始
	void BeginDraw();

	//描画終了
	void EndDraw();

	//開放処理
	void Release();
};

