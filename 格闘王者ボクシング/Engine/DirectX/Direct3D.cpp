#include<assert.h>
#include "Direct3D.h"



namespace Direct3D
{
	LPDIRECT3D9			_pD3d = nullptr;	//Direct3Dオブジェクト
	LPDIRECT3DDEVICE9	_pDevice = nullptr;	//Direct3Dデバイスオブジェクト
	float				_aspect = 1.0f;		//スクリーンのアスペクト比	
	bool		_isDrawCollision = false;	//コリジョンを表示するか
	bool		_isLighting= false;			//ライティングするか

	void Initialize(HWND hWnd, int screenWidth, int screenHeight)
	{
		HRESULT hr;

		//Direct3Dオブジェクトの作成
		_pD3d = Direct3DCreate9(D3D_SDK_VERSION);
		assert(_pD3d >= 0);


		//DIRECT3Dデバイスオブジェクトの作成
		D3DPRESENT_PARAMETERS d3dpp;
		ZeroMemory(&d3dpp, sizeof(d3dpp));
		d3dpp.BackBufferFormat = D3DFMT_X8R8G8B8;
		d3dpp.BackBufferCount = 1;
		d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
		d3dpp.Windowed = GetPrivateProfileInt("SCREEN", "FullScreen", 0, ".\\Data\\setup.ini") != 1;
		d3dpp.EnableAutoDepthStencil = TRUE;
		d3dpp.AutoDepthStencilFormat = D3DFMT_D16;
		d3dpp.BackBufferWidth = screenWidth;
		d3dpp.BackBufferHeight = screenHeight;
		d3dpp.hDeviceWindow = hWnd;
		d3dpp.PresentationInterval = D3DPRESENT_INTERVAL_ONE;

		// アンチエイリアシング
		if (GetPrivateProfileInt("RENDER", "AntiAliasing", 0, ".\\Data\\setup.ini") != 0)
		{
			DWORD QualityBackBuffer = 0;
			_pD3d->CheckDeviceMultiSampleType(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, d3dpp.BackBufferFormat, 
				d3dpp.Windowed, D3DMULTISAMPLE_4_SAMPLES, &QualityBackBuffer);
			d3dpp.MultiSampleType = D3DMULTISAMPLE_4_SAMPLES;
			d3dpp.MultiSampleQuality = QualityBackBuffer - 1;
		}

		hr = _pD3d->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd, D3DCREATE_HARDWARE_VERTEXPROCESSING, &d3dpp, &_pDevice);
		assert(_pDevice > 0);


		//アスペクト比
		_aspect = (float)screenWidth / (float)screenHeight;

		//アルファブレンド
		_pDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
		_pDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
		_pDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);

		//ライティング
		_isLighting = GetPrivateProfileInt("RENDER", "Lighting", 0, ".\\Data\\setup.ini") != 0;
		if (_isLighting)
		{
			_pDevice->SetRenderState(D3DRS_LIGHTING, TRUE);

			//ライトの向き　当て方
			//　8/29時点ではキャラが明るすぎる気がするので修正推奨
			dir[0] = D3DXVECTOR3(1.0f, -1.0f, 1.0f);
			dir[1] = D3DXVECTOR3(-1.0f, 1.0f, -1.0f);
			dir[2] = D3DXVECTOR3(-1.0f, -1.0f, 1.0f);

			//  ライトの色の初期化 わかりやすくライトの色を別々な色に設定
			//  後で全部白にする
			diff[0].r = diff[1].g = diff[2].b = 1.0f;
			diff[1].r = diff[0].g = diff[0].b = 1.0f;
			diff[2].r = diff[2].g = diff[1].b = 1.0f;

			//  ハイライトの色の初期化			
			for (int i = 0; i < 3; i++)
			{
				spec[i].r = spec[i].g = spec[i].b = 1.0f;
			}

			int index;	//ライト管理変数
			D3DLIGHT9 lightState;	//ライトの設定
			ZeroMemory(&lightState, sizeof(D3DLIGHT9));	


			//３つのライトの管理
			for (index = 0; index < 3; index++)
			{

				//  パラメータセット：ライトの色
				lightState.Diffuse = diff[index];

				//  パラメータセット：ハイライトの色
				lightState.Specular = spec[index];

				//タイプ
				lightState.Type = D3DLIGHT_DIRECTIONAL;			//ライトの種類

				//向き
				lightState.Direction = dir[index];  //ライトの方向を3Dベクトルで設定

				//  ライト情報のセット
				_pDevice->SetLight(index, &lightState);	//ライトに情報をセット
				_pDevice->LightEnable(index, TRUE);		//ライト有効
			}
			//  Direct3Dによるライティングを有効にする
			_pDevice->SetRenderState(D3DRS_LIGHTING, TRUE);
		}
		else
		{
			_pDevice->SetRenderState(D3DRS_LIGHTING, FALSE);
		}

		//カメラ
		D3DXMATRIX view, proj;
		D3DXMatrixLookAtLH(&view, &D3DXVECTOR3(0, 5, -10), &D3DXVECTOR3(0, 0, 0), &D3DXVECTOR3(0, 1, 0));
		D3DXMatrixPerspectiveFovLH(&proj, D3DXToRadian(60), (float)screenWidth / screenHeight, 0.5f, 1000.0f);
		_pDevice->SetTransform(D3DTS_VIEW, &view);
		_pDevice->SetTransform(D3DTS_PROJECTION, &proj);

		//コリジョン表示するか
		_isDrawCollision = GetPrivateProfileInt("DEBUG", "ViewCollider", 0, ".\\Data\\setup.ini") != 0;


	}

	//描画開始
	void BeginDraw()
	{
		//画面をクリア
		_pDevice->Clear(0, nullptr, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, D3DCOLOR_XRGB(0, 0, 0), 1.0f, 0);

		//描画開始
		_pDevice->BeginScene();
	}

	//描画終了
	void EndDraw()
	{
		//描画終了
		_pDevice->EndScene();

		//スワップ
		_pDevice->Present(nullptr, nullptr, nullptr, nullptr);
	}

	//開放処理
	void Release()
	{
		_pDevice->Release();
		_pD3d->Release();
	}

}