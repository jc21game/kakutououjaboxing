#include "sceneManager.h"
#include "../global.h"
#include "../ResouceManager/Model.h"
#include "../ResouceManager/Image.h"

#include "../../BootScene.h"
#include "../../SplashScene.h"
#include "../../TitleScene.h"
#include "../../MenuScene.h"
//#include "../../OptionScene.h"
#include "../../ChoiceScene.h"
#include "../../PlayScene.h"
#include "../../ResultScene.h"
#include "../../CharaResultScene.h"
#include "../../EnemyResultScene.h"


//コンストラクタ
SceneManager::SceneManager(IGameObject * parent)
	: IGameObject(parent, "SceneManager")
{
}

//初期化
void SceneManager::Initialize()
{
	//最初のシーンを準備
	_currentSceneID = SCENE_ID_SPLASH;
	_nextSceneID = SCENE_ID_SPLASH;
	CreateGameObject<SplashScene>(this);
}

//更新
void SceneManager::Update()
{
	//次のシーンが現在のシーンと違う　＝　シーンを切り替えなければならない
	if (_currentSceneID != _nextSceneID)
	{
		//そのシーンのオブジェクトを全削除
		KillAllChildren();

		//ロードしたデータを全削除
		Model::AllRelease();
		Image::AllRelease();

		//次のシーンを作成
		switch (_nextSceneID) 
		{
		case SCENE_ID_BOOT: CreateGameObject<BootScene>(this); break;

		//スプラッシュシーンの作成
		case SCENE_ID_SPLASH: CreateGameObject<SplashScene>(this); break;

		//タイトルシーンの作成
		case SCENE_ID_TITLE: CreateGameObject<TitleScene>(this); break;

		//メニューシーンの作成
		//case SCENE_ID_MENU: CreateGameObject<MenuScene>(this); break;
		
		//オプションシーンの作成
		//case SCENE_ID_OPTION: CreateGameObject<OptionScene>(this); break;
		
		//戦闘準備シーンの作成
		case SCENE_ID_CHOICE: CreateGameObject<ChoiceScene>(this); break;

		//プレイシーン(バトルシーン)の作成
		case SCENE_ID_FIGHT: CreateGameObject<PlayScene>(this); break;

		//リザルトシーンの作成
		case SCENE_ID_RESULT: CreateGameObject<ResultScene>(this); break;

		case SCENE_ID_RESULT_CHARA: CreateGameObject<CharaResultScene>(this); break;

		case SCENE_ID_RESULT_ENEMY: CreateGameObject<EnemyResultScene>(this); break;
		
		}

		_currentSceneID = _nextSceneID;
	}
}

//描画
void SceneManager::Draw()
{
}

//開放
void SceneManager::Release()
{
}

//シーン切り替え（実際に切り替わるのはこの次のフレーム）
void SceneManager::ChangeScene(SCENE_ID next)
{
	_nextSceneID = next;
}