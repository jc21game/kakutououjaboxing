#pragma once

enum SCENE_ID 
{
	SCENE_ID_BOOT = 0,

	//スプラッシュシーンのID追加
	SCENE_ID_SPLASH,
	//タイトルシーンのID追加
	SCENE_ID_TITLE,
	//メニューシーンのID追加
	SCENE_ID_MENU,
	//戦闘準備シーンのID追加
	SCENE_ID_CHOICE,
	//Test1(Fight)シーンのID追加
	SCENE_ID_FIGHT,
	//Test2(Optio)シーンのID追加
	SCENE_ID_OPTION,
	//リザルトのそれぞれのシーンの追加
	SCENE_ID_RESULT,		//1
	SCENE_ID_RESULT_CHARA,	//2
	SCENE_ID_RESULT_ENEMY,	//3

};