#pragma once

#include "Engine/global.h"

//■■シーンを管理するクラス
class EnemyResultScene : public IGameObject
{

	int _hPict;    //画像番号
	int _hSound;
	//カウントの追加
	int butp1;
	int butp2; 

public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	EnemyResultScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};
