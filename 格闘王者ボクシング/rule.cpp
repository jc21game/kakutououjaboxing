#include "rule.h"
#include "Engine/ResouceManager/Image.h"



//コンストラクタ
rule::rule(IGameObject * parent)
	:IGameObject(parent, "rule"), hPict_(-1)
{
}

//デストラクタ
rule::~rule()
{
}

//初期化
void rule::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("data/rule.png");
	assert(hPict_ >= 0);
	

	//_position = D3DXVECTOR3(-250, -200.0f, 0);
	//べつの画像用
	_position = D3DXVECTOR3(150.0f, 50.0f, 0);
}

//更新
void rule::Update()
{
}

//描画
void rule::Draw()
{
	Image::SetMatrix(hPict_, _worldMatrix);
	Image::Draw(hPict_);
}

//開放
void rule::Release()
{
}