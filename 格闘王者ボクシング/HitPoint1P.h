#pragma once
#include "Engine/GameObject/GameObject.h"

//◆◆◆を管理するクラス
class HitPoint1P : public IGameObject
{
	int _hPict;    //画像番号
public:
	//コンストラクタ
	HitPoint1P(IGameObject* parent);

	//デストラクタ
	~HitPoint1P();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};