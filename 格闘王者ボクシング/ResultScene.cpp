#include "ResultScene.h"
#include "Engine/DirectX/Input.h"
#include "Engine/ResouceManager/Image.h"
//#include "Engine/ResouceManager/Audio.h"
#include "ReB1.h"
#include "ReB1C.h"
#include "ReB2.h"
#include "ReB2C.h"
#include "DrawEnd.h"
#include "DrawBack.h"

//コンストラクタ
ResultScene::ResultScene(IGameObject * parent)
	: IGameObject(parent, "ResultScene"), _hPict(-1), _hSound(-1),butp1(0), butp2(0)
{
}

//初期化
void ResultScene::Initialize()
{
	CreateGameObject<DrawBack>(this);
	CreateGameObject<DrawEnd>(this);
	CreateGameObject<ReB1>(this);
	CreateGameObject<ReB2>(this);
}

//更新
void ResultScene::Update()
{
	//両者が特定のボタンを押すとタイトルに戻るように
	if (Input::IsKeyDown(DIK_SPACE) || Input::IsPadButtonDown(XINPUT_GAMEPAD_A, 0))
	{
		butp1 = 1;
		//Audio::Play(_hSound);
		CreateGameObject<ReB1C>(this);
	}
	
	if (Input::IsKeyDown(DIK_RETURN) || Input::IsPadButtonDown(XINPUT_GAMEPAD_A, 1))
	{
		butp2 = 1;
		//Audio::Play(_hSound);
		CreateGameObject<ReB2C>(this);
	}
	
	if ( (butp1 == 1) && (butp2 == 1) )
	{

		butp1 = 0;
		butp2 = 0;
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		//changeScebeの引数nextをMENUに
		pSceneManager->ChangeScene(SCENE_ID_TITLE);
	}
}

//描画
void ResultScene::Draw()
{
	Image::SetMatrix(_hPict, _worldMatrix);
	Image::Draw(_hPict);
}

//開放
void ResultScene::Release()
{
}
