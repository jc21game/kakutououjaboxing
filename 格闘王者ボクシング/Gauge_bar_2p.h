#pragma once
#include "Engine/GameObject/GameObject.h"

//◆◆◆を管理するクラス
class Gauge_bar_2p : public IGameObject
{
	int _hPict;    //画像番号
	int stanMAX = 0; //体力100%

	bool hit_;		//falseとtrueで判断
	bool jab_hit_;	//ジャブ版
	bool hook_hit_;	//フック版
	bool upper_hit_;//アッパー版

	bool guard_;//ガード時の判定



public:
	
	//コンストラクタ
	Gauge_bar_2p(IGameObject* parent);

	//デストラクタ
	~Gauge_bar_2p();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	void HitStop();

	void SetHit(bool hit)
	{
		hit_ = hit;
	}

	void JobSetHit(bool jab_hit)
	{
		jab_hit_ = jab_hit;
	}

	void HookSetHit(bool hook_hit)
	{
		hook_hit_ = hook_hit;
	}

	void UpperSetHit(bool upper_hit)
	{
		upper_hit_ = upper_hit;
	}

	void SetGuard(bool guard)
	{
		guard_ = guard;
	}
};