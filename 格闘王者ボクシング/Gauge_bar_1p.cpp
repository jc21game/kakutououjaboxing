#include "Gauge_bar_1p.h"
#include "Engine/ResouceManager/Image.h"
#include "Playscene.h"
#include "Down1P.h"

//1%当たりのダメージ量
#define DAMAGE 0.009f;
//1%のポジション移動量
#define DAMAGE_POS 1.2;
//ガード時のダメージ
#define GA_DAMAGE 0.0018f;
//ガード時のポジション
#define GA_DAMAGE_POS 0.24;

//コンストラクタ
Gauge_bar_1p::Gauge_bar_1p(IGameObject * parent)
	:IGameObject(parent, "Gauge_bar_1p"), _hPict(-1)
	, chit_(false), cjab_hit_(false), chook_hit_(false), cupper_hit_(false),
	cguard_(false)
{
}

//デストラクタ
Gauge_bar_1p::~Gauge_bar_1p()
{
}

//初期化
void Gauge_bar_1p::Initialize()
{
	//モデルデータのロード
	_hPict = Image::Load("Data/gauge_bar.png");
	assert(_hPict >= 0);
	//大きさ、場所の設定
	_scale = D3DXVECTOR3(0.0, 0.9, 0.9);
	_position = D3DXVECTOR3(20, -170, 0);


}

//更新
void Gauge_bar_1p::Update()
{

	/*if (guard_ == true) {
	
	}*/



	if (Input::IsKeyDown(DIK_3))
	{
		//3キーを押すたびおおよそ10%増える
		chit_ = true;
	}

	//hit_がtrueを押すたびにおおよそ10%増える
	//ガードで一定数カット
	if (chit_ == true && cguard_ == true) {
		_scale.x += 10 * GA_DAMAGE;
		_position.x -= 10 * GA_DAMAGE_POS;
	}
	else if (chit_ == true) {
		_scale.x += 10 * DAMAGE;
		_position.x -= 10 * DAMAGE_POS;
	}

	//ジャブでだいたい5%
	if (cjab_hit_ == true && cguard_ == true) {
		_scale.x += 5 * GA_DAMAGE;
		_position.x -= 5 * GA_DAMAGE_POS;
	}
	else if (cjab_hit_ == true) {
	_scale.x += 5 * DAMAGE;
	_position.x -= 5 * DAMAGE_POS;
	}

	//フックでだいたい15％　つよい
	if (chook_hit_ == true && cguard_ == true) {
		_scale.x += 15 * GA_DAMAGE;
		_position.x -= 15 * GA_DAMAGE_POS;
	}
	else if (chook_hit_ == true) {
		_scale.x += 15 * DAMAGE;
		_position.x -= 15 * DAMAGE_POS;
	}
	//アッパーでだいたい20％　めちゃんこつおい
	if (cupper_hit_ == true && cguard_ == true) {
		_scale.x += 20 * GA_DAMAGE;
		_position.x -= 20 * GA_DAMAGE_POS;
	}
	else if (cupper_hit_ == true) {
		_scale.x += 20 * DAMAGE;
		_position.x -= 20 * DAMAGE_POS;
	}

	// 100に固定されているときに4を押した場合0に戻す処理(仮)
		if (stanMAX == 1 && (chit_ == true || cupper_hit_ == true || chook_hit_ == true || cjab_hit_ == true))
		{
			CHitStop();
		}

	//判定のリセット
	chit_ = cjab_hit_ = chook_hit_ = cupper_hit_ = false;

	//体力が100%を超えた場合100に固定する処理
	if (_scale.x >= 0.9 && stanMAX == 0)
	{
		_scale.x = 0.9f;
		_position.x = -100;
		stanMAX = 1;
		
	}
	//0%より大きかったらかつガード中以外は少しずつ減少するようにした
	if (_scale.x > 0.0f && _scale.x < 0.9f && cguard_ == false)
	{
		_scale.x -= 0.0009;
		_position.x += 0.12;
	}
}

//描画
void Gauge_bar_1p::Draw()
{
	Image::SetMatrix(_hPict, _worldMatrix);
	Image::Draw(_hPict);
}

//開放
void Gauge_bar_1p::Release()
{
}

void Gauge_bar_1p::CHitStop()
{
	_scale.x = 0.0f;
	_position.x = 20;
	stanMAX = 0;
	((PlayScene*)GetParent())->GetDown_1p()->SetDown(true);

}
