
#pragma once
#include "Engine/global.h"


//Fightボタンを管理するクラス
class Fight : public IGameObject
{
public:

	//ファイトボタンのUI番号
	int  fight_hPict;
	//サウンド番号
	int _hSound;  

	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	Fight(IGameObject* parent);


	//初期化
	void Initialize() override;


	//更新
	void Update() override;


	//描画
	void Draw() override;


	//開放
	void Release() override;

	//何かに当たった
	//引数：pTarget 当たった相手
	void OnCollision(IGameObject *pTarget) override;
};
