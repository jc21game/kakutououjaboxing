#include "Press.h"
#include "Engine/ResouceManager/Image.h"
#include "assert.h"

//Pressボタンの表示					
//Pressボタンが選択されたらメニューシーンへ切り替え	
//背景の表示



//コンストラクタ
Press::Press(IGameObject * parent)
	:IGameObject(parent, "Press"), _hPict(-1)
{
}

//デストラクタ
Press::~Press()
{
}

//初期化
void Press::Initialize()
{

	//画像データのロード
	_hPict = Image::Load("data/press.png");
	assert(_hPict >= 0);
	//大きさを変える
	_scale = D3DXVECTOR3(0.8f, 0.8f, 0.8f);
	//位置を変える
	_position = D3DXVECTOR3(200.0f,700.0f, 0);

}

//更新
void Press::Update()
{
}

//描画
void Press::Draw()
{
	Image::SetMatrix(_hPict, _worldMatrix);
	Image::Draw(_hPict);
}

//開放
void Press::Release()
{
}