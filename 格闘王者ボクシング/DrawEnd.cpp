#include "DrawEnd.h"
#include "Engine/ResouceManager/Image.h"

//コンストラクタ
DrawEnd::DrawEnd(IGameObject * parent)
	:IGameObject(parent, "DrawEnd"), _hPict(-1)
{
}

//デストラクタ
DrawEnd::~DrawEnd()
{
}

//初期化
void DrawEnd::Initialize()
{
	//画像データのロード
	_hPict = Image::Load("data/DRAW.png");
	assert(_hPict >= 0);
	// 大きさを変える
	_scale = D3DXVECTOR3(2.0f, 2.0f, 2.0f);
	//位置を変える
	_position = D3DXVECTOR3(-500.0f, -900.0f, 0);
}

//更新
void DrawEnd::Update()
{
}

//描画
void DrawEnd::Draw()
{
	Image::SetMatrix(_hPict, _worldMatrix);
	Image::Draw(_hPict);
}

//開放
void DrawEnd::Release()
{
}