#include "Gauge_bar_2p.h"
#include "Engine/ResouceManager/Image.h"
#include "Playscene.h"
#include "Down2P.h"

//コンストラクタ
Gauge_bar_2p::Gauge_bar_2p(IGameObject * parent)
	:IGameObject(parent, "Gauge_bar_2p"), _hPict(-1),
	hit_(false), jab_hit_(false),hook_hit_(false), upper_hit_(false),
	guard_(false)
{
}

//デストラクタ
Gauge_bar_2p::~Gauge_bar_2p()
{
}

//初期化
void Gauge_bar_2p::Initialize()
{
	//モデルデータのロード
	_hPict = Image::Load("Data/gauge_bar.png");
	assert(_hPict >= 0);
	//大きさ、場所の設定
	_scale = D3DXVECTOR3(0.0, 0.9, 0.9);
	_position = D3DXVECTOR3(1570, -170, 0);


}

//更新
void Gauge_bar_2p::Update()
{
	//ガード中のダメージを1/10に(仮)
	/*if (guard_ == true) {
		0.0091 = 0.0091 / 10;
		1.25f = 1.25f / 10;
		guard_cancel = true;

	}
	else if (guard_ == false && guard_cancel == true) {
		0.0091 = 0.0091 * 10;
		1.25f = 1.25f * 10;
		guard_cancel = false;
	}*/


	//デバック用
	if (Input::IsKeyDown(DIK_4))
	{
		//4キーを押すたびおおよそ10%増える
		hit_ = true;
	}

	//hit_がtrueを押すたびにおおよそ10%増える(ストレート)
	//ガード中だと1/5になる
	if (hit_ == true && guard_ == true) {
		_scale.x -= 2 * 0.0091f;
		_position.x += 2 * 1.25f;
	}
	else if (hit_ == true) {
		_scale.x -= 10 * 0.0091f;
		_position.x += 10 * 1.25f;
	}
	//ジャブでだいたい5％
	if (jab_hit_ == true && guard_ == true) {
		_scale.x -= 1 * 0.0091;
		_position.x += 1 * 1.25f;
	}
	else if (jab_hit_ == true) {
		_scale.x -= 5 * 0.0091;
		_position.x += 5 * 1.25f;
	}

	//フックでだいたい15％　つよい
	if (hook_hit_ == true && guard_ == true) {
		_scale.x -= 3 * 0.0091;
		_position.x += 3 * 1.25f;
	}
	else if (hook_hit_ == true) {
		_scale.x -= 15 * 0.0091;
		_position.x += 15 * 1.25f;
	}

	//アッパーでだいたい20％　めちゃんこつよい
	if (upper_hit_ == true && guard_ == true) {
		_scale.x -= 4 * 0.0091;
		_position.x += 4 * 1.25f;
	}
	else if (upper_hit_ == true) {
		_scale.x -= 20 * 0.0091;
		_position.x += 20 * 1.25f;
	}

	//100に固定されているときに4を押した場合0に戻す処理(仮)
	if (stanMAX == 1 && (hit_ == true || upper_hit_ == true || hook_hit_ == true || jab_hit_ == true))
	{
		HitStop();
	}

	//当たった判定のリセット
	hit_ = jab_hit_ = hook_hit_ = upper_hit_ = false;


	//体力が100%を超えた場合100に固定する処理
	if (_scale.x <= -0.91 && stanMAX == 0 )
	{
		_scale.x = -0.91f;
		_position.x = 1695;
		stanMAX = 1;
	}
	//0%より大きかったらかつガード中以外は少しずつ減少するようにした
	if (_scale.x < 0.0f && _scale.x > -0.91f && guard_ == false)
	{
		_scale.x += 0.00091;
		_position.x -= 0.125;
	}
}

//描画
void Gauge_bar_2p::Draw()
{
	Image::SetMatrix(_hPict, _worldMatrix);
	Image::Draw(_hPict);
}

//開放
void Gauge_bar_2p::Release()
{
}


//100に固定されているときに4を押した場合ダウンカウントを1増やし0に戻す処理
void Gauge_bar_2p::HitStop()
{
	_scale.x = 0.0f;
	_position.x = 1570;
	stanMAX = 0;
	((PlayScene*)GetParent())->GetDown_2p()->SetDown(true);
}
