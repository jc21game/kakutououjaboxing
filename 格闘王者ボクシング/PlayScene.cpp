#include "PlayScene.h"
#include "Engine/DirectX/Input.h"
#include "Engine/ResouceManager/Image.h"
#include "Chara.h"
#include "Enemy.h"
#include "Stage.h"
#include "Center.h"
#include "BACK.h"
#include "HitPoint1P.h"
#include "HitPoint2P.h"
#include "Down1P.h"
#include "Down2P.h"
#include "Down_Frame1P.h"
#include "Down_Frame2P.h"
#include "Gauge_bar_1p.h"
#include "Gauge_bar_2p.h"
#include "Engine/DirectX/Text.h"
#include <string>

//コンストラクタ
PlayScene::PlayScene(IGameObject * parent)
	: IGameObject(parent, "PlayScene"), _hPict(-1), pText_(nullptr), 
	cnt1_(0), cnt10_(0), minCnt_(7), frame_(0)
	,Down_Time(false)
{
}

//初期化
void PlayScene::Initialize()
{
	

	//画像データのロード
	/*_hPict = Image::Load("data/haikei.png");
	assert(_hPict >= 0);*/

	//こやつらは孤独な生き物なのでポインタを上げないとこいつらだけでかかわることができない
	pChara = CreateGameObject<Chara>(this);
	pEnemy = CreateGameObject<Enemy>(this);
	CreateGameObject<Stage>(this);
	CreateGameObject<Center>(this);
	CreateGameObject<BACK>(this);
	pDown_1p = CreateGameObject<Down1P>(this);
	pDown_2p = CreateGameObject<Down2P>(this);
	CreateGameObject<Down_Frame1P>(this);
	CreateGameObject<Down_Frame2P>(this);
	pBar_1p = CreateGameObject<Gauge_bar_1p>(this);
	pBar_2p = CreateGameObject<Gauge_bar_2p>(this);
	CreateGameObject<HitPoint1P>(this);
	CreateGameObject<HitPoint2P>(this);
	
	//テキストを作成
	pText_ = new Text("ＭＳ ゴシック", 80);

	sec_[0] = "0";
	sec_[1] = "9";
	sec_[2] = "8";
	sec_[3] = "7";
	sec_[4] = "6";
	sec_[5] = "5";
	sec_[6] = "4";
	sec_[7] = "3";
	sec_[8] = "2";
	sec_[9] = "1";
	sec_[10] = "0";

	min_[0] = "0";
	min_[1] = "9";
	min_[2] = "8";
	min_[3] = "7";
	min_[4] = "6";
	min_[5] = "5";
	min_[6] = "4";
	min_[7] = "3";
	min_[8] = "2";
	min_[9] = "1";
	min_[10] = "0";
}

//更新
void PlayScene::Update()
{
	//デバック用で
	//SPACEを押したら起動するようにする
	if (Input::IsKeyDown(DIK_SPACE)) 
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		//changeScebeの引数nextをRESULTに
		pSceneManager->ChangeScene(SCENE_ID_RESULT);

	}


	//フレームのカウントが60でちょうど1秒

	//ダウンしていないときだけ動くようにする
	if (Down_Time == false)
	{
		frame_++;
	}


	if (frame_ == 60)
	{
		frame_ = 0;
		//**が９より小さければ

		if (minCnt_ <= 10)
		{

			//最初3:00から2:59にする
			if (cnt10_ == 0 && cnt1_ == 0)
			{
				cnt10_ = 5;
				cnt1_ = 0;
				minCnt_++;
			}



			//cnt10は10の位のカウント
			if (cnt10_ <= 10)
			{
				if (cnt10_ == 10 && cnt1_ == 10)
				{
					cnt10_ = 5;
					cnt1_ = 1;
					minCnt_++;
				}

				//1の位のカウント
				else if (cnt1_ < 10)
				{
					cnt1_++;
				}

				else if (cnt1_ == 10)
				{
					cnt1_ = 1;
					cnt10_++;
				}

			}


		}
	}

	//残り時間が0になったらとりあえずリザルトに飛ばす
	//現状は引き分けにする　って　ことで
	if (cnt1_ == 10 && cnt10_ == 10 && minCnt_ == 10)
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		//changeScebeの引数nextをRESULTに
		pSceneManager->ChangeScene(SCENE_ID_RESULT);
	}
}

//描画
void PlayScene::Draw()
{
	Image::SetMatrix(_hPict, _worldMatrix);
	Image::Draw(_hPict);
	//第一、第二引数は位置情報
	pText_->Draw(715,20, min_[minCnt_] + ":" + sec_[cnt10_] + sec_[cnt1_]); //自動で2:59にする
	
}

//開放
void PlayScene::Release()
{
}