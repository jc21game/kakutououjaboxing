
#include "Chara_Hand_decision.h"
#include "Engine/ResouceManager/Model.h"
#include "Chara.h"
#include "Enemy.h"

//コンストラクタ
Chara_Hand_decision::Chara_Hand_decision(IGameObject * parent)
	:IGameObject(parent, "Chara_Hand_decision"),_move(D3DXVECTOR3(0, 0, 0)), _dz(0.5f),
	SPEED(0.5f)
{
}

//デストラクタ
Chara_Hand_decision::~Chara_Hand_decision()
{
}

//初期化
void Chara_Hand_decision::Initialize()
{
	//衝突判定
	SphereCollider* collision = new SphereCollider(D3DXVECTOR3(2.0, 0.0, 1.0), 0.5f);
	AddCollider(collision);
}

//更新
void Chara_Hand_decision::Update()
{

	//キーボードの右を押されたときキャラを右に動かす
	if (Input::IsKey(DIK_RIGHT))
	{
		_position.x += 0.3;
	}
	//キーボードの左を押されたときキャラを左に動かす
	if (Input::IsKey(DIK_LEFT))
	{

		_position.x -= 0.3;
	}
	//キーボードの上を押されたときキャラを上に動かす
	if (Input::IsKey(DIK_UP))
	{
		_position.z += 0.5;
	}
	//キーボードの下を押されたときキャラを下に動かす
	if (Input::IsKey(DIK_DOWN))
	{

		_position.z -= 0.5;
	}


	//攻撃判定の移動
	_position += _move;
	_move.z += _dz;
	_dz += 0.01f;


	//こぶし判定くらいのところに飛ばしたら消える
	if (_dz > 0.55)
	{
		KillMe();
	}
}

//描画
void Chara_Hand_decision::Draw()
{
}

//開放
void Chara_Hand_decision::Release()
{
}
//発射
void Chara_Hand_decision::Punch(D3DXVECTOR3 position, D3DXVECTOR3 direction)
{
	//位置
	_position = position;

	//移動は、引数で渡された方向にSPEEDの速さ
	D3DXVec3Normalize(&_move, &direction);
	_move *= SPEED;
}

//何かに当たった
void Chara_Hand_decision::OnCollision(IGameObject * pTarget)
{

}