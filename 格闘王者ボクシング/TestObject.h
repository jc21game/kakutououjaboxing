/////////////////////////////////////
//////これコピッて使ってね///////////
/////////////////////////////////////

#pragma once
#include "Engine/GameObject/GameObject.h"

//●●を管理するクラス
class TestObject : public IGameObject
{

public:
	//コンストラクタ
	TestObject(IGameObject* parent);

	//デストラクタ
	~TestObject();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};