#include "Press.h"
#include "Engine/Image.h"
#include "assert.h"

//Pressボタンの表示					
//Pressボタンが選択されたらメニューシーンへ切り替え	
//背景の表示



//コンストラクタ
Press::Press(IGameObject * parent)
	:IGameObject(parent, "Press"), hPict_(-1)
{
}

//デストラクタ
Press::~Press()
{
}

//初期化
void Press::Initialize()
{

	//画像データのロード
	hPict_ = Image::Load("data/press.png");
	assert(hPict_ >= 0);
	//大きさを変える
	scale_ = D3DXVECTOR3(0.8f, 0.8f, 0.8f);
	//位置を変える
	position_ = D3DXVECTOR3(200.0f, 700.0f, 0);

}

//更新
void Press::Update()
{
}

//描画
void Press::Draw()
{
	Image::SetMatrix(hPict_, worldMatrix_);
	Image::Draw(hPict_);
}

//開放
void Press::Release()
{
}