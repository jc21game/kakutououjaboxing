#include "TitleLogo.h"
#include "Engine/Image.h"
#include "assert.h"
#include "Press.h"

//Backボタンの表示					
//Backボタンが選択されたらメニューシーンへ切り替え	
//背景の表示



//コンストラクタ
TitleLogo::TitleLogo(IGameObject * parent)
	:IGameObject(parent, "TitleLogo"), hPict_(-1)
{
}

//デストラクタ
TitleLogo::~TitleLogo()
{
}

//初期化
void TitleLogo::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("data/Title.png");
	assert(hPict_ >= 0);
	//大きさを変える
	scale_ = D3DXVECTOR3(1.5f, 1.5f, 1.5f);
	//位置を変える
	position_ = D3DXVECTOR3(-700.0f, -1000.0f, 0);

	//最初の色を明るくしたい
	D3DCOLOR color = 0xffffffff;

}

//更新
void TitleLogo::Update()
{

	if (position_.x >= 300)
	{
		//すべて表示されているときにスペースで次に進む
		if (Input::IsKeyDown(DIK_SPACE) && count == 1)
		{
			//タイトルシーンへ切り替わる
			SceneManager::ChangeScene(SCENE_ID_CHOICE);
		}
	}

	if (position_.x <= 300)
	{
		//タイトルの動きを飛ばした時
		if (Input::IsKeyDown(DIK_SPACE))
		{
			//規定値に動かす
			position_.x = 300;
			position_.y = 0;
			scale_.x = 0.5f;
			scale_.y = 0.5f;
			scale_.z = 0.5f;
			CreateGameObject<Press>(this);
			//すべて表示できた判定
			count = 1;
		}
	}

	if (position_.x <= 300)
	{
		position_.x += 10;
	}
	if (position_.y <= 0)
	{
		position_.y += 10;
	}

	if (scale_.x >= 0.5)
	{
		scale_.x -= 0.01f;
		scale_.y -= 0.01f;
		scale_.z -= 0.01f;
	}

	if (position_.x == 300)
	{
		CreateGameObject<Press>(this);
		//すべて表示されたとき用
		count = 1;
	}

}

//描画
void TitleLogo::Draw()
{
	Image::SetMatrix(hPict_, worldMatrix_);
	Image::Draw(hPict_);
}

//開放
void TitleLogo::Release()
{
}

