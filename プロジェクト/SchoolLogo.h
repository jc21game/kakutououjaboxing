#pragma once
#include "Engine/IGameObject.h"

//スクールロゴを管理するクラス
class SchoolLogo : public IGameObject
{
	//画像番号
	int hPict_;


public:
	//コンストラクタ
	SchoolLogo(IGameObject* parent);

	//デストラクタ
	~SchoolLogo();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

};