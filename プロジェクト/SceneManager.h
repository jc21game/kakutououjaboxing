﻿#pragma once
#include "Engine/IGameObject.h"
#include "Engine/Global.h"



enum SCENE_ID
{
	SCENE_ID_BOOT = 0,	//仮画面
	SCENE_ID_SPLASH,	//スプラッシュシーン
	SCENE_ID_TITLE,		//タイトルシーン
	SCENE_ID_CHOICE,	//準備シーン
	SCENE_ID_FIGHT,		//戦闘(シングルプレイ)シーン
	SCENE_ID_BATTLE,	//戦闘(マルチプレイ)シーン
	SCENE_ID_RESULT,	//戦績シーン
};



//シーンを管理するクラス
class SceneManager : public IGameObject
{
	static SCENE_ID currentSceneID_;
	static SCENE_ID nextSceneID_;
	static IGameObject*	pCurrentScene_;

public:

	//コンストラクタ
	SceneManager(IGameObject* parent);

	//デストラクタ
	~SceneManager();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//シーン切り替え
	static void ChangeScene(SCENE_ID next);

	//カレントシーンのゲッター
	static IGameObject* GetCurrentScene() { return pCurrentScene_; }
};