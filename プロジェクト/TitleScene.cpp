
#include "TitleScene.h"

#include "Object/Title.h"
#include "Object/Press.h"
#include "Object/Background.h"
#include "Object/Stage.h"


//コンストラクタ
TitleScene::TitleScene(IGameObject * parent)
	: IGameObject(parent, "TitleScene")
{
}

//初期化
void TitleScene::Initialize()
{
	//タイトルの動きが止まった時にプレスを出したい
	CreateGameObject<Stage>(this);
	CreateGameObject<Background>(this);
	CreateGameObject<Title>(this);
}

//更新
void TitleScene::Update()
{
	//SPACEが押されたら
	if (Input::IsKeyDown(DIK_SPACE))
	{

		SceneManager::ChangeScene(SCENE_ID_CHOICE);

	}
}

//描画
void TitleScene::Draw()
{
}

//開放
void TitleScene::Release()
{
}
