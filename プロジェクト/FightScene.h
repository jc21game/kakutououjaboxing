#pragma once

#include "Engine/Global.h"
#include "Engine/Direct3D.h"
#include "Engine/Input.h"
#include "Engine/Image.h"
#include "Engine/Camera.h"

class Camera;
class Gauge_bar_1p;
class Gauge_bar_2p;
class Down2P;
class Down1P;

//戦闘シーンを管理するクラス
class FightScene : public IGameObject
{

	int hPict_;

	Gauge_bar_1p* pBar_1p;
	Gauge_bar_2p* pBar_2p;
	Down2P* pDown_2p;
	Down1P* pDown_1p;

public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	FightScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//画面分割用のやつ
	void DrawSub()	override;



	Gauge_bar_1p* GetBar_1p()
	{
		//ポインタ
		return pBar_1p;
	}

	Gauge_bar_2p* GetBar_2p()
	{
		//ポインタ
		return pBar_2p;
	}

	Down2P* GetDown_2p()
	{
		//ポインタ
		return pDown_2p;
	}

	Down1P* GetDown_1p()
	{
		//ポインタ
		return pDown_1p;
	}

};