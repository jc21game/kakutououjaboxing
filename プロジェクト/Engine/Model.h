﻿#pragma once

#include <assert.h>
#include <vector>
#include <string>
#include "Global.h"
#include "Fbx.h"
//H
namespace Model
{
	struct ModelData
	{
		std::string fileName;	//ファイル名

		Fbx*	pFbx;			//ロードしたモデルデータのアドレス
		
		D3DXMATRIX matrix;		//行列

		//アニメーションのフレーム
		float nowFrame, animSpeed;
		int startFrame, endFrame;

		//初期化
		ModelData() : fileName(""), pFbx(nullptr),nowFrame(0), startFrame(0), endFrame(0), animSpeed(0)
		{
			D3DXMatrixIdentity(&matrix);
		}

		//アニメーションのフレーム数をセット
		//(第一引数：開始フレーム、第二引数：終了フレーム、第三引数：アニメーション速度)
		void SetAnimFrame(int start, int end, float speed)
		{
			nowFrame = start;
			startFrame = start;
			endFrame = end;
			animSpeed = speed;
		}

	};

	//初期化
	void Initialize();

	//モデルをロード
	int Load(std::string fileName);
	//描画
	void Draw(int handle);

	//アニメーションのフレーム数をセット
	//引数：handle		設定したいモデルの番号
	//引数：startFrame	開始フレーム
	//引数：endFrame	終了フレーム
	//引数：animSpeed	アニメーション速度
	void SetAnimFrame(int handle, int startFrame, int endFrame, float animSpeed);

	//現在のアニメーションのフレームを取得
	int GetAnimFrame(int handle);

	//任意のボーンの位置を取得
	//引数：handle		調べたいモデルの番号
	//引数：boneName	調べたいボーンの名前
	//戻値：ボーンの位置（ワールド座標）
	D3DXVECTOR3 GetBonePosition(int handle, std::string boneName);

	//ワールド行列を設定
	void SetMatrix(int handle, D3DXMATRIX& matrix);
	//ワールド行列の取得
	D3DXMATRIX GetMatrix(int handle);
	//任意のモデルを開放
	void Release(int handle);
	//すべてのモデルを開放
	//(シーンが切り替わるときに必ず実行してください)
	void AllRelease();


	//レイキャスト（レイを飛ばして当たり判定）
	//引数：handle	判定したいモデルの番号
	//引数：data	必要なものをまとめたデータ
	void RayCast(int handle, RayCastData *data);
};