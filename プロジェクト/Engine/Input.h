﻿#pragma once

#define DIRECTINPUT_VERSION 0x0800

#include <dInput.h>
#include <d3dx9.h>
#include "XInput.h"

#pragma comment(lib, "dxguid.lib")
#pragma comment(lib, "dInput8.lib")

#define SAFE_RELEASE(p) if(p != nullptr){ p->Release(); p = nullptr;} 

namespace Input
{
	//初期化
	//引数：hWnd	ウィンドウハンドル
	void Initialize(HWND hWnd);

	//更新
	void Update();

	/////////////////////////キーボード///////////////////////////
	bool IsKey(int keyCode);
	bool IsKeyDown(int keyCode);
	bool IsKeyUp(int keyCode);
	//////////////////////////////////////////////////////////////

	////////////////////////////ボタン////////////////////////////
	//コントローラーのボタンが押されているか調べる
	bool IsPadButton(int buttonCode, int padID = 0);

	//コントローラーのボタンを今押したか調べる（押しっぱなしは無効）
	bool IsPadButtonDown(int buttonCode, int padID = 0);

	//コントローラーのボタンを今放したか調べる
	bool IsPadButtonUp(int buttonCode, int padID = 0);

	//左スティックの傾きを取得
	D3DXVECTOR3 GetPadStickL(int padID = 0);

	//右スティックの傾きを取得
	D3DXVECTOR3 GetPadStickR(int padID = 0);

	//左トリガーの押し込み具合を取得
	float		GetPadTrrigerL(int padID = 0);

	//右トリガーの押し込み具合を取得
	float		GetPadTrrigerR(int padID = 0);

	//振動させる
	void SetPadVibration(int l, int r, int padID = 0);
	//////////////////////////////////////////////////////////////

	//開放
	void Release();
};