#pragma once
#include "Global.h"
#include "Xact3.h"

namespace Audio
{

	void Initialize();
	void WaveLoad(char* wFilename);
	void SoundLoad(char* sFilename);
	void Play(char* filename);
	void Stop();
	void Release();

}