﻿#include "Direct3D.h"


namespace Direct3D
{
	LPDIRECT3D9			Direct3D::pD3d_ = nullptr;	   //Direct3Dオブジェクト
	LPDIRECT3DDEVICE9	Direct3D::pDevice_ = nullptr;	//Direct3Dデバイスオブジェクト

	bool		_isLighting = false;			//ライティングするか


	void Direct3D::Initialize(HWND hWnd)
	{
		//Direct3Dオブジェクトの作成
		pD3d_ = Direct3DCreate9(D3D_SDK_VERSION);
		assert(pD3d_ != nullptr);

		//DIRECT3Dデバイスオブジェクトの作成
		D3DPRESENT_PARAMETERS d3dpp;	                //専用の構造体
		ZeroMemory(&d3dpp, sizeof(d3dpp));	          //中身を全部0にする
		d3dpp.BackBufferFormat = D3DFMT_UNKNOWN;
		d3dpp.BackBufferCount = 1;
		d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
		d3dpp.Windowed = TRUE;
		d3dpp.EnableAutoDepthStencil = TRUE;
		d3dpp.AutoDepthStencilFormat = D3DFMT_D16;
		d3dpp.BackBufferWidth = g.screenWidth;
		d3dpp.BackBufferHeight = g.screenHeight;
		pD3d_->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd,
			D3DCREATE_HARDWARE_VERTEXPROCESSING, &d3dpp, &pDevice_);
		assert(pDevice_ != nullptr);

		//アルファブレンド
		pDevice_->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
		pDevice_->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
		pDevice_->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);

		//ライティング
		pDevice_->SetRenderState(D3DRS_LIGHTING, TRUE);

		//ライトを設置
		D3DLIGHT9 lightState;
		ZeroMemory(&lightState, sizeof(lightState));
		lightState.Type = D3DLIGHT_DIRECTIONAL;
		lightState.Direction = D3DXVECTOR3(1, -8, 1);
		lightState.Diffuse.r = 1.0f;
		lightState.Diffuse.g = 1.0f;
		lightState.Diffuse.b = 1.0f;
		pDevice_->SetLight(0, &lightState);
		pDevice_->LightEnable(0, TRUE);


		//ライティング
		_isLighting = GetPrivateProfileInt("RENDER", "Lighting", 0, ".\\Data\\setup.ini") != 0;
		if (_isLighting)
		{
			pDevice_->SetRenderState(D3DRS_LIGHTING, TRUE);

			//ライトの向き　当て方
			//　8/29時点ではキャラが明るすぎる気がするので修正推奨
			dir[0] = D3DXVECTOR3(1.0f, -1.0f, 1.0f);
			dir[1] = D3DXVECTOR3(5.0f, 1.0f, -1.0f);
			dir[2] = D3DXVECTOR3(-1.0f, -1.0f, 1.0f);

			//  ライトの色の初期化 わかりやすくライトの色を別々な色に設定
			//  後で全部白にする
			diff[0].r = diff[1].g = diff[2].b = 1.0f;
			diff[1].r = diff[0].g = diff[0].b = 1.0f;
			diff[2].r = diff[2].g = diff[1].b = 1.0f;

			//  ハイライトの色の初期化			
			for (int i = 0; i < 3; i++)
			{
				spec[i].r = spec[i].g = spec[i].b = 1.0f;
			}

			int index;	//ライト管理変数
			D3DLIGHT9 lightState;	//ライトの設定
			ZeroMemory(&lightState, sizeof(D3DLIGHT9));


			//３つのライトの管理
			for (index = 0; index < 3; index++)
			{

				//  パラメータセット：ライトの色
				lightState.Diffuse = diff[index];

				//  パラメータセット：ハイライトの色
				lightState.Specular = spec[index];

				//タイプ
				lightState.Type = D3DLIGHT_DIRECTIONAL;			//ライトの種類

				//向き
				lightState.Direction = dir[index];  //ライトの方向を3Dベクトルで設定

				//  ライト情報のセット
				pDevice_->SetLight(index, &lightState);	//ライトに情報をセット
				pDevice_->LightEnable(index, TRUE);		//ライト有効
			}
			//  Direct3Dによるライティングを有効にする
			pDevice_->SetRenderState(D3DRS_LIGHTING, TRUE);
		}
		else
		{
			pDevice_->SetRenderState(D3DRS_LIGHTING, FALSE);
		}



		//カメラ
		D3DXMATRIX view;
		D3DXMatrixLookAtLH(&view, &D3DXVECTOR3(0, 5, -10), &D3DXVECTOR3(0, 0, 0), &D3DXVECTOR3(0, 1, 0));
		pDevice_->SetTransform(D3DTS_VIEW, &view);

		D3DXMATRIX proj;
		D3DXMatrixPerspectiveFovLH(&proj, D3DXToRadian(60), (float)g.screenWidth / g.screenHeight, 1.0f, 100.0f);
		pDevice_->SetTransform(D3DTS_PROJECTION, &proj);
	}

	void Direct3D::BeginDraw()
	{
		//画面をクリア
		pDevice_->Clear(0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER,
			D3DCOLOR_XRGB(0, 0, 0), 1.0f, 0);

		//描画開始
		pDevice_->BeginScene();
	}

	void Direct3D::EndDraw()
	{
		//描画終了
		pDevice_->EndScene();

		//スワップ
		pDevice_->Present(NULL, NULL, NULL, NULL);
	}

	void Direct3D::Release()
	{
		SAFE_RELEASE(pDevice_);
		SAFE_RELEASE(pD3d_);
	}
}