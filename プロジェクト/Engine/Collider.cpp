#include "Collider.h"


#include "../Engine/IGameObject.h"
#include "../Engine/Direct3D.h"




Collider::Collider(IGameObject* owner, D3DXVECTOR3 center, float radius): pGameObject_(nullptr), pMesh_(nullptr)
{
	owner_ = owner;
	center_ = center;
	radius_ = radius;
	//リリース時は判定枠は表示しない
#ifdef _DEBUG
	//テスト表示用判定枠
	D3DXCreateSphere(Direct3D::pDevice_, radius, 8, 4, &pMesh_, 0);
#endif
}


Collider::~Collider()
{
	if (pMesh_ != nullptr)
	{
		pMesh_->Release();
	}
}


bool Collider::IsHit(Collider& target)
{
	D3DXVECTOR3 v = (center_ + owner_->GetPosition()) -
		(target.center_ + target.owner_->GetPosition());

	float length = D3DXVec3Length(&v);
	if (length <= radius_ + target.radius_)
	{
		return true;
	}

	return false;
}




//テスト表示用の枠を描画
//引数：position	位置
void Collider::Draw(D3DXVECTOR3 position)
{
	D3DXMATRIX mat;
	D3DXMatrixTranslation(&mat, position.x + center_.x, position.y + center_.y, position.z + center_.z);
	Direct3D::pDevice_->SetTransform(D3DTS_WORLD, &mat);
	pMesh_->DrawSubset(0);
}
