﻿#pragma once
#include <fbxsdk.h>
#include "Global.h"
#include <vector>
#include <map>

#pragma comment(lib,"libfbxsdk-mt.lib")

class FbxParts;

//レイキャスト用構造体
//いらなかったら消してください
struct RayCastData
{
	D3DXVECTOR3 start;	//レイ発射位置
	D3DXVECTOR3 dir;	//レイの向きベクトル
	float       dist;	//衝突点までの距離
	BOOL        hit;	//レイが当たったか
	D3DXVECTOR3 normal;	//法線

	RayCastData() { dist = 99999.0f; }
};



class Fbx
{

	struct Vertex
	{
		D3DXVECTOR3 pos;
		D3DXVECTOR3 normal;
		D3DXVECTOR2 uv;
	};

	friend class FbxParts;

	FbxManager*  pManager_;
	FbxImporter* pImporter_;
	FbxScene*    pScene_;

	
	
	LPDIRECT3DVERTEXBUFFER9	pVertexBuffer_;
	LPDIRECT3DINDEXBUFFER9*	ppIndexBuffer_;

	LPDIRECT3DTEXTURE9*	pTexture_; //テクスチャ
	D3DMATERIAL9*		pMaterial_;
	//テクスチャのロード
	//引数：fileName	ファイル名
	//戻値：テクスチャのアドレス
	LPDIRECT3DTEXTURE9 LoadTexture(std::string fileName);

	std::vector<FbxParts*>	parts; //パーツ（複数あるかも）
	//テクスチャ（複数あるかも）
	std::map<std::string, LPDIRECT3DTEXTURE9> _pTexture;


	FbxTime::EMode	frameRate;		//アニメーションのフレームレート
	float			animSpeed;		//アニメーション速度
	int startFrame, endFrame;		//アニメーションの最初と最後のフレーム
	
	



	int vertexCount_;
	int polygonCount_;
	int indexCount_;
	int materialCount_;
	int* polygonCountOfMaterial_;

	//ノードの中身を調べる
	//引数：pNode		調べるノード
	//引数：pPartsList	パーツのリスト
	void CheckNode(FbxNode* pNode, std::vector<FbxParts*> *pPartsList);
	void CheckMesh(FbxMesh* pMesh);

public:
	Fbx();
	~Fbx();

	//モデルをロードする
	//引数：fileName	ファイル名
	//戻値：成功／失敗
	HRESULT Load(std::string fileName);
	void Draw(D3DXMATRIX matrix, int frame);
	void SetAnimFrame(int startFrame, int endFrame, float speed);
	D3DXVECTOR3 GetBonePosition(std::string boneName);
	//レイキャスト（レイを飛ばして当たり判定）
	//引数：data	必要なものをまとめたデータ
	void RayCast(RayCastData *data);

};


