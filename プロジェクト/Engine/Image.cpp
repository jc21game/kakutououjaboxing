#include <vector>
#include "Image.h"
#include "Global.h"


namespace Image
{
	std::vector<ImageData*> dataList;

	int Load(std::string fileName)
	{
		ImageData* pData = new ImageData;
		pData->fileName = fileName;

		bool isExist = false;
		for (unsigned int i = 0; i < dataList.size(); i++)
		{
			// すでにロードしたことのあるモデルだった場合
			if (dataList[i]->fileName == fileName)
			{
				// アドレスのコピー
				pData->pSprite = dataList[i]->pSprite;
				isExist = true;
				break;
			}
		}

		// ロードしたことがなかった場合
		if (isExist == false)
		{
			// ※同じモデルを使いまわしている(同じ番地を見ている)ため、そこを消すとモデルを※
			// ※見失ってしまうのでDELETEの際は注意が必要※
			pData->pSprite = new Sprite;
			pData->pSprite->Load(fileName.c_str());
			// ⇒c_str()：char型の変数に変換
			// 　Loadの引数(const char*)とfileName(std::string)で型が違うため合わせる必要がある
		}

		dataList.push_back(pData);

		return dataList.size() - 1;
		// ⇒１個目のデータは０番地に入っているので調整する
	}

	void Draw(int handle)
	{
		// dataListのhandle番地のデータをmatrixに渡す…？
		dataList[handle]->pSprite->Draw(dataList[handle]->matrix);
	}

	void SetMatrix(int handle, D3DXMATRIX &matrix)
	{
		dataList[handle]->matrix = matrix;
	}

	void Release(int handle)
	{
		bool isExist = false;
		for (unsigned int i = 0; i < dataList.size(); i++)
		{
			// モデルデータを使っていた場合
			// dataList[i]->fileName == dataList[handle]->fileNameも可
			if ((i != handle) && (dataList[i] != nullptr) && (dataList[i]->pSprite == dataList[handle]->pSprite))
			{
				isExist = true;
				break;
			}
		}

		// モデルデータを使っていなかった場合
		if (isExist == false)
		{
			SAFE_DELETE(dataList[handle]->pSprite);
		}

		SAFE_DELETE(dataList[handle]);
	}

	void AllRelease()
	{
		for (unsigned int i = 0; i < dataList.size(); i++)
		{
			// ※削除後にアクセスするとエラーになるので注意※
			if (dataList[i] != nullptr)
			{
				Release(i);
			}
		}

		dataList.clear();	// 配列の削除
	}
};