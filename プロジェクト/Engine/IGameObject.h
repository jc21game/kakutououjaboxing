﻿#pragma once
#include <d3dx9.h>
#include <vector>
#include <list>
#include <string>
#include <assert.h>
#include "GameObjectManager.h"
//#include "Collider.h"


class Collider;


class IGameObject
{
protected:

	std::string name_;
	D3DXMATRIX	localMatrix_;
	D3DXMATRIX	worldMatrix_;

	D3DXVECTOR3 position_;
	D3DXVECTOR3 rotate_;
	D3DXVECTOR3	scale_;

	bool dead_;
	D3DXMATRIX view_;
	Collider	*pCollider_;
	std::list<Collider*>	colliderList_;	//衝突判定リスト

	//フラグ
	struct OBJECT_STATE
	{
		unsigned initialized : 1;	//初期化済みか
		unsigned entered : 1;		//更新するか
		unsigned visible : 1;		//描画するか
		unsigned dead : 1;			//削除するか
	};
	OBJECT_STATE _State;

	//親オブジェクト
	IGameObject* pParent;

	//子オブジェクトリスト
	std::list<IGameObject*> childList_;

	////オブジェクト削除（再帰）
	////引数：obj　削除するオブジェクト
	//void KillObjectSub(IGameObject* obj);


public:

	//コンストラクタ
	IGameObject();
	IGameObject(IGameObject* parent);
	IGameObject(IGameObject* parent, const std::string& name);

	//デストラクタ
	virtual ~IGameObject();

	void Transform();
	
	//いつものやつ
	virtual void Initialize() = 0;
	virtual void Update() = 0;
	virtual void Draw() = 0;
	virtual void Release() = 0;

	void UpdateSub();
	virtual void DrawSub();
	void ReleaseSub();


	//子オブジェクトリストを取得
	std::list<IGameObject*>* GetChildList();

	//親オブジェクトを取得
	IGameObject* GetParent();

	//名前でオブジェクトを検索（対象は自分の子供以下)
	IGameObject* FindChildObject(const std::string& name);

	//名前でオブジェクトを検索（対象は全体）
	IGameObject* FindObject(const std::string& name) { return GameObjectManager::FindChildObject(name); }

	//オブジェクトの名前を取得
	const std::string& GetObjectName(void) const;

	//
	void SetCollider(D3DXVECTOR3& center, float radius);

	//コライダー（衝突判定）を追加する
	void AddCollider(Collider * collider);

	//何かと衝突した場合に呼ばれる（オーバーライド用）
	virtual void OnCollision(IGameObject* pTarget) {};
	
	//各フラグの制御
	bool IsDead();	// 削除するかどうか
	void KillMe();	// 自分を削除する
	void Enter();	// Updateを許可
	void Leave();	// Updateを拒否
	void Visible();		// Drawを許可
	void Invisible();	// Drawを拒否
	bool IsInitialized();	// 初期化済みかどうか
	void SetInitialized();	// 初期化済みにする
	bool IsEntered();	// Update実行していいか
	bool IsVisibled();	// Draw実行していいか

	//衝突判定
	void Collision(IGameObject* pTarget);

	//テスト用の衝突判定枠を表示
	void CollisionDraw();

	void PushBackChild(IGameObject* pObj);

	//位置を設定
	D3DXVECTOR3 GetPosition() { return position_; }
	D3DXVECTOR3 GetRotate() { return rotate_; }
	D3DXVECTOR3 GetScale() { return scale_; }
	void SetPosition(D3DXVECTOR3 position) { position_ = position; }
	void SetRotation(D3DXVECTOR3 rotate) { rotate_ = rotate; }
	void SetRotate(float x, float y, float z) { SetRotation(D3DXVECTOR3(x, y, z)); }
	void SetScale(D3DXVECTOR3 scale) { scale_ = scale; }
	D3DXMATRIX GetView() {

		return view_;

	}
};

//オブジェクトを作成するテンプレート
template <class T>
T* CreateGameObject(IGameObject* parent)
{
	T* p = new T(parent);
	parent->PushBackChild(p);
	p->Initialize();
	return p;
}