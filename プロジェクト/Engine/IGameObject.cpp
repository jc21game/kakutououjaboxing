﻿#include "IGameObject.h"
#include "Global.h"
#include "Collider.h"



//コンストラクタ（親も名前もなし）
IGameObject::IGameObject() :
	IGameObject(nullptr, "")
{
	colliderList_.clear();
}

//コンストラクタ（名前なし）
IGameObject::IGameObject(IGameObject * parent) :
	IGameObject(parent, "")
{
}

//コンストラクタ（標準）
IGameObject::IGameObject(IGameObject * parent, const std::string & name):
	pParent(parent), name_(name), position_(D3DXVECTOR3(0, 0, 0)),
	rotate_(D3DXVECTOR3(0, 0, 0)), scale_(D3DXVECTOR3(1, 1, 1)),
	dead_(false),pCollider_(nullptr)
{
}

//デストラクタ
IGameObject::~IGameObject()
{
	SAFE_DELETE(pCollider_);
}

void IGameObject::Transform()
{
	D3DXMATRIX matT, matRX, matRY, matRZ, matS;

	D3DXMatrixTranslation(&matT, position_.x, position_.y, position_.z);
	D3DXMatrixRotationX(&matRX, D3DXToRadian(rotate_.x));
	D3DXMatrixRotationY(&matRY, D3DXToRadian(rotate_.y));
	D3DXMatrixRotationZ(&matRZ, D3DXToRadian(rotate_.z));
	D3DXMatrixScaling(&matS, scale_.x, scale_.y, scale_.z);

	localMatrix_ = matS * matRY * matRX * matRZ * matT;

	if (pParent != nullptr)
		worldMatrix_ = localMatrix_ * pParent->worldMatrix_;
	else
		worldMatrix_ = localMatrix_;

}

void IGameObject::UpdateSub()
{
	Update();
	Transform();

	if (pCollider_ != nullptr)
	{
		Collision(SceneManager::GetCurrentScene());
	}
	


	for (auto it = childList_.begin(); it != childList_.end(); it++)
	{
		(*it)->UpdateSub();
	}

	for (auto it = childList_.begin(); it != childList_.end();)
	{
		if ((*it)->dead_ == true)
		{
			(*it)->ReleaseSub();
			SAFE_DELETE(*it);
			it = childList_.erase(it);
		}
		else
		{
			it++;
		}
	}

}

void IGameObject::DrawSub()
{

	Draw();

	for (auto it = childList_.begin(); it != childList_.end(); it++)
	{
		(*it)->DrawSub();
	}
}

void IGameObject::ReleaseSub()
{
	Release();

	for (auto it = childList_.begin(); it != childList_.end(); it++)
	{
		(*it)->ReleaseSub();
		SAFE_DELETE(*it);
	}
}

//子オブジェクトリストを取得
std::list<IGameObject*>* IGameObject::GetChildList()
{
	return &childList_;
}

//親オブジェクトを取得
IGameObject * IGameObject::GetParent(void)
{
	return pParent;
}

//名前でオブジェクトを検索（対象は自分の子供以下）
IGameObject * IGameObject::FindChildObject(const std::string & name)
{
	//子供がいないなら終わり
	if (childList_.empty())
		return nullptr;

	//イテレータ
	auto it = childList_.begin();	//先頭
	auto end = childList_.end();	//末尾

	//子オブジェクトから探す
	while (it != end) {
		//同じ名前のオブジェクトを見つけたらそれを返す
		if ((*it)->GetObjectName() == name)
			return *it;

		//その子供（孫）以降にいないか探す
		IGameObject* obj = (*it)->FindChildObject(name);
		if (obj != nullptr)
		{
			return obj;
		}

		//次の子へ
		it++;
	}

	//見つからなかった
	return nullptr;
}

//オブジェクトの名前を取得
const std::string& IGameObject::GetObjectName(void) const
{
	return name_;
}


void IGameObject::KillMe()
{
	dead_ = true;
}

//コライダー（衝突判定）を追加する
void IGameObject::AddCollider(Collider* collider)
{
	collider->SetGameObject(this);
	colliderList_.push_back(collider);
}


void IGameObject::SetCollider(D3DXVECTOR3 & center, float radius)
{
	pCollider_ = new Collider(this, center, radius);
}

void IGameObject::Collision(IGameObject* targetObject)
{
	if ((targetObject != this) && (targetObject->pCollider_ != nullptr) &&
		(pCollider_->IsHit(*targetObject->pCollider_)))
	{
		//当たった
		OnCollision(targetObject);
	}

	for (auto itr = targetObject->childList_.begin(); itr != targetObject->childList_.end(); itr++)
	{
		Collision(*itr);
	}
}

//void IGameObject::OnCollision(IGameObject * target)
//{
//}

//テスト用の衝突判定枠を表示
void IGameObject::CollisionDraw()
{
	Direct3D::pDevice_->SetRenderState(D3DRS_FILLMODE, D3DFILL_WIREFRAME);	//ワイヤーフレーム
	Direct3D::pDevice_->SetRenderState(D3DRS_LIGHTING, FALSE);				//ライティングOFF
	Direct3D::pDevice_->SetTexture(0, nullptr);								//テクスチャなし

	for (auto i = this->colliderList_.begin(); i != this->colliderList_.end(); i++)
	{
		(*i)->Draw(position_);
	}

	Direct3D::pDevice_->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
	Direct3D::pDevice_->SetRenderState(D3DRS_LIGHTING, Direct3D::_isLighting);
}

void IGameObject::PushBackChild(IGameObject* pObj)
{
	childList_.push_back(pObj);
}


// 削除するかどうか
bool IGameObject::IsDead()
{
	return (_State.dead != 0);
}



// Updateを許可
void IGameObject::Enter()
{
	_State.entered = 1;
}

// Updateを拒否
void IGameObject::Leave()
{
	_State.entered = 0;
}

// Drawを許可
void IGameObject::Visible()
{
	_State.visible = 1;
}

// Drawを拒否
void IGameObject::Invisible()
{
	_State.visible = 0;
}

// 初期化済みかどうか
bool IGameObject::IsInitialized()
{
	return (_State.initialized != 0);
}

// 初期化済みにする
void IGameObject::SetInitialized()
{
	_State.initialized = 1;
}

// Update実行していいか
bool IGameObject::IsEntered()
{
	return (_State.entered != 0);
}

// Draw実行していいか
bool IGameObject::IsVisibled()
{
	return (_State.visible != 0);
}