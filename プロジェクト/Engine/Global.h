﻿#pragma once
#include <d3dx9.h>
#include <assert.h>
#include "Input.h"
#include "IGameObject.h"
#include "Direct3D.h"
#include "../SceneManager.h"

#define SAFE_DELETE(p) if(p != nullptr){ delete p; p = nullptr;}
#define SAFE_DELETE_ARRAY(p) if(p != nullptr){ delete[] p; p = nullptr;}
#define SAFE_RELEASE(p) if(p != nullptr){ p->Release(); p = nullptr;}

struct Global
{
	int screenWidth;
	int screenHeight;
};
extern Global g;