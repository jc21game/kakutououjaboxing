#pragma once
#include <string>
#include "Sprite.h"

namespace Image
{
	struct ImageData
	{
		std::string fileName;
		Sprite* pSprite;
		D3DXMATRIX matrix;

		// コンストラクタ
		ImageData() : fileName(""), pSprite(nullptr)
		{
			// 行列の初期化(何もさせない)
			D3DXMatrixIdentity(&matrix);
		}
	};

	int Load(std::string fileName);
	void Draw(int handle);
	void SetMatrix(int handle, D3DXMATRIX &matrix);
	void Release(int handle);
	void AllRelease();


};
