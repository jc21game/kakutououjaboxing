﻿#include "Fbx.h"
#include "Direct3D.h"
#include "FbxParts.h"



Fbx::Fbx() :pVertexBuffer_(nullptr), ppIndexBuffer_(nullptr), pTexture_(nullptr), pMaterial_(nullptr),
pManager_(nullptr), pImporter_(nullptr), pScene_(nullptr), polygonCountOfMaterial_(nullptr),
vertexCount_(0), polygonCount_(0), indexCount_(0), materialCount_(0)
{
}


Fbx::~Fbx()
{
	SAFE_DELETE_ARRAY(polygonCountOfMaterial_);
	SAFE_DELETE_ARRAY(pMaterial_);

	for (int i = 0; i < materialCount_; i++)
	{
		SAFE_RELEASE(ppIndexBuffer_[i]);
		SAFE_RELEASE(pTexture_[i]);
	}
	SAFE_DELETE_ARRAY(ppIndexBuffer_);
	SAFE_DELETE_ARRAY(pTexture_);

	SAFE_RELEASE(pVertexBuffer_);
	pScene_->Destroy();
	pManager_->Destroy();

	//各パーツの開放
	for (int i = 0; i < parts.size(); i++)
	{
		SAFE_DELETE(parts[i]);
	}
	parts.clear();
}

//FBXファイル読み込み
HRESULT Fbx::Load(std::string fileName)
{
	//ロードに必要なやつらを準備
	pManager_ = FbxManager::Create();				//一番えらいやつ
	pImporter_ = FbxImporter::Create(pManager_, "");//ファイルを読み込むやつ
	pScene_ = FbxScene::Create(pManager_, "");		//読み込んだファイルを管理するやつ

	//インポーターを使ってファイルロード
	if (pImporter_->Initialize(fileName.c_str()) == false)
	{
		return E_FAIL;
	}
	pImporter_->Import(pScene_);

	//インポーターはお役御免
	pImporter_->Destroy();

	// アニメーションのタイムモードの取得
	frameRate = pScene_->GetGlobalSettings().GetTimeMode();


	//現在のカレントディレクトリを覚えておく
	char defaultCurrentDir[MAX_PATH];
	GetCurrentDirectory(MAX_PATH, defaultCurrentDir);

	//カレントディレクトリをファイルがあった場所に変更
	char dir[MAX_PATH];
	_splitpath_s(fileName.c_str(), nullptr, 0, dir, MAX_PATH, nullptr, 0, nullptr, 0);
	SetCurrentDirectory(dir);


	//ルートノードを取得して
	FbxNode* rootNode = pScene_->GetRootNode();

	//そいつの子供の数を調べて
	int childCount = rootNode->GetChildCount();

	//1個ずつチェック
	for (int i = 0; childCount > i; i++)
	{
		CheckNode(rootNode->GetChild(i), &parts);
	}


	//カレントディレクトリを元の位置に戻す
	SetCurrentDirectory(defaultCurrentDir);

	return S_OK;
}


LPDIRECT3DTEXTURE9 Fbx::LoadTexture(std::string fileName)
{
	
	//ロード済みテクスチャか
	auto it = _pTexture.find(fileName);

	//まだロードしてない
	if (it == _pTexture.end())
	{
		//ファイル名+拡張だけにする
		char name[_MAX_FNAME];	//ファイル名
		char ext[_MAX_EXT];		//拡張子
		_splitpath_s(fileName.c_str(), nullptr, 0, nullptr, 0, name, _MAX_FNAME, ext, _MAX_EXT);
		wsprintf(name, "%s%s", name, ext);

		//画像ファイルを読み込んでテクスチャ作成
		LPDIRECT3DTEXTURE9 pTexture;
		HRESULT hr = D3DXCreateTextureFromFileEx(Direct3D::pDevice_, name, 0, 0, 0, 0, D3DFMT_UNKNOWN, D3DPOOL_DEFAULT,
			D3DX_FILTER_NONE, D3DX_DEFAULT, 0, nullptr, nullptr, &pTexture);

		if (FAILED(hr))
		{
			std::string message = "テクスチャ「";
			message += name;
			message += "」が開けません。\nFBXファイルと同じ場所にありますか？";

			MessageBox(NULL, message.c_str(), "BaseProjDx9エラー", MB_OK);
		}

		//一覧に追加
		_pTexture.insert(std::make_pair(fileName, pTexture));

		return pTexture;
	}

	//ロード済み
	return it->second;

}

//ノードの中身を調べる
void Fbx::CheckNode(FbxNode* pNode, std::vector<FbxParts*> *pPartsList)
{
	//そのノードにはメッシュ情報が入っているだろうか？
	FbxNodeAttribute* attr = pNode->GetNodeAttribute();
	if (attr != nullptr && attr->GetAttributeType() == FbxNodeAttribute::eMesh)
	{
		//パーツを用意
		FbxParts* pParts = new FbxParts;

		//マテリアル情報を準備
		pParts->InitMaterial(pNode, this);

		//頂点情報
		pParts->InitVertexBuffer(pNode->GetMesh());

		//インデックス情報
		pParts->InitIndexBuffer(pNode->GetMesh());

		//アニメーションに関する情報を取得
		pParts->InitAnimation(pNode->GetMesh());

		//パーツ情報を動的配列に追加
		pPartsList->push_back(pParts);
	}

	//子ノードにもデータがあるかも！！
	{
		//子供の数を調べて
		int childCount = pNode->GetChildCount();

		//一人ずつチェック
		for (int i = 0; i < childCount; i++)
		{
			CheckNode(pNode->GetChild(i), pPartsList);
		}
	}
}


void Fbx::CheckMesh(FbxMesh* pMesh)
{
	//頂点
	FbxVector4* pVertexPos = pMesh->GetControlPoints();
	vertexCount_ = pMesh->GetControlPointsCount();
	Vertex* vertexList = new Vertex[vertexCount_];

	polygonCount_ = pMesh->GetPolygonCount();
	indexCount_ = pMesh->GetPolygonVertexCount();

	for (int i = 0; vertexCount_ > i; i++)
	{
		vertexList[i].pos.x = (float)pVertexPos[i][0];
		vertexList[i].pos.y = (float)pVertexPos[i][1];
		vertexList[i].pos.z = (float)pVertexPos[i][2];
	}

	//法線
	for (int i = 0; i < polygonCount_; i++)
	{
		int startIndex = pMesh->GetPolygonVertexIndex(i);
		for (int j = 0; j < 3; j++)
		{
			int index = pMesh->GetPolygonVertices()[startIndex + j];

			FbxVector4 Normal;
			pMesh->GetPolygonVertexNormal(i, j, Normal);
			vertexList[index].normal = D3DXVECTOR3((float)Normal[0], (float)Normal[1], (float)Normal[2]);

			FbxVector2 uv = pMesh->GetLayer(0)->GetUVs()->GetDirectArray().GetAt(index);
			vertexList[index].uv = D3DXVECTOR2((float)uv.mData[0], (float)(1.0 - uv.mData[1]));
		}
	}

	Direct3D::pDevice_->CreateVertexBuffer(sizeof(Vertex) *vertexCount_, 0,
		D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1, D3DPOOL_MANAGED,
		&pVertexBuffer_, 0);
	assert(pVertexBuffer_ != nullptr);

	Vertex *vCopy;
	pVertexBuffer_->Lock(0, 0, (void**)&vCopy, 0);
	memcpy(vCopy, vertexList, sizeof(Vertex) *vertexCount_);
	pVertexBuffer_->Unlock();
	SAFE_DELETE_ARRAY(vertexList);


	//インデックス
	ppIndexBuffer_ = new IDirect3DIndexBuffer9*[materialCount_];
	polygonCountOfMaterial_ = new int[materialCount_];

	for (int i = 0; i < materialCount_; i++)
	{
		int* indexList = new int[indexCount_];
		int count = 0;
		for (int polygon = 0; polygon < polygonCount_; polygon++)
		{
			int materialID = pMesh->GetLayer(0)->GetMaterials()->GetIndexArray().GetAt(polygon);
			if (materialID == i)
			{
				for (int vertex = 0; vertex < 3; vertex++)
				{
					indexList[count++] = pMesh->GetPolygonVertex(polygon, vertex);
				}
			}
		}
		polygonCountOfMaterial_[i] = count / 3;

		Direct3D::pDevice_->CreateIndexBuffer(sizeof(int) * indexCount_, 0,
			D3DFMT_INDEX32, D3DPOOL_MANAGED, &ppIndexBuffer_[i], 0);
		assert(ppIndexBuffer_ != nullptr);
		DWORD *iCopy;
		ppIndexBuffer_[i]->Lock(0, 0, (void**)&iCopy, 0);
		memcpy(iCopy, indexList, sizeof(int) * indexCount_);
		ppIndexBuffer_[i]->Unlock();
		SAFE_DELETE_ARRAY(indexList);
	}

}



//描画
void Fbx::Draw(D3DXMATRIX matrix, int frame)
{
	//パーツを1個ずつ描画
	for (int k = 0; k < parts.size(); k++)
	{
		// その瞬間の自分の姿勢行列を得る
		FbxTime     time;
		time.SetTime(0, 0, 0, frame, 0, 0, frameRate);


		//スキンアニメーション（ボーン有り）の場合
		if (parts[k]->GetSkinInfo() != nullptr)
		{
			parts[k]->DrawSkinAnime(matrix, time);
		}

		//メッシュアニメーションの場合
		else
		{
			parts[k]->DrawMeshAnime(matrix, time, pScene_);
		}
	}
}
//アニメーションの最初と最後のフレームを指定
void Fbx::SetAnimFrame(int startFrame, int endFrame, float speed)
{
	startFrame = startFrame;
   //_frame = startFrame;
	endFrame = endFrame;
	animSpeed = speed;
}


//任意のボーンの位置を取得
D3DXVECTOR3 Fbx::GetBonePosition(std::string boneName)
{
	D3DXVECTOR3 position = D3DXVECTOR3(0, 0, 0);
	for (int i = 0; i < parts.size(); i++)
	{
		if (parts[i]->GetBonePosition(boneName, &position))
			break;
	}


	return position;
}



//レイキャスト（レイを飛ばして当たり判定）
void Fbx::RayCast(RayCastData * data)
{
	//すべてのパーツと判定
	for (int i = 0; i < parts.size(); i++)
	{
		parts[i]->RayCast(data);
	}
}
