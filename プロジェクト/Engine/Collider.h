#pragma once
#include "Global.h"
#include <d3dx9.h>

class IGameObject;

//当たり判定のタイプ
enum ColliderType
{
	COLLIDER_BOX,		//箱型
	COLLIDER_CIRCLE		//球体
};

//当たり判定を管理するクラス
class Collider
{

	friend class BoxCollider;
	friend class SphereCollider;

protected:
	ColliderType	type_;			//種類
	IGameObject*	pGameObject_;	//この判定をつけたゲームオブジェクト
	D3DXVECTOR3		center_;        //中心位置（ゲームオブジェクトの原点から見た位置）
	float			radius_;
	IGameObject*	owner_;
	LPD3DXMESH		pMesh_;         //テスト表示用の枠



public:
	 Collider(IGameObject* owner, D3DXVECTOR3 center, float radius);
	 
	  ~Collider();

	//接触判定
     bool IsHit(Collider& target);

	//テスト表示用の枠を描画
	void Draw(D3DXVECTOR3 position);

	//セッター
	void SetGameObject(IGameObject* gameObject) { owner_ = gameObject; }

};

