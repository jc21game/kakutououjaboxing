﻿#include "Model.h"


//CPP
namespace Model
{
	//ロード済みのモデルデータ一覧
	std::vector<ModelData*>	dataList;

	//初期化
	void Initialize()
	{
		AllRelease();
	}

	//モデルをロード
	int Load(std::string fileName)
	{
		ModelData* pData = new ModelData;
		pData->fileName = fileName;

		bool isExist = false;
		for (int i = 0; i < dataList.size(); i++)
		{
			if (dataList[i]->fileName == fileName)
			{
				pData->pFbx = dataList[i]->pFbx;
				isExist = true;
				break;
			}
		}
		//新たにファイルを開く
		if (isExist == false)
		{
			pData->pFbx = new Fbx;
			pData->pFbx->Load(fileName.c_str());
		}

		dataList.push_back(pData);

		return dataList.size() - 1;
	}

	//描画
	void Draw(int handle)
	{
		if (handle < 0 || handle >= dataList.size() || dataList[handle] == nullptr)
		{
			return;
		}

		//アニメーションを進める
		dataList[handle]->nowFrame += dataList[handle]->animSpeed;

		//最後までアニメーションしたら戻す
		if (dataList[handle]->nowFrame > dataList[handle]->endFrame)
			dataList[handle]->nowFrame = dataList[handle]->startFrame;



		if (dataList[handle]->pFbx)
		{
			dataList[handle]->pFbx->Draw(dataList[handle]->matrix, dataList[handle]->nowFrame);
		}
	}


	//アニメーションのフレーム数をセット
	void SetAnimFrame(int handle, int startFrame, int endFrame, float animSpeed)
	{
		dataList[handle]->SetAnimFrame(startFrame, endFrame, animSpeed);
	}


	//現在のアニメーションのフレームを取得
	int GetAnimFrame(int handle)
	{
		return dataList[handle]->nowFrame;
	}


	//任意のボーンの位置を取得
	D3DXVECTOR3 GetBonePosition(int handle, std::string boneName)
	{
		D3DXVECTOR3 pos = dataList[handle]->pFbx->GetBonePosition(boneName);
		D3DXVec3TransformCoord(&pos, &pos, &dataList[handle]->matrix);
		return pos;
	}


	//ワールド行列を設定
	void SetMatrix(int handle, D3DXMATRIX & matrix)
	{
		dataList[handle]->matrix = matrix;
	}

	//ワールド行列の取得
	D3DXMATRIX GetMatrix(int handle)
	{
		return dataList[handle]->matrix;
	}


	void Release(int handle)
	{
		bool isExist = false;
		for (int i = 0; i < dataList.size(); i++)
		{
			if (i != handle && dataList[i] != nullptr &&
				dataList[i]->pFbx == dataList[handle]->pFbx)
			{
				isExist = true;
				break;
			}
		}

		if (isExist == false)
		{
			SAFE_DELETE(dataList[handle]->pFbx);
		}
		SAFE_DELETE(dataList[handle]);
	}

	void AllRelease()
	{
		for (int i = 0; i < dataList.size(); i++)
		{
			if(dataList[i] != nullptr)
			{
				Release(i);
			}
		}
		dataList.clear();
	}


	//レイキャスト（レイを飛ばして当たり判定）
	void RayCast(int handle, RayCastData *data)
	{
		D3DXVECTOR3 target = data->start + data->dir;
		D3DXMATRIX matInv;
		D3DXMatrixInverse(&matInv, 0, &dataList[handle]->matrix);
		D3DXVec3TransformCoord(&data->start, &data->start, &matInv);
		D3DXVec3TransformCoord(&target, &target, &matInv);
		data->dir = target - data->start;

		dataList[handle]->pFbx->RayCast(data);
	}
}