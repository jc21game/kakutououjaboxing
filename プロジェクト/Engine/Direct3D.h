﻿#pragma once
#include "Global.h"

namespace Direct3D
{
	extern LPDIRECT3D9			pD3d_;	   //Direct3Dオブジェクト
	extern LPDIRECT3DDEVICE9	pDevice_;	//Direct3Dデバイスオブジェクト

	extern bool					_isDrawCollision;	//コリジョンを表示するか
	extern bool					_isLighting;		//ライティングするか

	static D3DCOLORVALUE diff[3];          //  ライト色の変数          
	static D3DCOLORVALUE spec[3];          //  ハイライト色の変数          
	static D3DXVECTOR3 dir[3];             //  ライト向きの変数

	//Direct3Dの初期化
	//引数：hWnd	ウィンドウハンドル
	//戻値：なし
	void Initialize(HWND hWnd);

	//描画開始
	//引数：なし
	//戻値：なし
	void BeginDraw();

	void EndDraw();

	void Release();
}