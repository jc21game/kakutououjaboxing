#include "Option.h"
#include "Engine/ResouceManager/Image.h"
#include "Engine/ResouceManager/Audio.h"

//コンストラクタ
Option::Option(IGameObject * parent)
	:IGameObject(parent, "Option"),option_hPict(-1),  _hSound(-1)
{
}

//デストラクタ
Option::~Option()
{
}

//初期化
void Option::Initialize()
{

	//画像データのロード
	option_hPict = Image::Load("Data/OPTION.png");
	assert(option_hPict >= 0);

	_scale = D3DXVECTOR3(0.2f, 0.2f, 0.2f);
	_position = D3DXVECTOR3(630.0f, 650.0f, 0);


	BoxCollider* collision = new BoxCollider(D3DXVECTOR3(0, -150, 0), D3DXVECTOR3(5, 1, 1));
	AddCollider(collision);

	//サウンドデータのロード
	_hSound = Audio::Load("Data/Test2.wav");
	assert(_hSound >= 0);
}

//更新
void Option::Update()
{
}

//描画
void Option::Draw()
{
	Image::SetMatrix(option_hPict, _worldMatrix);
	Image::Draw(option_hPict);

}

//開放
void Option::Release()
{
}

//何かに当たった
void Option::OnCollision(IGameObject * pTarget)
{
	//UIに当たりなおかつスペースキーが押されたら
	if (pTarget->GetObjectName() == "UI"&& Input::IsKey(DIK_SPACE))
	{
		
		//オプションシーンへ切り替える
			SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
			pSceneManager->ChangeScene(SCENE_ID_OPTION);
			//Audio::Play(_hSound);
	}
}