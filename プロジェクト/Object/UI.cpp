#include "UI.h"
#include "Engine/ResouceManager/Image.h"
#include "Engine/ResouceManager/Audio.h"


//コンストラクタ
UI::UI(IGameObject * parent)
	:IGameObject(parent, "UI"), ui_hPict(-1)/*画像の初期化*/,ui_Sound(-1)//サウンドの初期化
{
}

//デストラクタ
UI::~UI()
{
}

//初期化
void UI::Initialize()
{
	//画像データのロード
	ui_hPict = Image::Load("data/UI.png");
	assert(ui_hPict >= 0);
	//大きさを変える(元の画像より小さくしている)
	_scale = D3DXVECTOR3(0.4f, 0.8f, 0.8f);

	//位置を変えている
	_position = D3DXVECTOR3(630.0f, 400.0f, 0);

	//当たり判定(箱型コライダー)
	SphereCollider* collision = new SphereCollider(D3DXVECTOR3(0, 0, 0), 2.0f);
	AddCollider(collision);

	//サウンドデータのロード
	ui_Sound = Audio::Load("data/Test23.wav");
	assert(ui_Sound >= 0);
}

//更新
void UI::Update()
{
	//キーボードの上を押されたときUIを上に動かす
	if (Input::IsKeyDown(DIK_UP))
	{
		_position.y = 400.0f;
		Audio::Play(ui_Sound);
	}
	//キーボードの下を押されたときUIを下に動かす
	if (Input::IsKeyDown(DIK_DOWN))
	{
		//プレイヤーを左に
		_position.y = 500.0f;
		
			Audio::Play(ui_Sound);
		
	}
}

//描画
void UI::Draw()
{
	//画像の描画
	Image::SetMatrix(ui_hPict, _worldMatrix);
	Image::Draw(ui_hPict);
}

//開放
void UI::Release()
{
}