#pragma once
#include "../Engine/IGameObject.h"

//◆◆◆を管理するクラス
class Logo : public IGameObject
{
	//画像番号
	int Logo_hPict;


public:
	//コンストラクタ
	Logo(IGameObject* parent);

	//デストラクタ
	~Logo();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//何かに当たった

};