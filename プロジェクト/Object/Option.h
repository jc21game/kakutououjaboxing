#pragma once
#include "Engine/GameObject/GameObject.h"

//◆◆◆を管理するクラス
class Option : public IGameObject
{
	int option_hPict;    //画像番号
	int _hSound;    //サウンド番号

public:
	//コンストラクタ
	Option(IGameObject* parent);

	//デストラクタ
	~Option();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;


	//何かに当たった
	//引数：pTarget 当たった相手
	void OnCollision(IGameObject *pTarget) override;


};