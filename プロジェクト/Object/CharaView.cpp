#include "CharaView.h"


//コンストラクタ
CharaView::CharaView(IGameObject * parent)
	:IGameObject(parent, "CharaView")
{
}

//デストラクタ
CharaView::~CharaView()
{
}

//初期化
void CharaView::Initialize()
{
	Camera* pCamera = CreateGameObject<Camera>(this);

	pCamera->SetPosition(D3DXVECTOR3(0, 10, 2.5));
	pCamera->SetTarget(D3DXVECTOR3(0, 10, 0));
	pCamera->SetRotate(0, 180, 0);

	//確認用のカメラ位置
	/*pCamera->SetPosition(D3DXVECTOR3(-10, 10, 5));
	pCamera->SetTarget(D3DXVECTOR3(0, 10, 0));
	pCamera->SetRotate(0, 180, 0);*/
}

//更新
void CharaView::Update()
{
}

//描画
void CharaView::Draw()
{
}

//開放
void CharaView::Release()
{
}