#include "ReB2C.h"


//コンストラクタ
ReB2C::ReB2C(IGameObject * parent)
	:IGameObject(parent, "ReB2C"), hPict_(-1)
{
}

//デストラクタ
ReB2C::~ReB2C()
{
}

//初期化
void ReB2C::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("data/Testbutton2.png");
	assert(hPict_ >= 0);
	//大きさを変える
	scale_ = D3DXVECTOR3(1.0f, 1.0f, 1.0f);
	//位置を変える
	position_ = D3DXVECTOR3(100.0f, 550.0f, 0);
}

//更新
void ReB2C::Update()
{
}

//描画
void ReB2C::Draw()
{
	Image::SetMatrix(hPict_, worldMatrix_);
	Image::Draw(hPict_);
}

//開放
void ReB2C::Release()
{
}