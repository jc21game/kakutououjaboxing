#include "Gauge_bar_2p.h"
#include "../PlayScene.h"
#include "Down2P.h"

//コンストラクタ
Gauge_bar_2p::Gauge_bar_2p(IGameObject * parent)
	:IGameObject(parent, "Gauge_bar_2p"), _hPict(-1),hit_(false), jab_hit_(false)
{
}

//デストラクタ
Gauge_bar_2p::~Gauge_bar_2p()
{
}

//初期化
void Gauge_bar_2p::Initialize()
{
	//モデルデータのロード
	_hPict = Image::Load("Data/gauge_bar.png");
	assert(_hPict >= 0);
	//大きさ、場所の設定
	scale_ = D3DXVECTOR3(0.0, 0.9, 0.9);
	position_ = D3DXVECTOR3(1570, -170, 0);


}

//更新
void Gauge_bar_2p::Update()
{
	if (Input::IsKeyDown(DIK_4))
	{
		//4キーを押すたびおおよそ10%増える
		hit_ = true;
	}

	//hit_がtrueを押すたびにおおよそ10%増える
	if (hit_ == true) {
		scale_.x -= 0.091;
		position_.x += 12.5;
		//100に固定されているときに4を押した場合0に戻す処理(仮)
		if (stanMAX == 1 && hit_ == true)
		{
			HitStop();
		}
		hit_ = false;
	}
	//ジャブでだいたい5％
	if (jab_hit_ == true) {
		scale_.x -= 0.0455;
		position_.x += 6.25;
		//100に固定されているときに4を押した場合0に戻す処理(仮)
		if (stanMAX == 1 && jab_hit_ == true)
		{
			HitStop();
		}
		jab_hit_ = false;
	}

	//体力が100%を超えた場合100に固定する処理
	if (scale_.x <= -0.91 && stanMAX == 0)
	{
		scale_.x = -0.91f;
		position_.x = 1695;
		stanMAX = 1;
	}
	//0%より大きかったら少しずつ減少するようにした
	if (scale_.x < 0.0f && scale_.x > -0.91f)
	{
		scale_.x += 0.00091;
		position_.x -= 0.125;
	}
}

//描画
void Gauge_bar_2p::Draw()
{
	Image::SetMatrix(_hPict, worldMatrix_);
	Image::Draw(_hPict);
}

//開放
void Gauge_bar_2p::Release()
{
}


//100に固定されているときに4を押した場合ダウンカウントを1増やし0に戻す処理
void Gauge_bar_2p::HitStop()
{
	scale_.x = 0.0f;
	position_.x = 1570;
	stanMAX = 0;
	((PlayScene*)GetParent())->GetDown_2p()->SetDown(true);
}
