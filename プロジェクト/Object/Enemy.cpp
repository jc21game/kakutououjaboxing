#include "Enemy.h"
#include "../Engine/Model.h"
#include "Enemy_Hand_decision.h"
#include "Enemy_Hand_decision_jab.h"
#include "Gauge_bar_2p.h"
#include  "../PlayScene.h"
//#include "Engine/gameObject/Camera.h"

//コンストラクタ
Enemy::Enemy(IGameObject * parent)
	:IGameObject(parent, "Enemy"), hModel_(-1), guardpoint(false), Rotate_(3.0),
	down_(false), tdown_(false), downjudge(false)
{
}

//デストラクタ
Enemy::~Enemy()
{
}

//初期化
void Enemy::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("data/motionfile.fbx");
	assert(hModel_ >= 0);

	/*Camera* pCamera = CreateGameObject<Camera>(this);

	pCamera->SetPosition(D3DXVECTOR3(0, 5, 0.5));
	pCamera->SetTarget(D3DXVECTOR3(0, 5, 1));

	*/

	//位置を変えている
	position_ = D3DXVECTOR3(0, 4, 5);
	//_scale = D3DXVECTOR3(0.7f, 0.7f, 0.7f);
	rotate_.y = 180;
	Model::SetAnimFrame(hModel_, 20, 40, 0.5f);

	//SphereCollider* head = new SphereCollider(D3DXVECTOR3(0.3, 9.5, 0), 0.6f);
	//AddCollider(head);//頭判定
	//BoxCollider* body = new BoxCollider(D3DXVECTOR3(0, 6.5, 0), D3DXVECTOR3(1.5, 3.5, 1));
	//AddCollider(body);//体判定
}

//更新
void Enemy::Update()
{
	//左スティックでオブジェクトを動かす
	//スティックの倒した方向に進む
	D3DXVECTOR3 stick = Input::GetPadStickL(1);

	D3DXMATRIX mRot;
	D3DXMatrixRotationY(&mRot, D3DXToRadian(rotate_.y));


	//移動ベクトル
	D3DXVECTOR3 move;
	D3DXVec3TransformCoord(&move, &D3DXVECTOR3(0, 0, 0.2f), &mRot);

	//ダウン中以外は行動可能
	if (downjudge == false)
	{
		//ガード中は動けない
		if (guardpoint == false) {
			//そのままだと早すぎるので割って遅くする
			position_.x -= (stick.y / 2.5f);
			position_.z += (stick.x / 2.5f);
			//キーボードのDを押されたときキャラを右に動かす
			if (Input::IsKey(DIK_D))
			{
				position_.z -= 0.5;
			}
			//キーボードのAを押されたときキャラを左に動かす
			if (Input::IsKey(DIK_A))
			{
				position_.z += 0.5;
			}
			//キーボードのWを押されたときキャラの向いてる方向に進ませる
			if (Input::IsKey(DIK_W))
			{
				position_ = position_ + move;
			}
			//キーボードのSを押されたときキャラの向いてる方向に下がらせる
			if (Input::IsKey(DIK_S))
			{
				position_ = position_ - move;
			}
			//キーボードのYを押されたときキャラが右回転する
			if (Input::IsKey(DIK_Y))
			{
				rotate_.y += Rotate_;
			}
			//キーボードのUを押されたときキャラが左回転する
			if (Input::IsKey(DIK_U))
			{
				rotate_.y -= Rotate_;
			}
		}

		// 指定範囲を超えないようにする
		if (position_.x < -20.0f)
		{
			position_.x = -20.0f;
		}
		if (position_.z < -20.0f)
		{
			position_.z = -20.0f;
		}
		if (20.0f < position_.x)
		{
			position_.x = 20.0f;
		}
		if (20.0f < position_.z)
		{
			position_.z = 20.0f;
		}
		//ここまで//////////////////

		if (Input::IsKeyDown(DIK_V))
		{
			if (Model::GetAnimFrame(hModel_) > 90 || Model::GetAnimFrame(hModel_) < 80)
			{
				Model::SetAnimFrame(hModel_, 80, 90, 0.5f);
			}
		}
		if (Model::GetAnimFrame(hModel_) == 90)
		{
			Model::SetAnimFrame(hModel_, 20, 40, 0.5f);//待機モーション
		}

		//Fキーでパンチ！！！！！！！！
		if (Input::IsKey(DIK_F) || Input::IsPadButtonDown(XINPUT_GAMEPAD_X, 1))
		{
			if (Model::GetAnimFrame(hModel_) > 55 || Model::GetAnimFrame(hModel_) < 45)
			{
				Model::SetAnimFrame(hModel_, 45, 55, 0.5f);

				//ハンド呼び出し
				Enemy_Hand_decision_jab* Ppunch_jab =
					CreateGameObject<Enemy_Hand_decision_jab>(FindObject("PlayScene"));

				//拳から攻撃判定を表示するための関数
				//ライトハンド参照
				D3DXVECTOR3 shotPos = Model::GetBonePosition(hModel_, "Character1_LeftHand");
				//ライトフィンガーベース参照
				D3DXVECTOR3 PunchRoot = Model::GetBonePosition(hModel_, "Character1_LeftFingerBase");
				//パンチの出し方？
				Ppunch_jab->Punch(shotPos, shotPos - PunchRoot);
			}
		}

		if (Model::GetAnimFrame(hModel_) == 55)
		{
			Model::SetAnimFrame(hModel_, 20, 40, 0.5f);//待機モーション
		}
		//Gで右ストレートでぶっとばす
		if (Input::IsKeyDown(DIK_G) || Input::IsPadButtonDown(XINPUT_GAMEPAD_Y, 1))
		{
			if (Model::GetAnimFrame(hModel_) > 73 || Model::GetAnimFrame(hModel_) < 60)
			{
				Model::SetAnimFrame(hModel_, 60, 73, 0.5f);
				IGameObject* playScene = FindObject("PlayScene");
				Enemy_Hand_decision* Ppunch = CreateGameObject<Enemy_Hand_decision>(FindObject("PlayScene"));

				//拳から攻撃判定を表示するための関数
				//ライトハンド参照
				D3DXVECTOR3 shotPos = Model::GetBonePosition(hModel_, "Character1_RightHand");
				//ライトフィンガーベース参照
				D3DXVECTOR3 PunchRoot = Model::GetBonePosition(hModel_, "Character1_RightFingerBase");
				//パンチの出し方？
				Ppunch->Punch(shotPos, shotPos - PunchRoot);

			}
		}
		if (Model::GetAnimFrame(hModel_) == 73)
		{
			Model::SetAnimFrame(hModel_, 20, 40, 0.5f);//待機モーション
		}
		//ガード
		if (Input::IsKey(DIK_H) ||
			(Input::IsPadButton(XINPUT_GAMEPAD_LEFT_SHOULDER, 1) &&
				Input::IsPadButton(XINPUT_GAMEPAD_RIGHT_SHOULDER, 1)))
		{
			if (Model::GetAnimFrame(hModel_) > 105 || Model::GetAnimFrame(hModel_) < 95)
			{
				Model::SetAnimFrame(hModel_, 95, 105, 0.5f);
				guardpoint = true;
			}
		}
		if (Model::GetAnimFrame(hModel_) == 100)
		{
			Model::SetAnimFrame(hModel_, 100, 105, 0.5f);//待機モーション
		}
		if (Input::IsKeyUp(DIK_H) ||
			Input::IsPadButtonUp(XINPUT_GAMEPAD_LEFT_SHOULDER, 1) ||
			Input::IsPadButtonUp(XINPUT_GAMEPAD_RIGHT_SHOULDER, 1))
		{
			Model::SetAnimFrame(hModel_, 105, 110, 0.5f);
			guardpoint = false;
		}
		if (Model::GetAnimFrame(hModel_) == 110)
		{
			Model::SetAnimFrame(hModel_, 20, 40, 0.5f);//待機モーション
		}

		if (Input::IsKeyUp(DIK_H))
		{
			Model::SetAnimFrame(hModel_, 105, 110, 0.5f);
		}
		if (Model::GetAnimFrame(hModel_) == 110)
		{
			Model::SetAnimFrame(hModel_, 20, 40, 0.5f);//待機モーション
		}

		//フック(仮)
		if (Input::IsKeyUp(DIK_J) || Input::IsPadButtonDown(XINPUT_GAMEPAD_B, 1))
		{
			Model::SetAnimFrame(hModel_, 135, 153, 1.0f);
		}
		if (Model::GetAnimFrame(hModel_) == 153)
		{
			Model::SetAnimFrame(hModel_, 20, 40, 0.5f);//待機モーション
		}

		//アッパー
		if (Input::IsKeyUp(DIK_K) || Input::IsPadButtonDown(XINPUT_GAMEPAD_A, 1))
		{
			Model::SetAnimFrame(hModel_, 105, 110, 0.5f);
		}
		if (Model::GetAnimFrame(hModel_) == 110)
		{
			Model::SetAnimFrame(hModel_, 20, 40, 0.5f);//待機モーション
		}

		if (Model::GetAnimFrame(hModel_) == 55)
		{
			Model::SetAnimFrame(hModel_, 20, 40, 0.5f);//待機モーション
		}
	}
	/////////////////////////ダウン関係////////////////////////////////////
	//ダウン(仮)
	if (Input::IsKeyUp(DIK_L) || down_ == true)
	{
		Model::SetAnimFrame(hModel_, 155, 190, 0.5f);
		down_ = false;
		downjudge = true;
	}

	//倒れ継続
	if (Model::GetAnimFrame(hModel_) == 190)
	{
		Model::SetAnimFrame(hModel_, 190, 191, 0.5f);//待機モーション
	}

	//倒れているときにGで復帰
	if (Model::GetAnimFrame(hModel_) <= 190 && downjudge == true && Input::IsKeyUp(DIK_G))
	{
		Model::SetAnimFrame(hModel_, 191, 200, 0.5f);//起き上がりモーション
	}
	//復帰
	if (Model::GetAnimFrame(hModel_) == 200)
	{
		Model::SetAnimFrame(hModel_, 20, 40, 0.5f);//待機モーション
		downjudge = false;
	}

	//ノックアウト(仮)

	//ノックアウト(仮)
	if (Input::IsKeyUp(DIK_O) || tdown_ == true)
	{
		Model::SetAnimFrame(hModel_, 201, 220, 0.5f);
		tdown_ = false;
		downjudge = true;
	}
	if (Model::GetAnimFrame(hModel_) == 220)
	{
		Model::SetAnimFrame(hModel_, 221, 300, 0.5f);//待機モーション
		//3つたまったらRESULTへ変更
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		//changeScebeの引数nextをRESULTに
		pSceneManager->ChangeScene(SCENE_ID_RESULT);
	}
	//////////////////////ダウン関係おわり///////////////////////////////

	//カメラがついてく
	D3DXMatrixLookAtLH(&view_, &(position_ + (D3DXVECTOR3(0, 10, 0))), &(position_ + (D3DXVECTOR3(0, 10, -1)))
		, &D3DXVECTOR3(0, 1, 0));
}

//描画
void Enemy::Draw()
{
	Model::SetMatrix(hModel_, worldMatrix_);
	Model::Draw(hModel_);
}

//開放
void Enemy::Release()
{

}

void Enemy::OnCollision(IGameObject * pTarget)
{
	//弾に当たった
	if (pTarget->GetObjectName() == "Chara_Hand_decision")
	{
		//ダメージを受けたい
		//((Gauge_bar_2p*)pTarget)->SetHit(true);
		//ペアレントで自分の親を生成。playsceneからGetBarをキャストして呼び出し、
		//Sethitを呼びだした
		((PlayScene*)GetParent())->GetBar_2p()->SetHit(true);

		//相手（弾）を消す
		pTarget->KillMe();
	}

	//ジャブに当たった
	if (pTarget->GetObjectName() == "Chara_Hand_decision_jab")
	{
		//ダメージを受けたい
		((PlayScene*)GetParent())->GetBar_2p()->JobSetHit(true);
		//相手（弾）を消す
		pTarget->KillMe();
	}
}