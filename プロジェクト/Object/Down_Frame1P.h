#pragma once
#include "../Engine/IGameObject.h"
#include "../Engine/Image.h"


//◆◆◆を管理するクラス
class Down_Frame1P : public IGameObject
{
	int _hPict;    //画像番号
public:
	//コンストラクタ
	Down_Frame1P(IGameObject* parent);

	//デストラクタ
	~Down_Frame1P();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};