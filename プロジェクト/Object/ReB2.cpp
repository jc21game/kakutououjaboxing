#include "ReB2.h"


//コンストラクタ
ReB2::ReB2(IGameObject * parent)
	:IGameObject(parent, "ReB2"), hPict_(-1)
{
}

//デストラクタ
ReB2::~ReB2()
{
}

//初期化
void ReB2::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("data/Testbutton.png");
	assert(hPict_ >= 0);
	//大きさを変える
	scale_ = D3DXVECTOR3(1.0f, 1.0f, 1.0f);
	//位置を変える
	position_ = D3DXVECTOR3(100.0f, 550.0f, 0);
}

//更新
void ReB2::Update()
{
}

//描画
void ReB2::Draw()
{
	Image::SetMatrix(hPict_, worldMatrix_);
	Image::Draw(hPict_);
}

//開放
void ReB2::Release()
{
}