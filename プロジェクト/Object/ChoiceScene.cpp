#include "ChoiceScene.h"
#include "Engine/ResouceManager/Image.h"
#include "BACK.h"
#include "Fight.h"
#include "UI.h"
#include "Loading.h"
#include "rule.h"

//コンストラクタ
ChoiceScene::ChoiceScene(IGameObject * parent)
	: IGameObject(parent, "ChoiceScene"), _hPict(-1)
{
}

//初期化
void ChoiceScene::Initialize()
{
	


	CreateGameObject<rule>(this);
	//CreateGameObject<Fight>(this);
	//CreateGameObject<UI>(this);
}

//更新
void ChoiceScene::Update()
{
	//スぺスキーを押すと
	if (Input::IsKeyDown(DIK_SPACE) || Input::IsPadButtonDown(XINPUT_GAMEPAD_A))
	{
		CreateGameObject<Loading>(this);
		//バトルシーンへ切り替わる
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_FIGHT);

	}
}

//描画
void ChoiceScene::Draw()
{
	Image::SetMatrix(_hPict, _worldMatrix);
	Image::Draw(_hPict);
}

//開放
void ChoiceScene::Release()
{
}

