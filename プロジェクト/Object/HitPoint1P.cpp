#include "HitPoint1P.h"


//コンストラクタ
HitPoint1P::HitPoint1P(IGameObject * parent)
	:IGameObject(parent, "HitPoint1P"), _hPict(-1)
{
}

//デストラクタ
HitPoint1P::~HitPoint1P()
{
}

//初期化
void HitPoint1P::Initialize()
{
	//画像データのロード
	_hPict = Image::Load("data/gaugeframe_1p.png");
	assert(_hPict >= 0);
	//大きさを変える
	scale_ = D3DXVECTOR3(0.9f, 0.9f, 0.9f);
	//位置を変える
	position_ = D3DXVECTOR3(-100, -170, 0);
}

//更新
void HitPoint1P::Update()
{
}

//描画
void HitPoint1P::Draw()
{
	Image::SetMatrix(_hPict, worldMatrix_);
	Image::Draw(_hPict);
}

//開放
void HitPoint1P::Release()
{
}