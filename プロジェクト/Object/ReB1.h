#pragma once
#include "../Engine/IGameObject.h"
#include "../Engine/Image.h"

//●●を管理するクラス
class ReB1: public IGameObject
{
	int hPict_; //画像番号
	int fps;
	int cnt = 0;
public:
	//コンストラクタ
	ReB1(IGameObject* parent);

	//デストラクタ
	~ReB1();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};