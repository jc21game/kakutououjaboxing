
#include "Enemy_Hand_decision_jab.h"
#include "Chara.h"
#include "Enemy.h"

//コンストラクタ
Enemy_Hand_decision_jab::Enemy_Hand_decision_jab(IGameObject * parent)
	:IGameObject(parent, "Enemy_Hand_decision_jab"), _move(D3DXVECTOR3(0, 0, 0)), _dz(0.5f),
	SPEED(0.5f)
{
}

//デストラクタ
Enemy_Hand_decision_jab::~Enemy_Hand_decision_jab()
{
}

//初期化
void Enemy_Hand_decision_jab::Initialize()
{
	////衝突判定
	//SphereCollider* collision = new SphereCollider(D3DXVECTOR3(2.0, 0.0, 1.0), 0.5f);
	//AddCollider(collision);
}

//更新
void Enemy_Hand_decision_jab::Update()
{

	//キーボードの右を押されたときキャラを右に動かす
	if (Input::IsKey(DIK_RIGHT))
	{
		position_.x += 0.3;
	}
	//キーボードの左を押されたときキャラを左に動かす
	if (Input::IsKey(DIK_LEFT))
	{

		position_.x -= 0.3;
	}
	//キーボードの上を押されたときキャラを上に動かす
	if (Input::IsKey(DIK_UP))
	{
		position_.z += 0.5;
	}
	//キーボードの下を押されたときキャラを下に動かす
	if (Input::IsKey(DIK_DOWN))
	{

		position_.z -= 0.5;
	}

	//攻撃判定の移動
	position_ += _move;
	_move.z -= _dz;
	_dz += 0.01f;

	//こぶし判定くらいのところに飛ばしたら消える
	if (_dz > 0.55)
	{
		KillMe();
	}
}

//描画
void Enemy_Hand_decision_jab::Draw()
{
}

//開放
void Enemy_Hand_decision_jab::Release()
{
}
//発射
void Enemy_Hand_decision_jab::Punch(D3DXVECTOR3 position, D3DXVECTOR3 direction)
{
	//位置
	position_ = position;
	//移動は、引数で渡された方向にSPEEDの速さ
	D3DXVec3Normalize(&_move, &direction);
	_move *= SPEED;
}

//何かに当たった
void Enemy_Hand_decision_jab::OnCollision(IGameObject * pTarget)
{

}