#pragma once
#include "../Engine/IGameObject.h"
#include "../Engine/Image.h"


//ルール画像を管理するクラス
class rule : public IGameObject
{
	int hPict_;    //画像番号
public:
	//コンストラクタ
	rule(IGameObject* parent);

	//デストラクタ
	~rule();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};