#pragma once
#include "../Engine/IGameObject.h"


//キャラを管理するクラス
class Chara : public IGameObject
{
	int hModel_;    //画像番号
	const float Rotate_;
	bool downjudge;	//ダウンの判定
	bool down_;		//ワンダウンの判定
	bool tdown_;	//スリーダウンの判定
	bool guardpoint; //ガード自動かない用とダメージ軽減判定？
	


public:

	//コンストラクタ
	Chara(IGameObject* parent);

	//デストラクタ
	~Chara();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//何かに当たった
	//引数：pTarget 当たった相手
	void OnCollision(IGameObject *pTarget) override;

	int GetModelHandle() { return hModel_; }

	
};