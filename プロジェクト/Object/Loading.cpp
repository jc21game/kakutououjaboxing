#include "Loading.h"
#include "Engine/ResouceManager/Image.h"
#include "assert.h"



//コンストラクタ
Loading::Loading(IGameObject * parent)
	:IGameObject(parent, "Loading"), _hPict(-1)
{
}

//デストラクタ
Loading::~Loading()
{
}

//初期化
void Loading::Initialize()
{
	//画像データのロード
	_hPict = Image::Load("data/NowLoading0.png");
	assert(_hPict >= 0);
	//大きさを変える
	_scale = D3DXVECTOR3(0.9f, 0.9f, 0.9f);
	//位置を変える
	_position = D3DXVECTOR3(700.0f, 550.0f, 0);

}

//更新
void Loading::Update()
{



}


//描画
void Loading::Draw()
{
	Image::SetMatrix(_hPict, _worldMatrix);
	Image::Draw(_hPict);
}

//開放
void Loading::Release()
{
}