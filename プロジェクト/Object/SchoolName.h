#pragma once
#include "../Engine/IGameObject.h"

//◆◆◆を管理するクラス
class SchoolName : public IGameObject
{
	//画像番号
	int SchoolName_hPict;
	

public:
	//コンストラクタ
	SchoolName(IGameObject* parent);

	//デストラクタ
	~SchoolName();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//何かに当たった

};