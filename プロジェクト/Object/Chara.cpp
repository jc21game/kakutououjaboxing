#include "Chara.h"
#include "../Engine/Model.h"
#include "CharaView.h"
#include "Chara_Hand_decision.h"
#include "Chara_Hand_decision_jab.h"
#include "Gauge_bar_1p.h"
#include "../PlayScene.h"

//コンストラクタ
Chara::Chara(IGameObject * parent)
	:IGameObject(parent, "Chara"), hModel_(-1), guardpoint(false), Rotate_(3.0),
	down_(false), tdown_(false), downjudge(false)
{

}

//デストラクタ
Chara::~Chara()
{
}

//初期化
void Chara::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("data/motionfile.fbx");
	assert(hModel_ >= 0);





	//位置を変えている
	position_ = D3DXVECTOR3(0, 4, -10);
	//_scale = D3DXVECTOR3(0.7f, 0.7f, 0.7f);
	Model::SetAnimFrame(hModel_, 20, 40, 0.5f);

	

}

//更新
void Chara::Update()
{
	/*Enemy* pEnemy = (Enemy*)FindObject("Enemy");    //ステージオブジェクトを探す
	int hEnemyModel = pEnemy->GetModelHandle();    //モデル番号を取得

	RayCastData data;
	data.start = position_;              //レイの発射位置
	data.dir = D3DXVECTOR3(0, -1, 0);    //レイの方向
	Model::RayCast(hEnemyModel, &data); //レイを発射

	//レイが当たったら
	if (data.hit)
	{
		//その分位置を下げる
		position_.y -= data.dist;
	}*/

	//左スティックでオブジェクトを動かす
	//スティックの倒した方向に進む
	D3DXVECTOR3 stick = Input::GetPadStickL(0);

	D3DXMATRIX mRot;
	D3DXMatrixRotationY(&mRot, D3DXToRadian(rotate_.y));

	//移動ベクトル
	D3DXVECTOR3 move;
	D3DXVec3TransformCoord(&move, &D3DXVECTOR3(0, 0, 0.2f), &mRot);

	//ダウン中以外行動可能
	if (downjudge == false)
	{

		if (guardpoint == false) {
			//そのままだと早すぎるので割って遅くする
			position_.x -= (stick.y / 2.5f);
			position_.z += (stick.x / 2.5f);

			//キーボードの右を押されたときキャラを右に動かす
			if (Input::IsKey(DIK_RIGHT))
			{
				position_.z += 0.5;
			}
			//キーボードの左を押されたときキャラを左に動かす
			if (Input::IsKey(DIK_LEFT))
			{
				position_.z -= 0.5;
			}
			//キーボードのWを押されたときキャラの向いてる方向に進ませる
			if (Input::IsKey(DIK_UP))
			{
				position_ = position_ + move;
			}
			//キーボードのSを押されたときキャラの向いてる方向に下がらせる
			if (Input::IsKey(DIK_DOWN))
			{
				position_ = position_ - move;
			}
			//キーボードのEを押されたときキャラが右回転する
			if (Input::IsKey(DIK_E))
			{
				rotate_.y += Rotate_;
			}
			//キーボードのRを押されたときキャラが左回転する
			if (Input::IsKey(DIK_R))
			{
				rotate_.y -= Rotate_;
			}

		}

		// 指定範囲を超えないようにする
		if (position_.x < -20.0f)
		{
			position_.x = -20.0f;
		}
		if (position_.z < -20.0f)
		{
			position_.z = -20.0f;
		}
		if (20.0f < position_.x)
		{
			position_.x = 20.0f;
		}
		if (20.0f < position_.z)
		{
			position_.z = 20.0f;
		}
		//ここまで//////////////////

		//Zキーでジャブ
		if (Input::IsKeyDown(DIK_Z) || Input::IsPadButtonDown(XINPUT_GAMEPAD_X, 0))
		{
			if (Model::GetAnimFrame(hModel_) > 55 || Model::GetAnimFrame(hModel_) < 45)
			{
				Model::SetAnimFrame(hModel_, 45, 55, 0.5f);

				//ハンド呼び出し
				Chara_Hand_decision_jab* Ppunch_jab =
					CreateGameObject<Chara_Hand_decision_jab>(FindObject("PlayScene"));

				//拳から攻撃判定を表示するための関数
				//ライトハンド参照
				D3DXVECTOR3 shotPos = Model::GetBonePosition(hModel_, "Character1_LeftHand");
				//ライトフィンガーベース参照
				D3DXVECTOR3 PunchRoot = Model::GetBonePosition(hModel_, "Character1_LeftFingerBase");
				//パンチの出し方？
				Ppunch_jab->Punch(shotPos, shotPos - PunchRoot);
			}
		}

		if (Model::GetAnimFrame(hModel_) == 55)
		{
			Model::SetAnimFrame(hModel_, 20, 40, 0.5f);//待機モーション
		}

		//Xキーは右ストレートでぶっとばす
		if ((Input::IsKeyDown(DIK_X) && down_ != true) || Input::IsPadButtonDown(XINPUT_GAMEPAD_Y, 0))
		{
			if (Model::GetAnimFrame(hModel_) > 73 || Model::GetAnimFrame(hModel_) < 60 //ストレートのフレーム
				&& Model::GetAnimFrame(hModel_) > 50 || Model::GetAnimFrame(hModel_) < 45)//ジャブのフレーム
			{
				Model::SetAnimFrame(hModel_, 60, 73, 0.5f);
				IGameObject* playScene = FindObject("PlayScene");
				Chara_Hand_decision* Ppunch = CreateGameObject<Chara_Hand_decision>(FindObject("PlayScene"));

				//拳から攻撃判定を表示するための関数
				//ライトハンド参照
				D3DXVECTOR3 shotPos = Model::GetBonePosition(hModel_, "Character1_RightHand");
				//ライトフィンガーベース参照
				D3DXVECTOR3 PunchRoot = Model::GetBonePosition(hModel_, "Character1_RightFingerBase");
				//パンチの出し方？
				Ppunch->Punch(shotPos, shotPos - PunchRoot);

			}
		}
		if (Model::GetAnimFrame(hModel_) == 73)
		{
			Model::SetAnimFrame(hModel_, 20, 40, 0.5f);//待機モーション
		}

		//Cキーおしっぱでガード
		if (Input::IsKey(DIK_C) ||
			(Input::IsPadButton(XINPUT_GAMEPAD_LEFT_SHOULDER, 0) &&
				Input::IsPadButton(XINPUT_GAMEPAD_RIGHT_SHOULDER, 0)))
		{
			if (Model::GetAnimFrame(hModel_) > 105 || Model::GetAnimFrame(hModel_) < 95)
			{
				Model::SetAnimFrame(hModel_, 95, 105, 0.5f);
			}
			guardpoint = true;
		}
		if (Model::GetAnimFrame(hModel_) == 100)
		{
			Model::SetAnimFrame(hModel_, 100, 105, 0.5f);//待機モーション
		}
		//ガードを話した時
		if (Input::IsKeyUp(DIK_C) ||
			Input::IsPadButtonUp(XINPUT_GAMEPAD_LEFT_SHOULDER, 0) ||
			Input::IsPadButtonUp(XINPUT_GAMEPAD_RIGHT_SHOULDER, 0))
		{
			Model::SetAnimFrame(hModel_, 105, 110, 0.5f);
			guardpoint = false;
		}

		if (Model::GetAnimFrame(hModel_) == 110)
		{
			Model::SetAnimFrame(hModel_, 20, 40, 0.5f);//待機モーション
		}

		//フック
		if (Input::IsKeyUp(DIK_V) || Input::IsPadButtonDown(XINPUT_GAMEPAD_B, 0))
		{
			Model::SetAnimFrame(hModel_, 135, 153, 1.0f);
		}
		if (Model::GetAnimFrame(hModel_) == 153)
		{
			Model::SetAnimFrame(hModel_, 20, 40, 0.5f);//待機モーション
		}

		//アッパー(現状なし)
		if (Input::IsKeyUp(DIK_B) || Input::IsPadButtonDown(XINPUT_GAMEPAD_A, 0))
		{
			Model::SetAnimFrame(hModel_, 105, 110, 0.5f);
		}
		if (Model::GetAnimFrame(hModel_) == 110)
		{
			Model::SetAnimFrame(hModel_, 20, 40, 0.5f);//待機モーション
		}
		if (Model::GetAnimFrame(hModel_) == 55)
		{
			Model::SetAnimFrame(hModel_, 20, 40, 0.5f);//待機モーション
		}

	}

	////////////////////////ダウン//////////////////////////
	if (Input::IsKeyUp(DIK_N) || down_ == true)
	{
		Model::SetAnimFrame(hModel_, 155, 190, 0.5f);
		down_ = false;
		downjudge = true;
	}
	//倒れ継続
	if (Model::GetAnimFrame(hModel_) == 190)
	{
		Model::SetAnimFrame(hModel_, 190, 191, 0.5f);//待機モーション
	}

	//倒れているときにXで復帰
	if (Model::GetAnimFrame(hModel_) <= 190 && downjudge == true && Input::IsKeyUp(DIK_X))
	{
		Model::SetAnimFrame(hModel_, 191, 200, 0.5f);//起き上がりモーション
	}
	//復帰
	if (Model::GetAnimFrame(hModel_) == 200)
	{
		Model::SetAnimFrame(hModel_, 20, 40, 0.5f);//待機モーション
		downjudge = false;
	}
	/////////////////ダウン表記おわり///////////////////////////////


	//ノックアウト(仮)
	if (Input::IsKeyUp(DIK_M) || tdown_ == true)
	{
		Model::SetAnimFrame(hModel_, 201, 220, 0.5f);
		tdown_ = false;
		downjudge = true;
	}
	if (Model::GetAnimFrame(hModel_) == 220)
	{
		Model::SetAnimFrame(hModel_, 221, 300, 0.5f);//待機モーション
	}
	//カメラがついてく
	D3DXMatrixLookAtLH(&view_, &(position_ + (D3DXVECTOR3(0, 10, 0))), &(position_ + (D3DXVECTOR3(0, 10, 1)))
		, &D3DXVECTOR3(0, 1, 0));
}

//描画
void Chara::Draw()
{
	Model::SetMatrix(hModel_, worldMatrix_);
	Model::Draw(hModel_);
}

//開放
void Chara::Release()
{
}

//何かに当たった
void Chara::OnCollision(IGameObject * pTarget)
{
	//弾に当たった
	if (pTarget->GetObjectName() == "Enemy_Hand_decision")
	{
		//ダメージを受けたい
		//((Gauge_bar_2p*)pTarget)->SetHit(true);
		//ペアレントで自分の親を生成。playsceneからGetBarをキャストして呼び出し、
		//CSethitを呼びだした
		((PlayScene*)GetParent())->GetBar_1p()->CSetHit(true);
		//相手（弾）を消す
		pTarget->KillMe();
	}
	if (pTarget->GetObjectName() == "Enemy_Hand_decision_jab")
	{
		//ダメージを受けたい
		((PlayScene*)GetParent())->GetBar_1p()->CJobSetHit(true);
		//相手（弾）を消す
		pTarget->KillMe();
	}
}