#include "ReB1C.h"


//コンストラクタ
ReB1C::ReB1C(IGameObject * parent)
	:IGameObject(parent, "ReB1C"), hPict_(-1)
{
}

//デストラクタ
ReB1C::~ReB1C()
{
}

//初期化
void ReB1C::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("data/Testbutton2.png");
	assert(hPict_ >= 0);
	//大きさを変える
	scale_ = D3DXVECTOR3(1.0f, 1.0f, 1.0f);
	//位置を変える
	position_ = D3DXVECTOR3(900.0f, 550.0f, 0);
}

//更新
void ReB1C::Update()
{
}

//描画
void ReB1C::Draw()
{
	Image::SetMatrix(hPict_, worldMatrix_);
	Image::Draw(hPict_);
}

//開放
void ReB1C::Release()
{
}