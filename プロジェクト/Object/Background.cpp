#include "Background.h"
#include "../Engine/Image.h"
#include "assert.h"
#include "stage.h"
#include "../Engine/Camera.h"


//Backボタンの表示					
//Backボタンが選択されたらメニューシーンへ切り替え	
//背景の表示



//コンストラクタ
Background::Background(IGameObject * parent)
	:IGameObject(parent, "Background")/*, _hPict(-1)*/
{
}

//デストラクタ
Background::~Background()
{
}

//初期化
void Background::Initialize()
{
	
	/*//画像データのロード
	_hPict = Image::Load("data/Background.png");
	assert(_hPict >= 0);
	//大きさを変える
	_scale = D3DXVECTOR3(1.0f, 1.0f, 1.0f);
	//位置を変える
	_position = D3DXVECTOR3(0.0f, 0.0f, 0);
	*/
	//カメラの設定
	Camera* pCamera = CreateGameObject<Camera>(this);
	pCamera->SetPosition(D3DXVECTOR3(0, 30,30));
}

//更新
void Background::Update()
{
	//向いている方向に回転させる行列
	D3DXMATRIX mRot;
	D3DXMatrixRotationY(&mRot, D3DXToRadian(rotate_.y));
	//回転させるスピード
	rotate_.y += 0.5f;
}

//描画
void Background::Draw()
{
	//Image::SetMatrix(_hPict, _worldMatrix);
	//Image::Draw(_hPict);

}

//開放
void Background::Release()
{
}