#include "ReB1.h"


//コンストラクタ
ReB1::ReB1(IGameObject * parent)
	:IGameObject(parent, "ReB1"), hPict_(-1)
{
}

//デストラクタ
ReB1::~ReB1()
{
}

//初期化
void ReB1::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("data/Testbutton.png");
	assert(hPict_ >= 0);
	//大きさを変える
	scale_ = D3DXVECTOR3(1.0f, 1.0f, 1.0f);
	//位置を変える
	position_ = D3DXVECTOR3(900.0f, 550.0f, 0);
}

//更新
void ReB1::Update()
{
}

//描画
void ReB1::Draw()
{
	Image::SetMatrix(hPict_, worldMatrix_);
	Image::Draw(hPict_);
}

//開放
void ReB1::Release()
{
}