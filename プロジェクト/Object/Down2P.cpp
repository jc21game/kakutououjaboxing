#include "Down2P.h"


//コンストラクタ
Down2P::Down2P(IGameObject * parent)
	:IGameObject(parent, "Down2P"), _hPict(-1), down_(false)
{
}

//デストラクタ
Down2P::~Down2P()
{
}

//初期化
void Down2P::Initialize()
{
	//画像データのロード
	_hPict = Image::Load("data/Down_gauge2P.png");
	assert(_hPict >= 0);
	//大きさを変える
	scale_ = D3DXVECTOR3(0.25f, 0.25f, 0.25f);
	//位置を変える
	position_ = D3DXVECTOR3(1325, 90, 0);
}

//更新
void Down2P::Update()
{

	//2が押されたら2pのスタン回数の表示(仮)
	//追加でゲージがリセットされたらダウン回数が増えるようにする
	if (Input::IsKeyDown(DIK_2) && DownCount == 0 || down_ == true && DownCount == 0)
	{
		//ワンダウン
		_hPict = Image::Load("data/Down_gauge2P_1Down.png");
		assert(_hPict >= 0);
		//ダウン回数1
		DownCount = 1;
		down_ = false;
	}
	else if (Input::IsKeyDown(DIK_2) && DownCount == 1 || down_ == true && DownCount == 1)
	{
		_hPict = Image::Load("data/Down_gauge2P_2Down.png");
		assert(_hPict >= 0);
		//ダウン回数2
		DownCount = 2;
		down_ = false;
	}
	else if (Input::IsKeyDown(DIK_2) && DownCount == 2 || down_ == true && DownCount == 2)
	{
		_hPict = Image::Load("data/Down_gauge2p_3Down.png");
		assert(_hPict >= 0);
		//ダウン回数3
		DownCount = 3;
		down_ = false;
		//3つたまったらRESULTへ変更
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		//changeScebeの引数nextをRESULTに
		pSceneManager->ChangeScene(SCENE_ID_RESULT);
	}
	//初期に戻せるか用に　いつか消える
	else if (Input::IsKeyDown(DIK_2) && DownCount == 3 || down_ == true && DownCount == 3)
	{
		_hPict = Image::Load("data/Down_gauge2P.png");
		assert(_hPict >= 0);
		DownCount = 0;
		down_ = false;
	}
}

//描画
void Down2P::Draw()
{
	Image::SetMatrix(_hPict, worldMatrix_);
	Image::Draw(_hPict);
}

//開放
void Down2P::Release()
{
}