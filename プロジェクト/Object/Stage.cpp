#include "Stage.h"
#include "../Engine/Model.h"

//コンストラクタ
Stage::Stage(IGameObject * parent)
	:IGameObject(parent, "Stage"), _hModel(-1)
{
}

//デストラクタ
Stage::~Stage()
{
}

//初期化
void Stage::Initialize()
{
	//モデルデータのロード
	_hModel = Model::Load("data/ring.fbx");
	assert(_hModel >= 0);

	scale_ = D3DXVECTOR3(2,1,2);
}

//更新
void Stage::Update()
{
}

//描画
void Stage::Draw()
{
	Model::SetMatrix(_hModel, worldMatrix_);
	Model::Draw(_hModel);
}

//開放
void Stage::Release()
{
}