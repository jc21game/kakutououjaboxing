/////////////////////////////////////
//////これコピッて使ってね///////////
/////////////////////////////////////

#include "TestObject.h"

//コンストラクタ
TestObject::TestObject(IGameObject * parent)
	:IGameObject(parent, "TestObject")
{
}

//デストラクタ
TestObject::~TestObject()
{
}

//初期化
void TestObject::Initialize()
{
}

//更新
void TestObject::Update()
{
}

//描画
void TestObject::Draw()
{
}

//開放
void TestObject::Release()
{
}
