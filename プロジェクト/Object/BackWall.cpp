#include "BackWall.h"
#include "../Engine/Camera.h"

//コンストラクタ
BackWall::BackWall(IGameObject * parent)
	:IGameObject(parent, "BackWall")
{
}

//デストラクタ
BackWall::~BackWall()
{
}

//初期化
void BackWall::Initialize()
{
	//BoxCollider* collision = new BoxCollider(D3DXVECTOR3(0, 6, -22), D3DXVECTOR3(40, 11, 3));//1Pから見て後ろ
	//当たり判定読み込み
	//AddCollider(collision);
}

//更新
void BackWall::Update()
{
}

//描画
void BackWall::Draw()
{
}

//開放
void BackWall::Release()
{
}