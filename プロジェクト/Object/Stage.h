#pragma once
#include "../Engine/IGameObject.h"

//◆◆◆を管理するクラス
class Stage : public IGameObject
{
	int _hModel;    //画像番号

public:
	//コンストラクタ
	Stage(IGameObject* parent);

	//デストラクタ
	~Stage();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};