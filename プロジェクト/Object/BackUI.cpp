#include "BackUI.h"
#include "Engine/ResouceManager/Image.h"
//コンストラクタ
BackUI::BackUI(IGameObject * parent)
	:IGameObject(parent, "BackUI"), backui_hPict(-1)//初期化
{
}

//デストラクタ
BackUI::~BackUI()
{
}

//初期化
void BackUI::Initialize()
{
	//画像データのロード
	backui_hPict = Image::Load("data/BackUI.png");
	assert(backui_hPict >= 0);
	//大きさを変える(元の画像より小さくした)
	_scale = D3DXVECTOR3(0.4f, 0.5f, 0.8f);

	//ポジションをBackボタンと重なるように変えた
	_position = D3DXVECTOR3(18.0f, 590.0f, 0);
	
	//当たり判定(箱型コライダー)
	SphereCollider* collision = new SphereCollider(D3DXVECTOR3(0, 0, 0), 3.0f);
	AddCollider(collision);
}

//更新
void BackUI::Update()
{

	
}

//描画
void BackUI::Draw()
{
	//画像を描画する
	Image::SetMatrix(backui_hPict, _worldMatrix);
	Image::Draw(backui_hPict);
}

//開放
void BackUI::Release()
{
}

