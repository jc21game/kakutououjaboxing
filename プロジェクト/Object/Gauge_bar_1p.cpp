#include "Gauge_bar_1p.h"
#include "../PlayScene.h"
#include "Down1P.h"

//コンストラクタ
Gauge_bar_1p::Gauge_bar_1p(IGameObject * parent)
	:IGameObject(parent, "Gauge_bar_1p"), _hPict(-1),chit_(false), cjab_hit_(false)
{
}

//デストラクタ
Gauge_bar_1p::~Gauge_bar_1p()
{
}

//初期化
void Gauge_bar_1p::Initialize()
{
	//モデルデータのロード
	_hPict = Image::Load("Data/gauge_bar.png");
	assert(_hPict >= 0);
	//大きさ、場所の設定
	scale_ = D3DXVECTOR3(0.0, 0.9, 0.9);
	position_ = D3DXVECTOR3(20, -170, 0);


}

//更新
void Gauge_bar_1p::Update()
{
	if (Input::IsKeyDown(DIK_3))
	{
		//3キーを押すたびおおよそ10%増える
		chit_ = true;
	}
	//hit_がtrueを押すたびにおおよそ10%増える
	if (chit_ == true) {
		scale_.x += 0.09f;
		position_.x -= 12;
		//100に固定されているときに当てた場合0に戻す処理(仮)
		if (stanMAX == 1 && chit_ == true)
		{
			CHitStop();
		}
		chit_ = false;
	}
	//ジャブでだいたい5%
	if (cjab_hit_ == true) {
		scale_.x += 0.045f;
		position_.x -= 6;
		//100に固定されているときに当てた場合0に戻す処理(仮)
		if (stanMAX == 1 && cjab_hit_ == true)
		{
			CHitStop();
		}
		cjab_hit_ = false;
	}

	//体力が100%を超えた場合100に固定する処理
	if (scale_.x >= 0.9 && stanMAX == 0)
	{
		scale_.x = 0.9f;
		position_.x = -100;
		stanMAX = 1;
	}
	//0%より大きかったら少しずつ減少するようにした
	if (scale_.x > 0.0f && scale_.x < 0.9f)
	{
		scale_.x -= 0.0009;
		position_.x += 0.12;
	}
}

//描画
void Gauge_bar_1p::Draw()
{
	Image::SetMatrix(_hPict, worldMatrix_);
	Image::Draw(_hPict);
}

//開放
void Gauge_bar_1p::Release()
{
}

void Gauge_bar_1p::CHitStop()
{
	scale_.x = 0.0f;
	position_.x = 20;
	stanMAX = 0;
	((PlayScene*)GetParent())->GetDown_1p()->SetDown(true);
}
