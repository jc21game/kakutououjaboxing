#pragma once
#include "../Engine/IGameObject.h"
#include "../Engine/Image.h"

//◆◆◆を管理するクラス
class Gauge_bar_1p : public IGameObject
{
	int _hPict;    //画像番号
	int stanMAX = 0; //体力100%
	bool chit_;		//falseとtrueで判断
	bool cjab_hit_;	//ジャブ版

public:
	//コンストラクタ
	Gauge_bar_1p(IGameObject* parent);

	//デストラクタ
	~Gauge_bar_1p();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	void CHitStop();

	void CSetHit(bool chit)
	{
		chit_ = chit;
	}

	void CJobSetHit(bool cjab_hit)
	{
		cjab_hit_ = cjab_hit;
	}
};