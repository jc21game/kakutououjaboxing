#pragma once
#include "../Engine/IGameObject.h"
#include "../Engine/Image.h"

//◆◆◆を管理するクラス
class HitPoint2P : public IGameObject
{
	int _hPict;    //画像番号
public:
	//コンストラクタ
	HitPoint2P(IGameObject* parent);

	//デストラクタ
	~HitPoint2P();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};