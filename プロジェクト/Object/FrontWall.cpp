#include "FrontWall.h"
#include "../Engine/Camera.h"

//コンストラクタ
FrontWall::FrontWall(IGameObject * parent)
	:IGameObject(parent, "FrontWall")
{
}

//デストラクタ
FrontWall::~FrontWall()
{
}

//初期化
void FrontWall::Initialize()
{
	//BoxCollider* collision = new BoxCollider(D3DXVECTOR3(0, 6, 22), D3DXVECTOR3(40, 11, 3));//1Pから見て前
	//当たり判定読み込み
	//AddCollider(collision);
}

//更新
void FrontWall::Update()
{
}

//描画
void FrontWall::Draw()
{
}

//開放
void FrontWall::Release()
{
}