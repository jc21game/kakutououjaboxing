#pragma once
#include "../Engine/IGameObject.h"
#include "../Engine/Image.h"

//●●を管理するクラス
class ReB2C : public IGameObject
{
	int hPict_; //画像番号
	int fps;
	int cnt = 0;
public:
	//コンストラクタ
	ReB2C(IGameObject* parent);

	//デストラクタ
	~ReB2C();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};