#pragma once
#include "../Engine/IGameObject.h"
#include "../Engine/Model.h"

//◆◆◆を管理するクラス
class Enemy_Hand_decision : public IGameObject
{
	D3DXVECTOR3 _move;	//移動ベクトル
	float _dz;			//Ｙ方向の加速度
	const float SPEED;	//速度
public:
	//コンストラクタ
	Enemy_Hand_decision(IGameObject* parent);

	//デストラクタ
	~Enemy_Hand_decision();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	void Punch(D3DXVECTOR3 position, D3DXVECTOR3 direction);

	void OnCollision(IGameObject *pTarget) override;

};
