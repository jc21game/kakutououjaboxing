#include "Down1P.h"


//コンストラクタ
Down1P::Down1P(IGameObject * parent)
	:IGameObject(parent, "Down1P"), _hPict(-1), down_(false)
{
}

//デストラクタ
Down1P::~Down1P()
{
}

//初期化
void Down1P::Initialize()
{
	//画像データのロード
	_hPict = Image::Load("data/Down_gauge1P.png");
	assert(_hPict >= 0);
	//大きさを変える
	scale_ = D3DXVECTOR3(0.25f, 0.25f, 0.25f);
	//位置を変える
	position_ = D3DXVECTOR3(10, 90, 0);
}

//更新
void Down1P::Update()
{
	//1が押されたら1pのスタン回数の表示(仮)
	if (Input::IsKeyDown(DIK_1) && DownCount == 0 || down_ == true && DownCount == 0)
	{
		//ワンダウン
		_hPict = Image::Load("data/Down_gauge1P_1Down.png");
		assert(_hPict >= 0);
		//ダウン回数1
		DownCount = 1;
		down_ = false;
	}
	else if (Input::IsKeyDown(DIK_1) && DownCount == 1 || down_ == true && DownCount == 1)
	{
		_hPict = Image::Load("data/Down_gauge1P_2Down.png");
		assert(_hPict >= 0);
		//ダウン回数2
		DownCount = 2;
		down_ = false;
	}
	else if (Input::IsKeyDown(DIK_1) && DownCount == 2 || down_ == true && DownCount == 2)
	{
		_hPict = Image::Load("data/Down_gauge1p_3Down.png");
		assert(_hPict >= 0);
		//ダウン回数3
		DownCount = 3;
		down_ = false;
	}
	//初期に戻せるか用に　いつか消える
	else if (Input::IsKeyDown(DIK_1) && DownCount == 3 || down_ == true && DownCount == 3)
	{
		_hPict = Image::Load("data/Down_gauge1P.png");
		assert(_hPict >= 0);
		DownCount = 0;
		down_ = false;
	}
}

//描画
void Down1P::Draw()
{
	Image::SetMatrix(_hPict, worldMatrix_);
	Image::Draw(_hPict);
}

//開放
void Down1P::Release()
{
}