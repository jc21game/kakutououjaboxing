#pragma once
#include "Fbx.h"

//プレイヤーを管理するクラス
class Player : public IGameObject
{
	int hModel_;

public:
	//コンストラクタ
	Player(IGameObject* parent);

	//デストラクタ
	~Player();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	void OnCollision(IGameObject* pTarget)	override;
};