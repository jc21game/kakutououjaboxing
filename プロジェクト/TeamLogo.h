#pragma once
#include "Engine/IGameObject.h"

//チームロゴを管理するクラス
class TeamLogo : public IGameObject
{
	//画像番号
	int hPict_;

public:
	//コンストラクタ
	TeamLogo(IGameObject* parent);

	//デストラクタ
	~TeamLogo();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

};