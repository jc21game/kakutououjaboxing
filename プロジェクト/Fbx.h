﻿#pragma once
#include <fbxsdk.h>
#include "Global.h"

#pragma comment(lib,"libfbxsdk-mt.lib")

class Fbx
{
	struct Vertex
	{
		D3DXVECTOR3 pos;
		D3DXVECTOR3 normal;
		D3DXVECTOR2 uv;
	};

	FbxManager*  pManager_;
	FbxImporter* pImporter_;
	FbxScene*    pScene_;


	LPDIRECT3DVERTEXBUFFER9	pVertexBuffer_;
	LPDIRECT3DINDEXBUFFER9*	ppIndexBuffer_;

	LPDIRECT3DTEXTURE9*	pTexture_; //テクスチャ
	D3DMATERIAL9*		pMaterial_;

	int vertexCount_;
	int polygonCount_;
	int indexCount_;
	int materialCount_;
	int* polygonCountOfMaterial_;

	void CheckNode(FbxNode* pNode);
	void CheckMesh(FbxMesh* pMesh);

public:
	Fbx();
	~Fbx();

	void Load(const char* fileName);
	void Draw(const D3DXMATRIX &matrix);
};