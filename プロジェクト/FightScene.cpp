#include "FightScene.h"


#include "Object/Chara.h"
#include "Object/Enemy.h"
#include "Object/Stage.h"
#include "Object/BACK.h"
#include "Object/HitPoint1P.h"
#include "Object/HitPoint2P.h"
#include "Object/Down1P.h"
#include "Object/Down2P.h"
#include "Object/Down_Frame1P.h"
#include "Object/Down_Frame2P.h"
#include "Object/Gauge_bar_1p.h"
#include "Object/Gauge_bar_2p.h"

//コンストラクタ
FightScene::FightScene(IGameObject * parent)
	: IGameObject(parent, "FightScene"), hPict_(-1)
{
}

//初期化
void FightScene::Initialize()
{


	//画像データのロード
	/*_hPict = Image::Load("data/haikei.png");
	assert(_hPict >= 0);*/

	//こやつらは孤独な生き物なのでポインタを上げないとこいつらだけでかかわることができない
	//悲しいやつらだ
	CreateGameObject<Chara>(this);
	CreateGameObject<Enemy>(this);
	CreateGameObject<Stage>(this);
	CreateGameObject<BACK>(this);
	pDown_1p = CreateGameObject<Down1P>(this);
	pDown_2p = CreateGameObject<Down2P>(this);
	CreateGameObject<Down_Frame1P>(this);
	CreateGameObject<Down_Frame2P>(this);
	pBar_1p = CreateGameObject<Gauge_bar_1p>(this);
	pBar_2p = CreateGameObject<Gauge_bar_2p>(this);
	CreateGameObject<HitPoint1P>(this);
	CreateGameObject<HitPoint2P>(this);

}

//更新
void FightScene::Update()
{
	//本来なら死んだor倒したでリザルトだけどまだ処理がないので仮で
	//SPACEを押したら起動するようにする
	if (Input::IsKeyDown(DIK_SPACE))
	{
		SceneManager::ChangeScene(SCENE_ID_RESULT);
	}
}

//描画
void FightScene::Draw()
{
	Image::SetMatrix(hPict_, worldMatrix_);
	Image::Draw(hPict_);
}

//開放
void FightScene::Release()
{
}

void FightScene::DrawSub()
{

	//左画面
	{
		//ビューポート矩形
		D3DVIEWPORT9 vp;
		vp.X = 0;
		vp.Y = 0;
		vp.Width = g.screenWidth / 2;
		vp.Height = g.screenHeight;
		vp.MinZ = 0;
		vp.MaxZ = 1;
		Direct3D::pDevice_->SetViewport(&vp);
		
		//ビュー行列auto a = FindChildObject("Chara");

		Chara* a = (Chara*)FindChildObject("Chara");
		

		/*D3DXMATRIX view;
		D3DXMatrixLookAtLH(&view, &D3DXVECTOR3(0, 14, 6), &D3DXVECTOR3(0, 15, 0), &D3DXVECTOR3(0, 1, 0));*/
		Direct3D::pDevice_->SetTransform(D3DTS_VIEW, &a->GetView());

		//シーンのオブジェクトを表示
		for (auto it = childList_.begin(); it != childList_.end(); it++)
		{
			(*it)->DrawSub();
		}
	}

	//右画面
	{
		//ビューポート矩形
		D3DVIEWPORT9 vp;
		vp.X = g.screenWidth / 2;
		vp.Y = 0;
		vp.Width = g.screenWidth / 2;
		vp.Height = g.screenHeight;
		vp.MinZ = 0;
		vp.MaxZ = 1;
		Direct3D::pDevice_->SetViewport(&vp);

		//ビュー行列auto a = FindChildObject("Chara");

		Enemy* a = (Enemy*)FindChildObject("Enemy");


		
		Direct3D::pDevice_->SetTransform(D3DTS_VIEW, &a->GetView());

		
		//シーンのオブジェクトを表示
		for (auto it = childList_.begin(); it != childList_.end(); it++)
		{
			(*it)->DrawSub();
		}
	}

	//戻す
	{
		//ビューポート矩形
		D3DVIEWPORT9 vp;
		vp.X = 0;
		vp.Y = 0;
		vp.Width = g.screenWidth;
		vp.Height = g.screenHeight;
		vp.MinZ = 0;
		vp.MaxZ = 1;
		Direct3D::pDevice_->SetViewport(&vp);
	}
}
