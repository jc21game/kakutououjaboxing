#include "ChoiceScene.h"

#include "Object/BACK.h"
#include "Object/Fight.h"
#include "Object/UI.h"
#include "Loading2.h"
#include "Object/rule.h"

//コンストラクタ
ChoiceScene::ChoiceScene(IGameObject * parent)
	: IGameObject(parent, "ChoiceScene"), hPict_(-1)
{
}

//初期化
void ChoiceScene::Initialize()
{



	CreateGameObject<rule>(this);
	//CreateGameObject<Fight>(this);
	//CreateGameObject<UI>(this);
}

//更新
void ChoiceScene::Update()
{
	//スぺスキーを押すと
	if (Input::IsKeyDown(DIK_SPACE) || Input::IsPadButtonDown(XINPUT_GAMEPAD_A))
	{
		CreateGameObject<Loading2>(this);
		//バトルシーンへ切り替わる
		
		SceneManager::ChangeScene(SCENE_ID_FIGHT);

	}
}

//描画
void ChoiceScene::Draw()
{
	
}

//開放
void ChoiceScene::Release()
{
}

