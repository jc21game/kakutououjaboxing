#pragma once
#include "Engine/IGameObject.h"

//プレスを管理するクラス
class Press : public IGameObject
{

	int hPict_;    //画像番号
public:
	//コンストラクタ
	Press(IGameObject* parent);

	//デストラクタ
	~Press();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};
