/////////////////////////////////////
//////これコピッて使ってね///////////
/////////////////////////////////////

#include "bootScene.h"


//コンストラクタ
BootScene::BootScene(IGameObject * parent)
	: IGameObject(parent, "BootScene")
{
}

//初期化
void BootScene::Initialize()
{
}

//更新
void BootScene::Update()
{
	//SPACEが押されたら
	if (Input::IsKeyDown(DIK_SPACE))
	{
		//スプラッシュシーンに切り替わる
		SceneManager::ChangeScene(SCENE_ID_SPLASH);

	}
}

//描画
void BootScene::Draw()
{
}

//開放
void BootScene::Release()
{
}
