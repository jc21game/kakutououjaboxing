#include "ResultScene.h"

#include "Object/ReB1.h"
#include "Object/ReB1C.h"
#include "Object/ReB2.h"
#include "Object/ReB2C.h"


//コンストラクタ
ResultScene::ResultScene(IGameObject * parent)
	: IGameObject(parent, "ResultScene"), hPict_(-1)/*, hSound_(-1)*/
{
}

//初期化
void ResultScene::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("data/Result.png");
	assert(hPict_ >= 0);
	CreateGameObject<ReB1>(this);
	CreateGameObject<ReB2>(this);

	//hSound_ = Audio::Load("data/TestSE.wav");
	//assert(hSound_ >= 0);
}

int butp1 = 0, butp2 = 0;

//更新
void ResultScene::Update()
{
	//両者が特定のボタンを押すとタイトルに戻るように
	if (Input::IsKeyDown(DIK_SPACE) || Input::IsPadButtonDown(XINPUT_GAMEPAD_A, 0))
	{
		butp1 = 1;
		//Audio::Play(hSound_);
		CreateGameObject<ReB1C>(this);
	}
	
	if (Input::IsKeyDown(DIK_RETURN) || Input::IsPadButtonDown(XINPUT_GAMEPAD_A, 1))
	{
		butp2 = 1;
		//Audio::Play(hSound_);
		CreateGameObject<ReB2C>(this);
	}
	
	if ( (butp1 == 1) && (butp2 == 1) )
	{

		butp1 = 0;
		butp2 = 0;
		//タイトルシーンに切り替わる
		SceneManager::ChangeScene(SCENE_ID_TITLE);
	}
}

//描画
void ResultScene::Draw()
{
	Image::SetMatrix(hPict_, worldMatrix_);
	Image::Draw(hPict_);
}

//開放
void ResultScene::Release()
{
}
