#include "Loading2.h"
#include "./Engine/Image.h"
#include "assert.h"



//コンストラクタ
Loading2::Loading2(IGameObject * parent)
	:IGameObject(parent, "Loading2"), _hPict(-1)
{
}

//デストラクタ
Loading2::~Loading2()
{
}

//初期化
void Loading2::Initialize()
{
	//画像データのロード
	_hPict = Image::Load("data/NowLoading0.png");
	assert(_hPict >= 0);
	//大きさを変える
	scale_ = D3DXVECTOR3(0.9f, 0.9f, 0.9f);
	//位置を変える
	position_ = D3DXVECTOR3(700.0f, 550.0f, 0);

}

//更新
void Loading2::Update()
{



}


//描画
void Loading2::Draw()
{
	Image::SetMatrix(_hPict, worldMatrix_);
	Image::Draw(_hPict);
}

//開放
void Loading2::Release()
{
}