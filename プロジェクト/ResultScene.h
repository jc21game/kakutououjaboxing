#pragma once

#include "Engine/global.h"
#include "Engine/Audio.h"
#include "Engine/Image.h"
#include "Engine/Input.h"

//リザルトシーンを管理するクラス
class ResultScene : public IGameObject
{

	int hPict_;		//画像番号
	//int hSound_;	//サウンド番号

public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	ResultScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};
