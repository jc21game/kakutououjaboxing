#pragma once
#include "Engine/IGameObject.h"

//リングを管理するクラス
class Ring : public IGameObject
{
	int hModel_;    //画像番号

public:
	//コンストラクタ
	Ring(IGameObject* parent);

	//デストラクタ
	~Ring();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};