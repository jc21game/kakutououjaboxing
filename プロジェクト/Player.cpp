#include "Player.h"
#include "Model.h"
#include "MiniOden.h"

//コンストラクタ
Player::Player(IGameObject * parent)
	:IGameObject(parent, "Player"), hModel_(-1)
{
}

//デストラクタ
Player::~Player()
{
	
}

//初期化
void Player::Initialize()
{
	hModel_ =  Model::Load("Data/Oden.fbx");

	CreateGameObject<MiniOden>(this);

	SetCollider(D3DXVECTOR3(0, 0, 0), 3.5f);
}

//更新
void Player::Update()
{
	if (Input::IsKey(DIK_LEFT))	position_.x -= 0.1f;
	if (Input::IsKey(DIK_RIGHT))position_.x += 0.1f;



	rotate_.y += 5;
	scale_.x = 0.3f;
	scale_.y = 0.3f;
	scale_.z = 0.3f;

	//if (position_.x > 5)
	//	KillMe();

	if (Input::IsKeyDown(DIK_ESCAPE))
	{
		SceneManager::ChangeScene(SCENE_ID_TEST);
	}
 }

//描画
void Player::Draw()
{
	Model::SetMatrix(hModel_, worldMatrix_);
	Model::Draw(hModel_);
}

//開放
void Player::Release()
{

}

void Player::OnCollision(IGameObject * pTarget)
{
	PostQuitMessage(0);
}
