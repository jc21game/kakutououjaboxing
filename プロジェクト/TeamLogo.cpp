#include "TeamLogo.h"
#include "Engine/Image.h"


//コンストラクタ
TeamLogo::TeamLogo(IGameObject * parent)
	:IGameObject(parent, "TeamLogo"), hPict_(-1)//初期化
{
}

//デストラクタ
TeamLogo::~TeamLogo()
{
}

//初期化
void TeamLogo::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("data/Ramen.png"); //チームロゴ
	assert(hPict_ >= 0);
	//大きさを変える(元の画像より小さくした)
	scale_ = D3DXVECTOR3(0.8f, 0.8f, 0.8f);

	//ポジション(上にある)
	position_ = D3DXVECTOR3(400, -900, 0);
}

//更新
void TeamLogo::Update()
{
	if (position_.y <= 100)
	{
		//int WaitTimer(1000);
		position_.y += 3.0f;
	}
}

//描画
void TeamLogo::Draw()
{
	//画像を描画する
	Image::SetMatrix(hPict_, worldMatrix_);
	Image::Draw(hPict_);
}

//開放
void TeamLogo::Release()
{
}