﻿#include "SceneManager.h"

////////シーンのインクルード//////////////////////
#include "BootScene.h"		//仮画面
#include "SplashScene.h"	//スプラッシュシーン
#include "TitleScene.h"		//タイトルシーン
#include "ChoiceScene.h"	//準備シーン
#include "PlayScene.h"		//戦闘(シングルプレイ)シーン
#include "ResultScene.h"	//リザルトシーン

//////////////////////////////////////////////////


SCENE_ID SceneManager::currentSceneID_ = SCENE_ID_BOOT;
SCENE_ID SceneManager::nextSceneID_ = SCENE_ID_BOOT;
IGameObject* SceneManager::pCurrentScene_ = nullptr;

//コンストラクタ
SceneManager::SceneManager(IGameObject * parent)
	:IGameObject(parent, "SceneManager")
{

}

//デストラクタ
SceneManager::~SceneManager()
{
}

//初期化
void SceneManager::Initialize()
{
	pCurrentScene_ = CreateGameObject<BootScene>(this);
}

//更新
void SceneManager::Update() 
{
	if (currentSceneID_ != nextSceneID_)
	{
		auto scene = childList_.begin();
		(*scene)->ReleaseSub();
		SAFE_DELETE(*scene);
		childList_.clear();

		switch (nextSceneID_)
		{
		case SCENE_ID_BOOT:
			pCurrentScene_ = CreateGameObject<BootScene>(this); break;

		case SCENE_ID_SPLASH:
			pCurrentScene_ = CreateGameObject<SplashScene>(this); break;

		case SCENE_ID_TITLE:
			pCurrentScene_ = CreateGameObject<TitleScene>(this); break;

		case SCENE_ID_CHOICE:
			pCurrentScene_ = CreateGameObject<ChoiceScene>(this); break;

		case SCENE_ID_FIGHT:
			pCurrentScene_ = CreateGameObject<PlayScene>(this); break;

		case SCENE_ID_RESULT:
			pCurrentScene_ = CreateGameObject<ResultScene>(this); break;

		}
		currentSceneID_ = nextSceneID_;
	}
}

//描画
void SceneManager::Draw()
{
}

//開放
void SceneManager::Release()
{
}

void SceneManager::ChangeScene(SCENE_ID next)
{
	nextSceneID_ = next;
}
