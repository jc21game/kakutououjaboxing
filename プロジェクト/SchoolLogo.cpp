#include "SchoolLogo.h"
#include "Engine/Image.h"
#include <chrono>
#include <thread>


//コンストラクタ
SchoolLogo::SchoolLogo(IGameObject * parent)
	:IGameObject(parent, "SchoolLogo"), hPict_(-1)//初期化
{
}

//デストラクタ
SchoolLogo::~SchoolLogo()
{
}

//初期化
void SchoolLogo::Initialize()
{

	//画像データのロード
	hPict_ = Image::Load("data/SchoolName.png"); //学校名
	assert(hPict_ >= 0);
	//大きさを変える(元の画像より小さくした)
	scale_ = D3DXVECTOR3(0.3f, 0.3f, 0.3f);

	//ポジション
	position_ = D3DXVECTOR3(500, -1000, 0);


}

//更新
void SchoolLogo::Update()
{

	if (position_.y <= 0)
	{
		position_.y += 3.0f;

	}

	if (position_.y >= 0)
	{
		std::this_thread::sleep_for(std::chrono::seconds(2));      // 2 秒
		//タイトルシーンへ切り替わる
		SceneManager::ChangeScene(SCENE_ID_TITLE);
	}
}

//描画
void SchoolLogo::Draw()
{
	//画像を描画する
	Image::SetMatrix(hPict_, worldMatrix_);
	Image::Draw(hPict_);
}

//開放
void SchoolLogo::Release()
{
}