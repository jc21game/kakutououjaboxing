#include "Ring.h"
#include "Engine/Model.h"

//コンストラクタ
Ring::Ring(IGameObject * parent)
	:IGameObject(parent, "Ring"), hModel_(-1)
{
}

//デストラクタ
Ring::~Ring()
{
}

//初期化
void Ring::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("data/ring.fbx");
	assert(hModel_ >= 0);

	scale_ = D3DXVECTOR3(4, 2, 4);

}

//更新
void Ring::Update()
{
	//向いている方向に回転させる行列
	D3DXMATRIX mRot;
	D3DXMatrixRotationY(&mRot, D3DXToRadian(rotate_.y));
	//回転させるスピード
	rotate_.y += 0.5f;
}

//描画
void Ring::Draw()
{
	Model::SetMatrix(hModel_, worldMatrix_);
	Model::Draw(hModel_);
}

//開放
void Ring::Release()
{
}