#include "SplashScene.h"
#include "Object/Logo.h"
#include "Object/SchoolName.h"


//コンストラクタ
SplashScene::SplashScene(IGameObject * parent)
	: IGameObject(parent, "SplashScene"), hPict_(-1)
{
}

//初期化
void SplashScene::Initialize()
{

	CreateGameObject<Logo>(this);
	CreateGameObject<SchoolName>(this);
}

//更新
void SplashScene::Update()
{
	//SPACEが押されたら
	if (Input::IsKeyDown(DIK_SPACE) || Input::IsPadButtonDown(XINPUT_GAMEPAD_A))
	{
		//タイトルシーンに切り替わる

		SceneManager::ChangeScene(SCENE_ID_TITLE);
	}
}

//描画
void SplashScene::Draw()
{

}

//開放
void SplashScene::Release()
{

}
