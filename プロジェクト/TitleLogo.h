#pragma once
#include "Engine/IGameObject.h"

//タイトルロゴを管理するクラス
class TitleLogo : public IGameObject
{

	int hPict_;    //画像番号

	//最初の動きを付けたところを飛ばしたい場合用
	int count = 0;
public:
	//コンストラクタ
	TitleLogo(IGameObject* parent);

	//デストラクタ
	~TitleLogo();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;


};
