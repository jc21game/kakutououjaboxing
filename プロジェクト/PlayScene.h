#pragma once

#include "Engine/Global.h"
#include "Engine/Direct3D.h"
#include "Engine/Input.h"
#include "Engine/Image.h"
#include "Engine/Camera.h"
#include "Engine/Text.h"

class Camera;
class Chara;
class Enemy;
class Text;

class Gauge_bar_1p;
class Gauge_bar_2p;
class Down2P;
class Down1P;

//戦闘シーンを管理するクラス
class PlayScene : public IGameObject
{

	int hPict_;

	int cnt1_;

	int cnt10_;

	int minCnt_;
	//フレームをカウントする変数
	int frame_;

	Text* pText_;    //テキスト
	Chara* pChara;
	Enemy* pEnemy;

	Gauge_bar_1p* pBar_1p;
	Gauge_bar_2p* pBar_2p;
	Down2P* pDown_2p;
	Down1P* pDown_1p;

	std::string sec_[11];	//秒カウント用配列

	std::string min_[11];	//分カウント用配列

public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	PlayScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//画面分割用のやつ
	void DrawSub()	override;



	Gauge_bar_1p* GetBar_1p()
	{
		//ポインタ
		return pBar_1p;
	}

	Gauge_bar_2p* GetBar_2p()
	{
		//ポインタ
		return pBar_2p;
	}

	Down2P* GetDown_2p()
	{
		//ポインタ
		return pDown_2p;
	}

	Down1P* GetDown_1p()
	{
		//ポインタ
		return pDown_1p;
	}

	Chara* GetChara()
	{
		//ポインタ
		return pChara;
	}

	Enemy* GetEnemy()
	{
		//ポイんた
		return pEnemy;
	}

};